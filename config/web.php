<?php

$config = [
	'id' => 'web-application',
	'defaultRoute' => 'main/default/index',
	'bootstrap' => [
		'log',
		'app\modules\user\Bootstrap',
		'app\modules\main\Bootstrap',
		'app\modules\admin\Bootstrap',
		'app\modules\navigation\Bootstrap',
		'app\modules\image\Bootstrap',
		'app\modules\seo\Bootstrap',
		'app\modules\news\Bootstrap',
		'app\modules\project\Bootstrap',
		'app\modules\contact\Bootstrap',
		'app\modules\slider\Bootstrap',
		'app\modules\catalog\Bootstrap',
		'app\modules\blockText\Bootstrap',
		'app\modules\portfolio\Bootstrap',
		'app\modules\page\Bootstrap',
		'app\modules\shop\Bootstrap',
		'app\modules\route\Bootstrap',
		'app\modules\config\Bootstrap',
	],
	'modules' => [
		'user' => [
			'class' => 'app\modules\user\Module',
			'controllerNamespace' => 'app\modules\user\controllers\frontend',
			'viewPath' => '@app/modules/user/views/frontend',
			'requireEmailConfirmation' => false,
		],
		'navigation' => [
			'class' => 'app\modules\navigation\Module',
		],
		'seo' => [
			'class' => 'app\modules\seo\Module',
		],
		'news' => [
			'class' => 'app\modules\news\Module',
			'controllerNamespace' => 'app\modules\news\controllers\frontend',
			'viewPath' => '@app/modules/news/views/frontend',
		],
		'project' => [
			'class' => 'app\modules\project\Module',
			'controllerNamespace' => 'app\modules\project\controllers\frontend',
			'viewPath' => '@app/modules/project/views/frontend',
		],
		'contact' => [
			'class' => 'app\modules\contact\Module',
			'controllerNamespace' => 'app\modules\contact\controllers\frontend',
			'viewPath' => '@app/modules/contact/views/frontend',
		],
		'portfolio' => [
			'class' => 'app\modules\portfolio\Module',
			'controllerNamespace' => 'app\modules\portfolio\controllers\frontend',
			'viewPath' => '@app/modules/portfolio/views/frontend',
		],
		'slider' => [
			'class' => 'app\modules\slider\Module',
		],
		'route' => [
			'class' => 'app\modules\route\Module',
		],
		'catalog' => [
			'class' => 'app\modules\catalog\Module',
			'controllerNamespace' => 'app\modules\catalog\controllers\frontend',
			'viewPath' => '@app/modules/catalog/views/frontend',
		],
		'blockText' => [
			'class' => 'app\modules\blockText\Module',
		],
		'page' => [
			'class' => 'app\modules\page\Module',
			'controllerNamespace' => 'app\modules\page\controllers\frontend',
			'viewPath' => '@app/modules/page/views/frontend',
		],
		'shop' => [
			'class' => 'app\modules\shop\Module',
			'controllerNamespace' => 'app\modules\shop\controllers\frontend',
			'viewPath' => '@app/modules/shop/views/frontend',
		],

		/*'treemanager' =>  [
			'class' => '\kartik\tree\Module',
			'treeStructure' => [
				'depthAttribute' => 'level',
			],
			// other module settings, refer detailed documentation
		],
		'news' => [
			'class' => 'app\modules\news\Module',
			'controllerNamespace' => 'app\modules\news\controllers\frontend',
			'viewPath' => '@app/modules/news/views/frontend',
		],
		'page' => [
			'class' => 'app\modules\page\Module',
			'controllerNamespace' => 'app\modules\page\controllers\frontend',
			'viewPath' => '@app/modules/page/views/frontend',
		],*/
		'admin' => [
			'class' => 'app\modules\admin\Module',
			'modules' => [
				'user' => [
					'class' => 'app\modules\user\Module',
					'controllerNamespace' => 'app\modules\user\controllers\backend',
					'viewPath' => '@app/modules/user/views/backend',
				],
				'news' => [
					'class' => 'app\modules\news\Module',
					'controllerNamespace' => 'app\modules\news\controllers\backend',
					'viewPath' => '@app/modules/news/views/backend',
				],
				'page' => [
					'class' => 'app\modules\page\Module',
					'controllerNamespace' => 'app\modules\page\controllers\backend',
					'viewPath' => '@app/modules/page/views/backend',
				],
				'project' => [
					'class' => 'app\modules\project\Module',
					'controllerNamespace' => 'app\modules\project\controllers\backend',
					'viewPath' => '@app/modules/project/views/backend',
				],
				'config' => [
					'class' => 'app\modules\config\Module',
					'controllerNamespace' => 'app\modules\config\controllers\backend',
					'viewPath' => '@app/modules/config/views/backend',
				],
				'contact' => [
					'class' => 'app\modules\contact\Module',
					'controllerNamespace' => 'app\modules\contact\controllers\backend',
					'viewPath' => '@app/modules/contact/views/backend',
				],
				'navigation' => [
					'class' => 'app\modules\navigation\Module',
					'controllerNamespace' => 'app\modules\navigation\controllers\backend',
					'viewPath' => '@app/modules/navigation/views/backend',
				],
				'slider' => [
					'class' => 'app\modules\slider\Module',
					'controllerNamespace' => 'app\modules\slider\controllers\backend',
					'viewPath' => '@app/modules/slider/views/backend',
				],
				'catalog' => [
					'class' => 'app\modules\catalog\Module',
					'controllerNamespace' => 'app\modules\catalog\controllers\backend',
					'viewPath' => '@app/modules/catalog/views/backend',
				],
				'blockText' => [
					'class' => 'app\modules\blockText\Module',
					'controllerNamespace' => 'app\modules\blockText\controllers\backend',
					'viewPath' => '@app/modules/blockText/views/backend',
				],
				'portfolio' => [
					'class' => 'app\modules\portfolio\Module',
					'controllerNamespace' => 'app\modules\portfolio\controllers\backend',
					'viewPath' => '@app/modules/portfolio/views/backend',
				],
				'route' => [
					'class' => 'app\modules\route\Module',
					'controllerNamespace' => 'app\modules\route\controllers\backend',
					'viewPath' => '@app/modules/route/views/backend',
				],
				'shop' => [
					'class' => 'app\modules\shop\Module',
					'controllerNamespace' => 'app\modules\shop\controllers\backend',
					'viewPath' => '@app/modules/shop/views/backend',
				],
				/*'page' => [
					'class' => 'app\modules\page\Module',
					'controllerNamespace' => 'app\modules\page\controllers\backend',
					'viewPath' => '@app/modules/page/views/backend',
				],
				'news' => [
					'class' => 'app\modules\news\Module',
					'controllerNamespace' => 'app\modules\news\controllers\backend',
					'viewPath' => '@app/modules/news/views/backend',
				],*/
			],
		],
		'main' => [
			'class' => 'app\modules\main\Module',
			// 'configFromSource' => app\modules\config\models\common\Config::getModuleConfig(app\modules\main\Module),
		],
		'image' => [
			'class' => '\app\modules\image\Module',
			'className' => '\app\modules\image\models\Image',
			'imagesStorePath' => 'uploads/store', //path to origin images
			'imagesCachePath' => 'uploads/cache', //path to resized copies
			'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
		],
		'config' => [
			'class' => 'app\modules\config\Module',
		],
	    'yii2images' => [
			'class' => 'rico\yii2images\Module',
			//be sure, that permissions ok
			//if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
			'imagesStorePath' => 'uploads/store', //path to origin images
			'imagesCachePath' => 'uploads/cache', //path to resized copies
			'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
			// 'placeHolderPath' => '@webroot/img/placeHolder.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
		],

		'sitemap' => [
	        'class' => 'himiklab\sitemap\Sitemap',
	        'models' => [
	            'app\modules\news\models\frontend\News',
	            'app\modules\catalog\models\frontend\Catalog',
	        ],
	        'enableGzip' => true, // default is false
	        'cacheExpire' => 1, // 1 second. Default is 24 hours
	    ],
	],
	'components' => [
		'request' => [
			'baseUrl' => '',
			// 'csrfParam' => '_csrf-backend',
			'cookieValidationKey' => 'Vli9OQvycuma_NjoWwLzwXuiU9IFdLHA',
		],
		'config' => [
			'class' => 'app\modules\config\components\AppConfig',
		],
		'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'my_application_cart',
        ],
		/*'assetManager' => array(
            //'linkAssets' => true,
            'forceCopy' => true
        ),*/

		'db' => [
			'class' => 'yii\db\Connection',
			'charset' => 'utf8',
			'dsn' => 'mysql:host=localhost;dbname=jsk',
			'username' => 'root',
			'password' => '',
			'tablePrefix' => 'tbl_',
		],

		'user' => [
			'identityClass' => 'app\modules\user\models\common\User',
			'enableAutoLogin' => true,
			'loginUrl' => ['user/default/login'],
		],

		'urlManager' => [
			'class' => 'app\components\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml'],
			],
		],

		'errorHandler' => [
			'errorAction' => 'main/default/error',
		],
		'cache' => [
	        'class' => 'yii\caching\FileCache',
	    ],
		/*'user' => [
			'identityClass' => 'app\modules\user\models\common\User',
			'enableAutoLogin' => true,
			'loginUrl' => ['user/default/login'],
		],

		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
		],*/
	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
