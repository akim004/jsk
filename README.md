Yii 2 CMS Application with all modules
===================================

INSTALLATION
------------

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install the application using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:1.0.0-beta2"
php composer.phar create-project --prefer-dist --stability=dev akim004/yii2-cms-app .
~~~

GETTING STARTED
---------------

After you install the application, you have to conduct the following steps to initialize
the installed application. You only need to do these once for all.

1. Run command `init` to initialize the application with a specific environment.
2. Create a new database and adjust `db` component  in `config/common.php` file.
3. Apply migrations with console command `yii migrate --migrationPath=@yii/rbac/migrations/`. This will create tables needed for RBAC module.
4. Apply migrations with console command `yii migrate --migrationPath=@dektrium/user/migrations/`. This will create tables needed for User's module.
5. Create superadmin user with console command `yii user/create email username password`.
6. Apply migrations with console commands:

DIRECTORY STRUCTURE
-------------------

```
    assets              contains application assets such as JavaScript and CSS
    commands            contains application console commands
    components          contains application components
    config              contains shared configurations
    mail                contains view files for e-mails
    controllers         contains console controllers (commands)
    environments            contains environment-based overrides
    messages
    migrations          contains database migrations
    models              contains model classes used in both 
    modules
    runtime             contains files generated during runtime
    tests               contains various tests for the advanced application
    vagrant
    validators          contains validators for model fields
    vendor              contains dependent 3rd-party packages
        codeception/         contains tests developed with Codeception PHP Testing Framework
    views               contains view files for the application
    web                 contains the entry script and Web resources
    widgets             contains application widgets

```
