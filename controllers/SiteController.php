<?php

namespace app\controllers;

use Yii;
use app\modules\blockText\models\common\BlockText;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\UploadedFile;



class SiteController extends Controller
{
	/*public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}*/

	public function actions()
	{
		return [
			// 'content-tools-image-upload' => \bizley\contenttools\actions\UploadAction::className(),
			// 'content-tools-image-insert' => \bizley\contenttools\actions\InsertAction::className(),
			'content-tools-image-rotate' => \bizley\contenttools\actions\RotateAction::className(),
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionSaveContent()
	{
		if(Yii::$app->user->can('moder')){

			$info = $_POST;

			$moduleName = str_replace('/', '', $info['page']);
			array_pop($info);
			array_pop($info);

			foreach ($info as $key => $data) {
				if($blockText = BlockText::find()->where(['param' => $key, 'module' => $moduleName])->one()){

				}else{
					$blockText = new BlockText;
					$blockText->param = $key;
					$blockText->module = $moduleName;
				}

				$blockText->description = $data;
				$blockText->save(false);
			}

		}
		return true;
		// return $this->render('index');
	}

	public function actionContentToolsImageInsert()
	{
		if(Yii::$app->user->can('moder')){
			try {
				if (Yii::$app->request->isPost) {
					$data = Yii::$app->request->post();
					if (empty($data['url'])) {
						throw new InvalidParamException('Invalid insert options!');
					}
					$url = trim($data['url']);
					if (substr($url, 0, 1) == '/') {
						$url = substr($url, 1);
					}
					if (strpos($url, '?_ignore=') !== false) {
						$url = substr($url, 0, strpos($url, '?_ignore='));
					}

					$crop = [];
					if (!empty($data['crop'])) {
						$crop = explode(',', $data['crop']);
						if (count($crop) !== 4) {
							throw new InvalidParamException('Invalid crop options!');
						}
						foreach ($crop as $c) {
							if (!is_numeric(trim($c)) || trim($c) < 0 || trim($c) > 1) {
								throw new InvalidParamException('Invalid crop options!');
							}
						}
					}
					if (!empty($crop)) {
						list($width, $height) = getimagesize($url);

						\yii\imagine\Image::crop(
								\Yii::getAlias('@webroot').'/'.$url,
								floor($width * trim($crop[3]) - $width * trim($crop[1])),
								floor($height * trim($crop[2]) - $height * trim($crop[0])),
								[
									floor($width * trim($crop[1])),
									floor($height * trim($crop[0]))
								]
							)->save(\Yii::getAlias('@webroot').'/'.$url);
					}

					list($width, $height) = getimagesize($url);

					return \yii\helpers\Json::encode([
						'size' => [$width, $height],
						'url'  => '/' . $url,
						'alt'  => basename($url)
					]);
				}
			} catch (Exception $e) {
				return Json::encode(['errors' => [$e->getMessage()]]);
			}
		}
	}

	public function actionContentToolsImageUpload()
	{
		if(Yii::$app->user->can('moder')){
			if (Yii::$app->request->isPost) {
				$model = new \app\models\ImageForm;
				$model->image = UploadedFile::getInstanceByName('image');
				if ($model->validate()) {
					if ($model->upload()) {
						$model->url = substr($model->url,1);
						list($width, $height) = getimagesize($model->url);
						return Json::encode([
							'size' => [$width, $height],
							'url'  => '/'.$model->url
						]);
					}
				}
				else {
					$errors = [];
					$modelErrors = $model->getErrors();
					foreach ($modelErrors as $field => $fieldErrors) {
						foreach ($fieldErrors as $fieldError) {
							$errors[] = $fieldError;
						}
					}
					if (empty($errors)) {
						$errors = ['Unknown file upload validation error!'];
					}
					return Json::encode(['errors' => $errors]);
				}
			}
		}
	}
}
