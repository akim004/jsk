<?php

namespace app\modules\image;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	public function bootstrap($app)
	{
		$app->i18n->translations['modules/image/*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'forceTranslation' => true,
			'basePath' => '@app/modules/image/messages',
			'fileMap' => [
				'modules/image/module' => 'module.php',
			],
		];

		$app->urlManager->registerModuleRules($app->getModule('image'));
	}
}