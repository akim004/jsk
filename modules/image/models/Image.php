<?php

namespace app\modules\image\models;

use Yii;
use yii\helpers\ArrayHelper;

class Image extends \rico\yii2images\models\Image
{
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}

	public function getNext($modelName, $itemId)
	{
		return self::find()->where('id > :id AND modelName = :modelName AND itemId = :itemId', ['id' => $this->id, ':modelName' => $modelName, ':itemId' => $itemId])->orderBy('id ASC')->limit(1)->one();
	}

	public function getPrev($modelName, $itemId)
	{
		return self::find()->where('id < :id AND modelName = :modelName AND itemId = :itemId', ['id' => $this->id, ':modelName' => $modelName, ':itemId' => $itemId])->orderBy('id DESC')->limit(1)->one();
	}

	public function getPath($size = false, $iscompress = false){
		$urlSize = ($size) ? '_'.$size : '';
		$base = $this->getModule()->getCachePath();
		$sub = $this->getSubDur();

		$origin = $this->getPathToOrigin();

		$filePath = $base.DIRECTORY_SEPARATOR.
			$sub.DIRECTORY_SEPARATOR.$this->urlAlias.$urlSize.'.'.pathinfo($origin, PATHINFO_EXTENSION);;
		if(!file_exists($filePath)){
			$this->createVersion($origin, $size);

			if($iscompress){
				$tiny = new \app\components\TinyPng();
				$tiny->compress($filePath);
			}

			if(!file_exists($filePath)){
				throw new \Exception('Problem with image creating.');
			}
		}


		return $filePath;
	}
}


