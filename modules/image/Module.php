<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/image/migrations --interactive=0
 *
 *  "costa-rico/yii2-images": "dev-master"
 *
 * php yii migrate/up --migrationPath=@vendor/costa-rico/yii2-images/migrations
 */

namespace app\modules\image;

use Yii;

class Module extends \rico\yii2images\Module implements \app\components\ModuleInterface
{
	use \app\components\ModuleTrait;

	public $controllerNamespace = 'app\modules\image\controllers';

	public $maxImageWidth = 1000;
	public $maxImageHeight = 1000;
	public $maxSize = 5; // 5МБ
	public $maxFiles = 10; // количество картнок

	public function init()
	{
		parent::init();

		$this->registerConfig($this->configFromSource);

	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/image/' . $category, $message, $params, $language);
	}

	public static function getUrlRules()
	{
		return [
		];
	}

}
