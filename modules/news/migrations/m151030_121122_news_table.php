<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_121122_news_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%news}}', [
			'id' => Schema::TYPE_PK,
			'created_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'short'       => Schema::TYPE_STRING . '(500) NULL DEFAULT NULL',
			'description' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
			'date'        => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'position'    => Schema::TYPE_INTEGER . '(2) NULL DEFAULT NULL DEFAULT 0',
			'slug'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'published'   => Schema::TYPE_SMALLINT . '(2) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createIndex('idx_news_name', '{{%news}}', 'name');
		$this->createIndex('idx_news_slug', '{{%news}}', 'slug');
	}

	public function down()
	{
		$this->dropTable('{{%news}}');
	}
}
