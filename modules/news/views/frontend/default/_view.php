<?php

use yii\helpers\Html;

?>

<div class="news-default-index">
	<?=Yii::$app->formatter->asDate($model->date, 'long');?>
	<h1><?=$model->name?></h1>
	<p>
		<?=$model->short?>
	</p>
	<?=Html::a($model->name, $model->createUrl);?>
</div>
