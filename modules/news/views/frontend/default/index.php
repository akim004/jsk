<?php

use yii\widgets\ListView;

?>

<div class="news-default-index">

	<?
	echo ListView::widget([
		'dataProvider' => $listDataProvider,
		'itemView' => '_view',

		'options' => [
			'tag' => 'div',
			'class' => 'news-list',
			'id' => 'news-list',
		],

		'layout' => "{summary}\n{items}\n{pager}",
		'summary' => 'Показано {count} из {totalCount}',
		'summaryOptions' => [
			'tag' => 'span',
			'class' => 'my-summary'
		],

		'itemOptions' => [
			'tag' => 'div',
			'class' => 'news-item',
		],

		'emptyText' => '<p>Список пуст</p>',
		'emptyTextOptions' => [
			'tag' => 'p'
		],

		'pager' => [
			'firstPageLabel' => 'Первая',
			'lastPageLabel' => 'Последняя',
			'nextPageLabel' => 'Следующая',
			'prevPageLabel' => 'Предыдущая',
			'maxButtonCount' => 5,
		],
	]);
	?>
</div>
