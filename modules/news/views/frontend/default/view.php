<?php

$this->context->title = $model->name;

if($model->seoText->title){
	$this->context->title = $model->seoText->title;
}
if($model->seoText->description){
	$this->context->description = $model->seoText->description;
}
if($model->seoText->keywords){
	$this->context->keywords = $model->seoText->keywords;
}

?>

<div class="news-default-index">
    <?=$model->description?>
</div>
