<?php

use app\modules\news\Module;
use app\modules\news\models\backend\News;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="news-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
		<li><a href="#settings">SEO</a></li>
		<li><a href="#sitemap">Sitemap</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'short')->textarea(['maxlength' => true]) ?>

			<?/*= $form->field($model, 'description')->widget(\app\widgets\CKEditor::className(), [
				'options' => ['rows' => 6],
				'preset' => 'advanced'
			]) */?>

			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

			<?
			if(!empty($model->date)){
			    $model->date = date('d.m.Y', $model->date);
			}else{
				$model->date = date('d.m.Y', time());
			}
			echo '<label class="control-label">'.$model->getAttributeLabel('date').'</label>';
			echo \kartik\date\DatePicker::widget([
				'model' => $model,
    			'attribute' => 'date',
    			'convertFormat' => true,
				'type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
				// 'name' => 'dp_3',
				// 'value' => '23-Feb-1982',
				'pluginOptions' => [
					'autoclose'=>true,
					'format' => 'dd.MM.yyyy'
				]
			]);
			?>


			<?= $form->field($model, 'published')->dropDownList(News::getStatusesArray()) ?>

			<div >
				<?$preview = $model->getImage() ? [Html::img('/'.$model->getImage()->getPathToOrigin(), ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon']),] : false;

					echo $form->field($model, 'imageUpload')->widget(FileInput::classname(), [
						'options' => [
							'accept' => 'imageUpload/*',
							'class' => 'for-remove-function',
						],
						'pluginOptions' => [
							'maxImageWidth' => $model->maxImageWidth,
							'maxImageHeight' => $model->maxImageHeight,
							'initialPreview'   => $preview,
							'initialCaption'   => "Загрузите картинку",
							'overwriteInitial' => true,
							'showUpload'       => false,
							'allowedFileExtensions' => ['jpg','gif','png'],
						]
					]);
				?>
			</div>
			<?= $form->field($model, 'isRemoveImage')->hiddenInput(['class' => 'removeImage'])->label(false); ?>
		</div>
		<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\RouteForm::widget(['model' => $model]) ?>
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>
		<div class="tab-pane x_content" id="sitemap">
			<?= \app\widgets\SitemapForm::widget(['model' => $model]) ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton(Module::t('module', 'Apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update and close'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
		<?= Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
