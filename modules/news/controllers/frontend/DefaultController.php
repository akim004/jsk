<?php

namespace app\modules\news\controllers\frontend;

use Yii;
use app\modules\news\models\frontend\News;
use app\modules\news\models\frontend\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends \app\components\FrontendController
{
    public function actionIndex()
    {
    	$searchModel = new NewsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'listDataProvider' => $dataProvider,
		]);
    }

    public function actionView($slug)
    {
        $model = $this->findModel($slug);

        return $this->render('view', ['model' => $model]);
    }

    protected function findModel($slug)
	{
		if (($model = News::find()->where(['slug' => $slug])->active()->one()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
