<?php

return [
	'News'                  => 'Новости',
	'Create News'           => 'Создать новость',
	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать новость: ',

	'successfully added' => 'Новость успешно добавлена',
	'successfully changed' => 'Новость успешно изменена',

	'Id'          => 'ID',
	'Created_at'  => 'Создан',
	'Updated_at'  => 'Обновлен',
	'Name'        => 'Заголовок',
	'Short'       => 'Короткое описание',
	'Description' => 'Полное описание',
	'Slug'        => 'Алиас',
	'Published'   => 'Статус',
	'Position'    => 'Позиция в списке',
	'ImageUpload' => 'Картинка',
	'Date'        => 'Дата',

	'Create' => 'Создать',
	'Apply'  => 'Сохранить',
	'Cancel' => 'Отменить',
	'Update and close' => 'Сохранить и выйти',

	'Active'     => 'Активный',
	'Not active' => 'Скрытый',
];