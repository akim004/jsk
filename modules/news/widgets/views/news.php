<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="news-box wrapper">
	<?if($news){?>
		<h2 class="subtitle">Новости</h2>

		<ul class="news-list">
		<?foreach ($news as $data) {?>
			<li>
				<div class="date">
					<span><span><?=date('d',$data->date)?></span><?=\Yii::$app->formatter->asDate($data->date, 'MMMM');?></span><?=date('Y',$data->date)?>
				</div>
				<a href="<?=$data->createUrl?>">
					<span class="title">
						<?=Html::encode($data->name)?>
					</span>
					<span class="text">
						<?=Html::encode($data->short)?>
					</span>
				</a>
		<?}?>

		</ul>

		<div class="more">
			<a href="<?=Url::to(['/news/default/index'])?>">Все новости</a>
		</div>
<?}?>
</div><!-- .news-box -->