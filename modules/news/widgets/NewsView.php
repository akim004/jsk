<?php
namespace app\modules\news\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use \app\modules\news\models\frontend\News;

class NewsView extends Widget
{
	public function init()
	{
		parent::init();

	}

	public function run()
	{
		$news = News::find()->orderBy('date DESC')->limit(3)->active()->all();

		return $this->render('news', [
			'news' => $news,
		]);
	}
}