<?php

namespace app\modules\news\models\frontend;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class News extends \app\modules\news\models\common\News
{
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'sitemap' => [
				'class' => \himiklab\sitemap\behaviors\SitemapBehavior::className(),
				'scope' => function ($model) {
					$model->joinWith(['sitemap sitemap'])->active()->andWhere('sitemap.enable = 1');
				},
				'dataClosure' => function ($model) {
					$sitemap = \app\modules\seo\models\Sitemap::find()->where(['item_id' => $model->id, 'class' => 'News'])->one();
					return [
						'loc' => $model->createUrl,
						'lastmod' => $model->updated_at,
						'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
						'priority' => $sitemap ? $sitemap->priority : 0.5,
					];
				}
			],
		]);
	}


	

	public function getImageUrl($size = '')
	{
		return $this->image ? $this->image->getUrl($size) : '';
	}
}
