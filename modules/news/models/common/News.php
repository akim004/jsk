<?php

namespace app\modules\news\models\common;

use Yii;
use app\modules\news\Module;
use app\modules\news\models\common\NewsQuery;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property integer $
 * @property integer $updated_at
 * @property string $name
 * @property string $short
 * @property string $description
 * @property integer $date
 * @property integer $position
 * @property string $slug
 * @property integer $published
 */
class News extends \yii\db\ActiveRecord
{
	/**
	 * Загружаемая картинка
	 * @var UploadedFile
	 */
	public $imageUpload;

	const ACTIVE = 0;
	const NOT_ACTIVE = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%news}}';
	}

	public static function find()
	{
		$newsQuery = new NewsQuery(get_called_class());

		return $newsQuery->orderBy(['position' => SORT_ASC]);
	}

	public function behaviors()
	{
		return [
			['class' => \app\behaviors\SeoBehavior::className()],
			['class' => \app\behaviors\SitemapBehavior::className()],
			['class' => \app\behaviors\RouteBehavior::className()],
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 1,
				/*'maxImageWidth' => '1000',
				'maxImageHeight' => '1000',
				'maxSize' => '5',*/
			],

		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['description'], 'string'],
			[['name', 'slug'], 'string', 'max' => 255],
			[['short'], 'string', 'max' => 500],
			[['position', 'published'], 'safe'],

			[['date'], 'filter', 'filter' => function ($value) {
				if(!preg_match("/^[\d\+]+$/",$value) && $value > 0){
					return strtotime($value);
				} else{
					return $value;
				}
			}],
			[['date'], 'integer'],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'Id'),
			'created_at'  => Module::t('module', 'Created_at'),
			'updated_at'  => Module::t('module', 'Updated_at'),
			'id'          => Module::t('module', 'Id'),
			'name'        => Module::t('module', 'Name'),
			'short'       => Module::t('module', 'Short'),
			'description' => Module::t('module', 'Description'),
			'slug'        => Module::t('module', 'Slug'),
			'published'   => Module::t('module', 'Published'),
			'position'    => Module::t('module', 'Position'),
			'imageUpload' => Module::t('module', 'ImageUpload'),
			'date'        => Module::t('module', 'Date'),
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE     => Module::t('module', 'Active'),
			self::NOT_ACTIVE => Module::t('module', 'Not active'),
		];
	}

	public function afterDelete() {
		parent::afterDelete();
		if($this->getImage()){
			$this->removeImage($this->getImage());
		}
	}

	public function saveImage($file)
	{
		$image = $this->getImage();

		if($this->isRemoveImage && $image){
			$this->removeImage($image);
		}

		if($file){
			if($image){
				$this->removeImage($image);
			}

			$file->saveAs('uploads/runtime/'.$file->name);
			$this->attachImage('uploads/runtime/'.$file->name);
			@unlink('uploads/runtime/'.$file->name);
		}
	}

	public function getCreateUrl()
	{
		return Url::to(['/news/default/view', 'slug' => $this->slug]);
	}
}
