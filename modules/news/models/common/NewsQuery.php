<?php

namespace app\modules\news\models\common;

use app\modules\news\models\common\News;

class NewsQuery extends \yii\db\ActiveQuery
{

    public function active()
	{
		return $this->andWhere(['published' => News::ACTIVE]);
	}
}