<?php

namespace app\modules\news\models\backend;

use Yii;
use app\modules\news\Module;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class News extends \app\modules\news\models\common\News
{
	/**
	 * Флаг проверки удалить ли картинку
	 * @var bool
	 */
	public $isRemoveImage;

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			[
				'class' => 'Zelenin\yii\behaviors\Slug',
				'attribute' => 'name',
				'slugAttribute' => 'slug',
				'ensureUnique' => true, // уникальность в базе
				// 'translit' => true,
				'replacement' => '-',
				'lowercase' => true,
				'immutable' => true,
				'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
			],
			['class' => \yii\behaviors\TimestampBehavior::className()],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['isRemoveImage'], 'safe'],
		]);
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::ACTIVE,
				'text' => Module::t('module', 'Active'),
			],
			[
				'value' => self::NOT_ACTIVE,
				'text' => Module::t('module', 'Not active'),
			],
		];
	}
}
