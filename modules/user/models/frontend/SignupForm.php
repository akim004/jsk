<?php
namespace app\modules\user\models\frontend;

use Yii;
use app\modules\user\Module;
use app\modules\user\traits\ModuleTrait;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
	public $username;
	public $email;
	public $password;
	public $password_repeat;
	public $verifyCode;

	use ModuleTrait;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['username', 'filter', 'filter' => 'trim'],
			['username', 'required'],
			['username', 'unique', 'targetClass' => User::className()],
			['username', 'string', 'min' => 2, 'max' => 255],

			// ['email', 'filter', 'filter' => 'trim'],
			// ['email', 'required'],
			// ['email', 'email'],
			// ['email', 'string', 'max' => 255],
			// ['email', 'unique', 'targetClass' => User::className()],

			['password', 'required'],
			['password', 'string', 'min' => 6],
			['password_repeat', 'compare', 'compareAttribute'=>'password'],

			['verifyCode', 'captcha', 'captchaAction' => '/user/default/captcha', 'skipOnEmpty' => !$this->module->checkCaptcha],
		];
	}

	public function attributeLabels()
	{
		return [
			'username'        => Module::t('module', 'Username'),
			'password'        => Module::t('module', 'Password'),
			'email'           => Module::t('module', 'Email'),
			'verifyCode'      => Module::t('module', 'Verify code'),
			'password_repeat' => Module::t('module', 'Password repeat'),
		];
	}

	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function signup()
	{
		if (!$this->validate()) {
            return false;
        }

        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);

        $user->setPassword($this->password);
		$user->generateEmailConfirmToken();

		if(!$user->save()){
			return false;
		}

		if($this->module->requireEmailConfirmation){
			$this->getModule()->mailer->sendConfirmEmailMessage($user);
		}

		return $user;
	}

	protected function loadAttributes(User $user)
    {
        $user->setAttributes($this->attributes);
    }
}
