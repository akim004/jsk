<?php

namespace app\modules\user\models\frontend;

use Yii;
use app\modules\user\Module;
use app\modules\user\models\frontend\User;
use app\modules\user\traits\ModuleTrait;
use yii\base\Model;
/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
	use ModuleTrait;

	public $email;
	private $_user = false;
	private $_timeout;

	/**
	 * PasswordResetRequestForm constructor.
	 * @param integer $timeout
	 * @param array $config
	 */
	public function __construct($timeout, $config = [])
	{
		$this->_timeout = $timeout;
		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['email', 'filter', 'filter' => 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'exist',
				'targetClass' => User::className(),
				'filter' => ['status' => User::STATUS_ACTIVE],
				'message' => Module::t('module', 'Error user not found by email')
			],
			['email', 'validateIsSent'],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'email' => Module::t('module', 'Email'),
		];
	}
	/**
	 * @param string $attribute
	 * @param array $params
	 */
	public function validateIsSent($attribute, $params)
	{
		if (!$this->hasErrors() && $user = $this->getUser()) {
			if (User::isPasswordResetTokenValid($user->$attribute, $this->_timeout)) {
				$this->addError($attribute, Module::t('module', 'Error token is sent'));
			}
		}
	}

	/**
	 * Sends an email with a link, for resetting the password.
	 *
	 * @return boolean whether the email was send
	 */
	public function sendEmail()
	{
		if ($user = $this->getUser()) {
			$user->generatePasswordResetToken();
			if ($user->save()) {
				return $this->getModule()->mailer->sendPasswordResetMessage($user);
			}
		}
		return false;
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	public function getUser()
	{
		if ($this->_user === false) {
			$this->_user = User::findOne([
				'email' => $this->email,
				'status' => User::STATUS_ACTIVE,
			]);
		}
		return $this->_user;
	}
}