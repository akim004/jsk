<?php

namespace app\modules\user\models\frontend;

use Yii;
use app\modules\user\Module;
use app\modules\user\models\frontend\User;
use app\modules\user\traits\ModuleTrait;
use yii\base\Model;

class LoginForm extends Model
{

	use ModuleTrait;

	public $username;
	public $password;
	public $rememberMe = true;

	private $_user = false;


	public function rules()
	{
		return [
			['username', 'trim'],
			['username', 'required'],

			['password', 'required'],
			['password', 'validatePassword'],

			['rememberMe', 'boolean'],
		];
	}

	public function attributeLabels()
	{
		return [
			'username'   => Module::t('module', 'Username'),
			'rememberMe' => Module::t('module', 'Remember me'),
			'password'   => Module::t('module', 'Password'),
		];
	}

	/**
	 * Validates the username and password.
	 * This method serves as the inline validation for password.
	 */
	public function validatePassword()
	{
		if (!$this->hasErrors()) {
			$user = $this->getUser();

			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError('password', Module::t('module', 'Invalid login or password'));
			} elseif ($user && $user->status == User::STATUS_BLOCKED) {
				$this->addError('username', Module::t('module','Your account has been blocked.'));
			} elseif ($user && $user->status == User::STATUS_WAIT) {
				$this->addError('username', Module::t('module', 'You need to confirm your email address.'));
			}
		}
	}

	/**
	 * Logs in a user using the provided username and password.
	 * @return boolean whether the user is logged in successfully
	 */
	public function login()
	{
		if ($this->validate()) {
			return Yii::$app->user->login($this->getUser(), $this->rememberMe ? $this->module->rememberFor : 0);
		} else {
			return false;
		}
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	public function getUser()
	{
		if ($this->_user === false) {
			$this->_user = User::findByUsername($this->username);
		}

		return $this->_user;
	}
}
