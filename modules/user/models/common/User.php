<?php

namespace app\modules\user\models\common;

use Yii;
use app\modules\user\Module;
use app\modules\user\traits\ModuleTrait;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $username
 * @property string $auth_key
 * @property string $email_confirm_token
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 */
class User extends ActiveRecord implements IdentityInterface
{
	use ModuleTrait;

	const STATUS_BLOCKED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_WAIT = 2;

	const ROLE_DEFAULT = 1;
	const ROLE_USER = 1;
	const ROLE_MODER = 5;
	const ROLE_ADMIN = 10;

	// const SCENARIO_PROFILE = 'profile';

	private $security;

	public function __construct($config = []) {

		$this->security = Yii::$app->security;

		parent::__construct($config);
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%user}}';
	}

	public static function find()
	{
		return new UserQuery(get_called_class());
	}

	 public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/*public function scenarios()
	{
		 return ArrayHelper::merge(parent::scenarios(), [
			 self::SCENARIO_PROFILE => ['email'],
		]);
	}*/

	/**
	 * @inheritdoc
	 */
	 public function rules()
	{
		return [
			['username', 'required', 'on' => ['register']],
			// ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
			['username', 'unique', 'targetClass' => self::className(), 'message' => 'This username has already been taken.'],
			['username', 'string', 'min' => 3, 'max' => 255],

			// ['email', 'required', 'on' => ['register']],
			['email', 'email'],
			['email', 'unique', 'targetClass' => self::className(), 'message' => 'This email address has already been taken.'],
			['email', 'string', 'max' => 255],

			['status', 'integer'],
			['status', 'default', 'value' => $this->module->requireEmailConfirmation ? self::STATUS_WAIT : self::STATUS_ACTIVE],
			['status', 'in', 'range' => array_keys(self::getStatusesArray())],

			['role', 'integer', 'max' => 64],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'         => Module::t('module', 'Id'),
			'created_at' => Module::t('module', 'Created_at'),
			'updated_at' => Module::t('module', 'Updated_at'),
			'username'   => Module::t('module', 'Username'),
			'email'      => Module::t('module', 'Email'),
			'status'     => Module::t('module', 'Status'),
			'role'       => Module::t('module', 'Role'),
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::STATUS_BLOCKED => 'Заблокирован',
			self::STATUS_ACTIVE  => 'Активен',
			self::STATUS_WAIT    => 'Ожидает подтверждения',
		];
	}

	public function getStatusName()
	{
		 return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
	}

	public static function getRolesArray()
	{
		return [
			self::ROLE_USER  => 'Пользователь',
			self::ROLE_MODER => 'Модератор',
			self::ROLE_ADMIN => 'Администратор',
		];
	}

	public function getRoleName()
	{
		 return ArrayHelper::getValue(self::getRolesArray(), $this->role);
	}

	 /**
	* @inheritdoc
	*/
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
	}

	 /**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	 /**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 * @return static|null
	 */
	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return $this->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password_hash = $this->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = $this->security->generateRandomString();
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token, $timeout)
    {
        if (!static::isPasswordResetTokenValid($token, $timeout)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @param integer $timeout
	 * @return bool
	 */
	public static function isPasswordResetTokenValid($token, $timeout)
	{
		if (empty($token)) {
			return false;
		}
		$parts = explode('_', $token);
		$timestamp = (int) end($parts);
		return $timestamp + $timeout >= time();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = $this->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}

	/**
	 * @param string $email_confirm_token
	 * @return static|null
	 */
	public static function findByEmailConfirmToken($emailConfirmToken)
	{
		return static::findOne(['email_confirm_token' => $emailConfirmToken, 'status' => self::STATUS_WAIT]);
	}

	/**
	 * Removes email confirmation token
	 */
	public function removeEmailConfirmToken()
	{
		$this->email_confirm_token = null;
	}
	/**
	 * Generates email confirmation token
	 */
	public function generateEmailConfirmToken()
	{
		$this->email_confirm_token = $this->security->generateRandomString();
	}


	 /**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($insert) {
				// Set default status
				if (!$this->status) {
					$this->status = $this->module->requireEmailConfirmation ? self::STATUS_WAIT : self::STATUS_ACTIVE;

				}
				if (!$this->role) {
					$this->role = self::ROLE_DEFAULT;
				}
				$this->generateAuthKey();
			}
			return true;
		}
		return false;
	}


}
