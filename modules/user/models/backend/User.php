<?php

namespace app\modules\user\models\backend;

use Yii;
use app\modules\user\Module;
use yii\helpers\ArrayHelper;

class User extends \app\modules\user\models\common\User
{
	const SUPERMAN = 1;

	const SCENARIO_ADMIN_CREATE = 'adminCreate';
	const SCENARIO_ADMIN_UPDATE = 'adminUpdate';

	public $newPassword;
	public $newPasswordRepeat;

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['newPassword', 'newPasswordRepeat'], 'required', 'on' => self::SCENARIO_ADMIN_CREATE],
			['newPassword', 'string', 'min' => 6],
			['newPasswordRepeat', 'compare', 'compareAttribute' => 'newPassword'],
			['superman', 'mayEdit'],
		]);
	}

	public function mayEdit()
	{
		if (!$this->hasErrors()) {
			if((Yii::$app->user->identity->superman != self::SUPERMAN) && ($this->superman == self::SUPERMAN)){
				$this->addError('username', Module::t('module','Супермена нельзя трогать'));
			}
		}
	}

	public function scenarios()
	{
		return ArrayHelper::merge(parent::scenarios(), [
			self::SCENARIO_ADMIN_CREATE => ['username', 'email', 'status', 'newPassword', 'newPasswordRepeat','role'],
			self::SCENARIO_ADMIN_UPDATE => ['username', 'email', 'status', 'newPassword', 'newPasswordRepeat','role', 'superman'],
		]);
	}

	public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'newPassword'       => 'Новый пароль',
			'newPasswordRepeat' => 'Повторите новый пароль',
		]);
	}

	public function beforeSave($insert){

		if (parent::beforeSave($insert)) {
			if (!empty($this->newPassword)) {
				$this->setPassword($this->newPassword);
			}
			return true;
		}
		return false;
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::STATUS_BLOCKED,
				'text' => 'Заблокирован',
			],
			[
				'value' => self::STATUS_ACTIVE,
				'text' => 'Активен',
			],
			[
				'value' => self::STATUS_WAIT,
				'text' => 'Ожидает подтверждения',
			],
		];
	}

	public static function getRolesArrayForSource()
	{
		return [
			[
				'value' => self::ROLE_USER,
				'text' => 'Пользователь',
			],
			[
				'value' => self::ROLE_MODER,
				'text' => 'Модератор',
			],
			[
				'value' => self::ROLE_ADMIN,
				'text' => 'Администратор',
			],
		];
	}
}
