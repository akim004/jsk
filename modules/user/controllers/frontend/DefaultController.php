<?php

namespace app\modules\user\controllers\frontend;

use Yii;
use app\components\FrontendController;
use app\modules\user\Module;
use app\modules\user\models\frontend\EmailConfirmForm;
use app\modules\user\models\frontend\LoginForm;
use app\modules\user\models\frontend\PasswordResetForm;
use app\modules\user\models\frontend\PasswordResetRequestForm;
use app\modules\user\models\frontend\SignupForm;
use app\modules\user\traits\AjaxValidationTrait;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends FrontendController
{
	use AjaxValidationTrait;

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout', 'signup'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex(){
		return $this->redirect(['profile/index'], 301);
	}

	// Регистрация
	public function actionSignup(){

		$model = \Yii::createObject(SignupForm::className());

		$this->performAjaxValidation($model);

		if ($model->load(Yii::$app->request->post()) && ($user = $model->signup())) {

			if(Yii::$app->request->isAjax){
				\Yii::$app->response->format = Response::FORMAT_JSON;

				\Yii::$app->user->login($user, $this->module->rememberFor);

				$result['redirect'] = Url::to(['/user/profile/index']);

	            return $result;
			}

			Yii::$app->getSession()->setFlash('success', Module::t('module', 'Success signup'));
			if($this->module->requireEmailConfirmation){
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'A message has been sent to your email address. It contains a confirmation link that you must click to complete registration.'));
			}
			return $this->goHome();
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	// Подтверждение по email
	public function actionEmailConfirm($token){

		try {
			$model = \Yii::createObject(
							['class' => EmailConfirmForm::className()],
							[$token]
						);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->confirmEmail()) {
			Yii::$app->getSession()->setFlash('success', Module::t('module', 'Success confirm email'));
		} else {
			Yii::$app->getSession()->setFlash('error', Module::t('module', 'Error confirm email'));
		}

		return $this->goHome();
	}

	// Вход
	public function actionLogin(){

		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = \Yii::createObject(LoginForm::className());

		$this->performAjaxValidation($model);

		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			if(Yii::$app->request->isAjax){
				\Yii::$app->response->format = Response::FORMAT_JSON;
				$result['redirect'] = Url::to(['/user/profile/index']);
	            return $result;
			}
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}


	public function actionLogout(){
		Yii::$app->user->logout();

		return $this->goHome();
	}



	public function actionPasswordResetRequest()
	{
		$model = \Yii::createObject(
								['class' => PasswordResetRequestForm::className()],
								[$this->module->passwordResetTokenExpire]
							);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'Flash password reset request'));
				return $this->goHome();
			} else {
				Yii::$app->getSession()->setFlash('error', Module::t('module', 'Flash password reset error'));
			}
		}
		return $this->render('passwordResetRequest', [
			'model' => $model,
		]);
	}

	public function actionPasswordReset($token)
	{
		try {
			$model = \Yii::createObject(
								['class' => PasswordResetForm::className()],
								[$token, $this->module->passwordResetTokenExpire]
							);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}
		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', Module::t('module', 'Flash password reset success'));
			return $this->goHome();
		}
		return $this->render('passwordReset', [
			'model' => $model,
		]);
	}
}
