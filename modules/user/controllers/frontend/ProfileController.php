<?php
namespace app\modules\user\controllers\frontend;

use Yii;
use app\components\FrontendController;
use app\modules\user\models\frontend\ChangePasswordForm;
use app\modules\user\models\frontend\User;
use app\modules\user\traits\AjaxValidationTrait;
use app\modules\user\Module;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\web\Controller;

class ProfileController extends FrontendController
{
	use AjaxValidationTrait;

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
	public function actionIndex()
	{
		return $this->render('index', [
			'model' => $this->findModel(),
		]);
	}
	public function actionUpdate()
	{
		$model = $this->findModel();
		$model->scenario = User::SCENARIO_PROFILE;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}
	public function actionChangePassword()
	{
		$user = $this->findModel();

		$model = \Yii::createObject(
							['class' => ChangePasswordForm::className()],
							[$user]
						);

		$this->performAjaxValidation($model);

		if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
			Yii::$app->getSession()->setFlash('success', Module::t('module', 'Password has been changed'));
			if(Yii::$app->request->isAjax){
				\Yii::$app->response->format = Response::FORMAT_JSON;
	            return 'success';
			}
			return $this->redirect(['index']);
		} else {
			return $this->render('changePassword', [
				'model' => $model,
			]);
		}
	}
	/**
	 * @return User the loaded model
	 */
	private function findModel()
	{
		return User::findOne(Yii::$app->user->identity->getId());
	}
}