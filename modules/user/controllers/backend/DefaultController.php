<?php

namespace app\modules\user\controllers\backend;

use Yii;
use app\components\BackendController;
use app\modules\user\Module;
use app\modules\user\models\backend\User;
use app\modules\user\models\backend\UserSearch;
use dosamigos\editable\EditableAction;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UsersController implements the CRUD actions for User model.
 */
class DefaultController extends BackendController
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(), // Ограничение по типу запросов GET , POST, PUT ...
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['moder'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'editable' => [
				'class' => EditableAction::className(),
				'modelClass' => User::className(),
				'forceCreate' => false
			],
		];
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionIndex(){

		$searchModel = new UserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionView($id){

		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionCreate(){

		$model = new User();
		$model->scenario = User::SCENARIO_ADMIN_CREATE;
		$model->status   = User::STATUS_ACTIVE;

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully added'));

			if(isset($_GET['wait'])){
				return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
			}

			return $this->redirect(['index']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->scenario = User::SCENARIO_ADMIN_UPDATE;

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully changed'));

			if(isset($_GET['wait'])){
				return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
			}

			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionDelete($id){

		$this->findModel($id)->delete();

		if (!Yii::$app->request->isAjax) {
			return $this->redirect(['index']);
		}
	}

	protected function findModel($id){

		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
