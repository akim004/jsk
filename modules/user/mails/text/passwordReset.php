<?php
use app\modules\user\Module;
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/password-reset', 'token' => $user->password_reset_token]);
?>

<?= Module::t('module', 'Hello {username}', ['username' => $user->username]); ?>

<?= Module::t('module', 'Follow_to_reset_password') ?>

<?= $resetLink ?>