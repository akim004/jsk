<?php

return [

	'Hello {username}' => 'Здравствуйте, {username}!',
	'Thank you for signing up on {0}' => 'Благодарим Вас за регистрацию на {0}',
	'In order to complete your registration, please click the link below' => 'Для завершения регистрации, пожалуйста, перейдите по ссылке ниже',
	'If you cannot click the link, please try pasting the text into your browser' => 'Если вы не можете нажать на ссылку, пожалуйста, попробуйте вставить текст в вашем браузере',
	'If you did not make this request you can ignore this email' => 'Если вы не отправляли такой запрос, то можете проигнорировать это сообщение',

	'No confirmation code' => 'Отсутствует код подтверждения',
	'Invalid token' => 'Неверный токен',

	'Signup'              => 'Регистрация',
	'Success signup'      => 'Регистрация прошла успешно',
	'Error confirm email' => 'Ошибка подтверждения Email.',
	'Please fill out the following fields to signup' => 'Пожалуйста, заполните следующие поля для регистрации',
	'A message has been sent to your email address. It contains a confirmation link that you must click to complete registration.' => 'Вам было отправлено письмо. Оно содержит ссылку, по которой вы должны перейти, чтобы завершить регистрацию.',
	'Success confirm email' => 'Спасибо! Ваш Email успешно подтверждён. Можете зайти под свои логин-паролем',

	'Id'         => 'Id',
	'Created_at' => 'Дата создания',
	'Updated_at' => 'Дата обновления',
	'Username'   => 'Логин',
	'Email'      => 'Email',
	'Status'     => 'Статус',
	'Role'       => 'Роль',

	'Password'    => 'Пароль',
	'Verify code' => 'Код',

	'Invalid login or password' => 'Неправильный логин или пароль',
	'Your account has been blocked.' => 'Ваш аккаунт был заблокирован',
	'You need to confirm your email address.' => 'Вы должны активировать аккаунт.',

	'Login'    => 'Вход',
	'Reset it' => 'Восстановить',
	'Send'     => 'Отправить',
	'Remember me'     => 'Запомнить меня',

	'Please fill out the following fields to login'                           => 'Пожалуйста, заполните следующие поля для входа',
	'If you forgot your password you can'                                     => 'Если забыли пароль, то можете ',

	'Error user not found by email' => 'Пользователь с таким адресом не найден.',
	'Error token is sent' => 'Токен уже отправлен.',

	'We have received a request to reset the password for your account on {0}' => 'Вы запросили смену пароля на сайте {0}',
	'Please click the link below to complete your password reset' => 'Чтобы восстановить пароль, нажмите на ссылку ниже',

	'Request password reset' => 'Сброс пароля',
	'Please fill out your email. A link to reset password will be sent there' => 'Пожалуйста, введите ваш email. На почту будет отправлена ссылка для восстановления пароля',

	'Flash password reset error' => 'Извините. У нас возникли проблемы с отправкой.',
	'Flash password reset request' => 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.',
	'Flash password reset success' => 'Спасибо! Пароль успешно изменён.',

	'User new password' => 'Новый пароль',
	'Reset password' => 'Восстановление пароля',
	'Please choose your new password' => 'Пожалуйста введите новый пароль',
	'Save' => 'Сохранить',

	'New password'             => 'Новый пароль',
	'New password repeat'      => 'Повторите пароль',
	'Current password'         => 'Текущий пароль',
	'Invalid current password' => 'Неверный текущий пароль.',

	'Password has been changed' => 'Пароль был изменен',

	'Create user' => 'Создать пользователя',
	'Users'       => 'Пользователи',
	'User'        => 'Пользователь',
	'Update user' => 'Изменить данные пользователя',


	'successfully added' => 'Пользователь успешно добавлен',
	'successfully changed' => 'Пользователь успешно изменен',


	'Delete'      => 'Удалить',
	'Update'      => 'Изменить',

	'Create' => 'Создать',
	'Apply'  => 'Сохранить',
	'Cancel' => 'Отменить',
	'Update and close' => 'Сохранить и выйти',

	'Password repeat' => 'Повторите пароль',

	/*
	'Password successfully changed'                                           => 'Спасибо! Пароль успешно изменён.',

	'Are you sure you want to delete this item?'                              => 'Вы уверены, что хотите удалить?',





	'This login already exists' => 'Такой логин уже существует',
	'This email already exists' => 'Такое email уже существует',*/
];