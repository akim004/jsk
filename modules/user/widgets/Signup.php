<?php
namespace app\modules\user\widgets;

use Yii;
use app\modules\user\models\frontend\SignupForm;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class Signup extends Widget
{
	public function init()
	{
		parent::init();

	}

	public function run()
	{
		$model = \Yii::createObject(SignupForm::className());

		echo $this->render('signup', [
			'model' => $model,
		]);
	}

}