<?php
namespace app\modules\user\widgets;

use Yii;
use app\modules\user\models\frontend\LoginForm;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class Login extends Widget
{
	public function init()
	{
		parent::init();

	}

	public function run()
	{
		$model = \Yii::createObject(LoginForm::className());

		echo $this->render('login', [
			'model' => $model,
		]);
	}

}