<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/user/migrations --interactive=0
 *
 * composer require 2amigos/yii2-editable-widget:~1.0
 * composer require 2amigos/yii2-grid-view-library "*"
 * composer require kartik-v/yii2-widget-datepicker "@dev"
 * composer require kartik-v/yii2-field-range "*"
 *
 */

namespace app\modules\user;

use Yii;
use app\modules\user\Mailer;

class Module extends \app\components\Module
{

	/**
	 * @var boolean If true after registration user will be required to confirm his e-mail address.
	 */
	public $requireEmailConfirmation = true;

	public $checkCaptcha = false;

	public $robotName;

	// остаться в системе
	public $rememberFor = 2592000; //3600*24*30

	public $passwordResetTokenExpire = 3600;

	/**
	 * @var string E-mail address from that will be sent the module messages
	 */
	public $robotEmail;

	public $controllerNamespace = 'app\modules\user\controllers';

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/user/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [
			'<_action:(login|logout|signup|email-confirm|request-password-reset|password-reset)>' => 'user/default/<_action>',
		];
	}

	protected function getMailer()
    {
        return \Yii::$container->get(Mailer::className());
    }
}
