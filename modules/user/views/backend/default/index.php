<?php

use app\components\grid\LinkColumn;
use app\components\grid\SetColumn;
use app\modules\user\Module;
use app\modules\user\components\UserStatusColumn;
use app\modules\user\models\backend\User;
use dosamigos\grid\EditableColumn;
use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?if(Yii::$app->user->can('admin')){?>
	<p>
		<?= Html::a(Module::t('module', 'Create user'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
<?}?>

	<?php \yii\widgets\Pjax::begin([
		'id' => 'pjax-container',
		'timeout' => 20000,
		// 'enablePushState' => false,
	]); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'class' => LinkColumn::className(),
				'attribute' => 'username',
			],

			// 'id',
		   /* [
				'class'     => \dosamigos\grid\EditableColumn::className(),
				'type'      => 'textarea',
				'url'       => ['editable'],
				'attribute' => 'description',
			],*/
			'email:email',
			/*[
				'class' => SetColumn::className(),
				'filter' => User::getStatusesArray(),
				'attribute' => 'status',
				'name' => 'statusName',
				'cssCLasses' => [
					User::STATUS_ACTIVE  => 'success',
					User::STATUS_WAIT    => 'warning',
					User::STATUS_BLOCKED => 'default',
				],
			],*/
			 /*[
				'filter' => DatePicker::widget([
					'model' => $searchModel,
					'attribute' => 'date_from',
					'attribute2' => 'date_to',
					'type' => DatePicker::TYPE_RANGE,
					'separator' => '-',
					'pluginOptions' => ['format' => 'yyyy-mm-dd']
				]),
				'attribute' => 'created_at',
				'format' => 'datetime',

			],*/
			[
				'class'     => app\components\EditableColumn::className(),
				'type'      => 'select',
				'url'       => ['editable'],
				'attribute' => 'role',
				'format' => 'raw',
				'filter'    => User::getRolesArray(),

				'value' => function($data) {
					return User::getRolesArray()[$data->role];
				},
				'editableOptions' => [
					// 'value' => 0,
					'mode' => 'pop', // 'inline'
					'source' => json_encode(User::getRolesArrayForSource()),
					'placement' => 'bottom',
				],
				'visible' => Yii::$app->user->can('admin') ? true : false,
			],
			[
				'class'     => app\components\EditableColumn::className(),
				'type'      => 'select',
				'url'       => ['editable'],
				'attribute' => 'status',
				'format' => 'raw',
				'filter'    => User::getStatusesArray(),

				'value' => function($data) {
					return User::getStatusesArray()[$data->status];
				},
				'editableOptions' => [
					'value' => 0,
					'mode' => 'pop', // 'inline'
					'source' => json_encode(User::getStatusesArrayForSource()),
					'placement' => 'bottom',
				],
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}{delete}',
				'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
				'visible' => Yii::$app->user->can('admin') ? true : false,
				'buttons' => [
					'delete' => function ($url) {
						return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
							'title' => Module::t('module', 'Delete'),
							'aria-label' => Module::t('module', 'Delete'),
							'onclick' => "
								if (confirm('Удалить пользователя?')) {
									$.ajax('$url', {
										type: 'POST'
									}).done(function(data) {

										$.pjax.reload({container: '#pjax-container', timeout: 10000});
									});
								}
								return false;
							",
						]);
					},
				],
			],

		],
	]); ?>
	<?php \yii\widgets\Pjax::end(); ?>

</div>
