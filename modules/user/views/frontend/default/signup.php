<?php

use app\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title =  Module::t('module', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin([
	'action' => Url::to(['/user/default/signup']),
	'id' => 'js-signup-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
]); ?>

	<div class="title">
		Регистрация
	</div>

	<div class="item">
		<div class="level">Логин</div>
		<div class="value">
			<?= $form->field($model, 'username')->label(false) ?>
		</div>
	</div>

	<div class="item">
		<div class="level">Пароль</div>
		<div class="value">
			<?= $form->field($model, 'password')->passwordInput()->label(false) ?>
		</div>
	</div>

	<div class="item">
		<div class="level">Повторите пароль</div>
		<div class="value">
			<?= $form->field($model, 'password_repeat')->passwordInput()->label(false) ?>
		</div>
	</div>

	<div class="bt">
		<?= Html::submitButton('Зарегистрироваться', ['class' => 'ladda-button', 'data-style' => 'slide-right']) ?>
	</div>
<?php ActiveForm::end(); ?>
