<?php
use app\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\LoginForm */

$this->title = Module::t('module', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-login">
	<h1><?= Html::encode($this->title) ?></h1>

	<p><?=Module::t('module', 'Please fill out the following fields to login');?>:</p>

	<div class="row">
		<div class="col-lg-5">
			<?php $form = ActiveForm::begin([
				'id' => 'js-login-form',
				'enableAjaxValidation' => true,
			]); ?>
			<?= $form->field($model, 'username') ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
			<?= $form->field($model, 'rememberMe')->checkbox() ?>
			<div style="color:#999;margin:1em 0">
				<?=Module::t('module', 'If you forgot your password you can');?> <?= Html::a(Module::t('module', 'Reset it'), ['password-reset-request']) ?>.
			</div>
			<div class="form-group">
				<?= Html::submitButton(Module::t('module', 'Login'), ['class' => 'ladda-button btn btn-primary', 'data-style' => 'slide-right']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>