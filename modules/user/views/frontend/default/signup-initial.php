<?php

use app\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$this->title =  Module::t('module', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-signup">
	<h1><?= Html::encode($this->title) ?></h1>
	<p><?=Module::t('module', 'Please fill out the following fields to signup');?>:</p>

	<div class="row">
		<div class="col-lg-5">
			<?php $form = ActiveForm::begin([
				'id' => 'js-signup-form',
				'enableAjaxValidation' => true,
				'enableClientValidation' => true,
			]); ?>
			<?= $form->errorSummary($model); ?>
			<?= $form->field($model, 'username') ?>
			<?= $form->field($model, 'email') ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
			<?if(\Yii::$app->getModule('user')->checkCaptcha){?>
				<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
					'captchaAction' => '/user/default/captcha',
					'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
				]) ?>
			<?}?>
			<div class="form-group">
				<?= Html::submitButton(Module::t('module', 'Signup'), ['class' => 'ladda-button btn btn-primary', 'data-style' => 'slide-right']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>