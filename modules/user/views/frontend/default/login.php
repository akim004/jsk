<?php
use app\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\LoginForm */

$this->title = Module::t('module', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin([
	'action' => Url::to(['/user/default/login']),
	'id' => 'js-login-form',
	'enableAjaxValidation' => true,
]); ?>
	<div class="title">
		Вход
	</div>

	<div class="box">
		<div class="item">
			<div class="level">Логин</div>
			<div class="value">
				<?= $form->field($model, 'username')->label(false)?>
			</div>
		</div>

		<div class="item">
			<div class="level">Пароль</div>
			<div class="value">
				<?= $form->field($model, 'password')->passwordInput()->label(false)?>
			</div>
		</div>
	</div>

	<div class="bt">
		<div class="text">
			<label>
				<?= $form->field($model, 'rememberMe')->checkbox() ?>
			</label>
		</div>

		<?= Html::submitButton(Module::t('module', 'Login'), ['class' => 'ladda-button','data-style' => 'slide-right']) ?>
		<a href="<?=Url::to(['/user/default/signup'])?>" class="button open-register">Зарегистрироваться</a>
	</div>
<?php ActiveForm::end(); ?>
