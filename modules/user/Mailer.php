<?php

namespace app\modules\user;

use Yii;
use app\modules\user\models\common\User;
use yii\base\Component;

/**
 * Mailer.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Mailer extends Component
{
    use \app\modules\user\traits\ModuleTrait;
    /** @var string */
    public $viewPath = '@app/modules/user/mails/';

    /** @var string|array Default: `Yii::$app->params['adminEmail']` OR `no-reply@example.com` */
    public $sender;

    /**
     * Sends an email to a user after registration.
     *
     * @param User  $user
     * @param Token $token
     * @param bool  $showPassword
     *
     * @return bool
     */
    public function sendConfirmEmailMessage(User $user)
    {
        return $this->sendMessage(
            $user->email,
            'Confirm email',
            'emailConfirm',
            ['user' => $user, 'module' => $this->getModule()]
        );
    }

    public function sendPasswordResetMessage(User $user)
    {
        return $this->sendMessage(
            $user->email,
            'Password reset email',
            'passwordReset',
            ['user' => $user, 'module' => $this->getModule()]
        );
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $view
     * @param array  $params
     *
     * @return bool
     */
    protected function sendMessage($to, $subject, $view, $params = [])
    {
        /** @var \yii\mail\BaseMailer $mailer */
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = $this->viewPath;
        $mailer->getView()->theme = Yii::$app->view->theme;

        if ($this->sender === null) {
            $this->sender = isset($this->module->robotEmail) ? $this->module->robotEmail : 'no-reply@example.com';
        }

        return $mailer->compose(['html' => $view, 'text' => 'text/' . $view], $params)
            ->setTo($to)
            ->setFrom($this->sender)
            ->setSubject($subject)
            ->send();
    }
}
