<?php

namespace app\modules\catalog;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	use \app\modules\catalog\traits\ModuleTrait;

	public function bootstrap($app)
	{
		$app->i18n->translations['modules/catalog/*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'forceTranslation' => true,
			'basePath' => '@app/modules/catalog/messages',
			'fileMap' => [
				'modules/catalog/module' => 'module.php',
			],
		];

		$app->urlManager->registerModuleRules($this->getModule());
	}
}