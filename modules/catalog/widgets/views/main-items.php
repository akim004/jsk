
<?if($topItems || $discountItems){?>
	<div class="tab tab-switch">
		<?if($topItems){?>
			<a href="" class="active" data-id="1">Популярное</a>
		<?}?>
		<?if($discountItems){?>
			<a href="" class="<?=!$topItems ? 'active' : '';?>" data-id="2">Акции</a>
		<?}?>
	</div>
<?}?>

<?if($topItems){?>
	<ul class="product-list tab-box" data-id="1">
		<?foreach ($topItems as $item) {?>
			<li>
				<a href="">
					<span class="img">
						<span><img src="uploads/prod-promo-1.jpg" alt=""/></span>
					</span>
					<span class="entry">
						<span class="id">
							11ag52s
						</span>
						<span class="text">
							Фасад глухой
						</span>
						<span class="title">
							Афина аргенто
						</span>
						<span class="price">
							1 550 Р
						</span>
					</span>
				</a>
		<?}?>
	</ul>
<?}?>

<?if($discountItems){?>
	<ul class="product-list tab-box <?=$topItems ? 'hidden' : '';?>" data-id="2">
		<?foreach ($discountItems as $item) {?>
			<li>
				<a href="">
					<span class="img">
						<span><img src="uploads/prod-promo-1.jpg" alt=""/></span>
					</span>
					<span class="entry">
						<span class="id">
							11ag52s
						</span>
						<span class="text">
							Фасад глухой
						</span>
						<span class="title">
							Афина аргенто
						</span>
						<span class="price">
							1 550 Р
						</span>
					</span>
				</a>
		<?}?>
	</ul>
<?}?>