<ul>
	<li>
		<a href="">Коллекции фасадов <span>+</span></a>
		<ul>
			<li class="type-1">
				<a href="">
					<span class="img">
						<img src="uploads/prod-coll-1.jpg" alt=""/>
					</span>
					<span class="entry">
						<span class="title">
							Прованс
						</span>
						<span class="in">
							<span>в каталог</span>
						</span>
					</span>
				</a>
			</li>
			<li class="type-2">
				<a href="">
					<span class="img">
						<img src="uploads/prod-coll-2.jpg" alt=""/>
					</span>
					<span class="entry">
						<span class="title">
							Модерн
						</span>
						<span class="in">
							<span>в каталог</span>
						</span>
					</span>
				</a>
			</li>
			<li class="type-3">
				<a href="">
					<span class="img">
						<img src="uploads/prod-coll-3.jpg" alt=""/>
					</span>
					<span class="entry">
						<span class="title">
							Классика
						</span>
						<span class="in">
							<span>в каталог</span>
						</span>
					</span>
				</a>
			</li>
		</ul>
	</li>
	<?if($catalogs){?>
		<?foreach ($catalogs as $catalog) {?>
			<li><a href="<?=$catalog->createUrl?>"><?=$catalog->name?></a></li>
		<?}?>
	<?}?>
</ul>

