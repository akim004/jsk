<?php
namespace app\modules\catalog\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use \app\modules\catalog\models\common\Item;

class MainItems extends Widget
{
	public $topPageSize = 1;
	public $discountPageSize = 1;

	public function init()
	{
		parent::init();

	}

	public function run()
	{
		$topItems = Item::find()->active()->top()->limit($this->topPageSize)->all();
		$discountItems = Item::find()->active()->discount($this->discountPageSize)->all();

		return $this->render('main-items', [
			'topItems' => $topItems,
			'discountItems' => $discountItems,
		]);
	}
}