<?php
namespace app\modules\catalog\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use \app\modules\catalog\models\common\Catalog;

class CatalogView extends Widget
{
	public function init()
	{
		parent::init();

	}

	public function run()
	{
		$catalogs = Catalog::find()->active()->roots()->all();

		return $this->render('catalog', [
			'catalogs' => $catalogs,
		]);
	}

	public static function getTreeView($activeId = null)
	{
		$roots = Catalog::find()->active()->roots()->all();
		$level=0;

		echo Html::beginTag('ul');
		foreach ($roots as $root) {
			$level=0;
			echo Html::beginTag('li');
			echo Html::a($root->name,
								[$root->createUrl],
								['class' => $root->id == $activeId ? 'active' : '']
						);

			foreach($root->children()->active()->all() as $n=>$category)
			{
				if($category->level == $level){
					echo Html::endTag('li')."\n";
				}else if($category->level>$level){
					echo Html::tag('span', '', ['class' => 'has-children fa fa-chevron-right'])."\n";
					echo Html::beginTag('ul'/*, ['style' => 'display:none;']*/)."\n";
				}else{
					echo Html::endTag('li')."\n";

					for($i = $level-$category->level; $i; $i--){
						echo Html::endTag('ul')."\n";
						echo Html::endTag('li')."\n";
					}
				}

				echo Html::beginTag('li');
				echo Html::a($category->name, [$category->createUrl], ['class' => $category->id == $activeId ? 'active' : '']);
				$level=$category->level;
			}

			for($i=$level;$i;$i--)
			{
				echo Html::endTag('li')."\n";
				echo Html::endTag('ul')."\n";
			}
			echo Html::endTag('li')."\n";
		}
		echo Html::endTag('ul')."\n";
	}

}