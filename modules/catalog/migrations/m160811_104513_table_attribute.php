<?php

use yii\db\Migration;
use yii\db\Schema;

class m160811_104513_table_attribute extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%attribute}}', [
			'id'        => Schema::TYPE_PK,
			'name'      => Schema::TYPE_STRING . '(255) NOT NULL',
			'type'      => Schema::TYPE_INTEGER . '(1) NOT NULL',
			'parent_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'position'  => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createIndex('idx-attribute-name', '{{%attribute}}', 'name');


		$this->createTable('{{%attribute_group}}', [
			'id'       => Schema::TYPE_PK,
			'name'     => Schema::TYPE_STRING . '(255) NOT NULL',
			'position' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);


		$this->createTable('{{%item_attribute}}', [
			'item_id'      => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'attribute_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'value'        => Schema::TYPE_STRING . '(255) NOT NULL',
		], $tableOptions);

		$this->createIndex('idx-item-attribute-item-id', '{{%item_attribute}}', 'item_id');
		$this->createIndex('idx-item-attribute-attribute-id', '{{%item_attribute}}', 'attribute_id');
	}

	public function down()
	{
		$this->dropTable('{{%attribute}}');
		$this->dropTable('{{%attribute_group}}');
		$this->dropTable('{{%item_attribute}}');
	}
}
