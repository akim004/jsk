<?php

use yii\db\Migration;
use yii\db\Schema;

class m160809_112821_table_catalog extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%catalog}}', [
            'id'          => Schema::TYPE_PK,
            'name'        => Schema::TYPE_STRING . '(128) NOT NULL',
            'slug'        => Schema::TYPE_STRING . '(128) NOT NULL',
            'description' => Schema::TYPE_TEXT ,
            'position'    => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
            'status'      => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0',
            'lft'         => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'rgt'         => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'level'       => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'root'        => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('idx-catalog-name', '{{%catalog}}', 'name');
        $this->createIndex('idx-catalog-slug', '{{%catalog}}', 'slug');
    }

    public function down()
    {
        $this->dropTable('{{%catalog}}');
    }
}
