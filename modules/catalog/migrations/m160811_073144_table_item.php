<?php

use yii\db\Migration;
use yii\db\Schema;

class m160811_073144_table_item extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%item}}', [
			'id'          => Schema::TYPE_PK,
			'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'article'     => Schema::TYPE_STRING . '(128) NOT NULL',
			'slug'        => Schema::TYPE_STRING . '(128) NOT NULL',
			'created_at'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'updated_at'  => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'description' => Schema::TYPE_TEXT ,
			'position'    => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'status'      => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0',
			'top'         => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0',
			'discount'    => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0',
			'price'       => Schema::TYPE_INTEGER . '(11) DEFAULT 0',
		], $tableOptions);

		$this->createIndex('idx-item-name', '{{%item}}', 'name');
		$this->createIndex('idx-item-slug', '{{%item}}', 'slug');
		$this->createIndex('idx-item-article', '{{%item}}', 'article');


		$this->createTable('{{%catalog_item}}', [
			'catalog_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'item_id'    => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%item}}');
		$this->dropTable('{{%catalog_item}}');
	}
}
