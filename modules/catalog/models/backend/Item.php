<?php

namespace app\modules\catalog\models\backend;

use app\modules\catalog\Module;
use app\modules\catalog\models\backend\ItemAttribute;
use app\modules\catalog\models\common\CatalogItem;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class Item extends \app\modules\catalog\models\common\Item
{

	public $categoryes;
	public $mattributes;

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			[
				'class' => 'Zelenin\yii\behaviors\Slug',
				'attribute' => 'name',
				'slugAttribute' => 'slug',
				'ensureUnique' => true, // уникальность в базе
				'replacement' => '-',
				'lowercase' => true,
				'immutable' => true,
				// 'translit' => true,
				// If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
				'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
			],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['categoryes', 'mattributes'], 'safe']
		]);
	}

	public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'categoryes' => Module::t('module','Categoryes'),
		]);
	}

	public function search($params)
	{
		$query = self::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> [
				'defaultOrder' => ['position' => SORT_ASC],
			]
		]);

		if (!$this->load($params)) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id'       => $this->id,
			'position' => $this->position,
			'status'   => $this->status,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'slug', $this->slug]);

		return $dataProvider;
	}

	protected function saveCategoryes()
	{
		CatalogItem::deleteAll('item_id = :id', [':id' => $this->id]);
		if($this->categoryes){
			foreach ($this->categoryes as $data) {
				$catItem = new CatalogItem();
				$catItem->item_id = $this->id;
				$catItem->catalog_id = $data;
				$catItem->save();
			}
		}
	}

	public function getRelationsCatalogItem()
	{
		return $this->hasMany(CatalogItem::className(), ['item_id' => 'id']);
	}

	protected function saveAttributes()
	{
		ItemAttribute::deleteAll('item_id = :id', [':id' => $this->id]);

		if($this->mattributes){
			foreach ($this->mattributes as $key => $data) {
				if($data){
					$itemAttr = new ItemAttribute();
					$itemAttr->item_id = $this->id;
					$itemAttr->attribute_id = $key;
					$itemAttr->value = $data;
					$itemAttr->save(false);
				}
			}
		}
	}

	public function beforeSave($insert)
	{
	    if (parent::beforeSave($insert)) {

	        $this->saveCategoryes();
	        $this->saveAttributes();

	        return true;
	    }
	    return false;
	}

	public function getAttributeValue($attributeId)
	{
		$model = \app\modules\catalog\models\backend\ItemAttribute::find()->where(['item_id' => $this->id, 'attribute_id' => $attributeId])->one();

		return $model ? $model->value : '';
	}
}
