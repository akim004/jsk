<?php

namespace app\modules\catalog\models\backend;

use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class Catalog extends \app\modules\catalog\models\common\Catalog
{
	/**
	 * Загружаемая картинка
	 * @var UploadedFile
	 */
	public $imageUpload;

	/**
	 * Флаг проверки удалить ли картинку
	 * @var bool
	 */
	public $isRemoveImage;

	public $parentId;

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			[
				'class' => 'Zelenin\yii\behaviors\Slug',
				'attribute' => 'name',
				'slugAttribute' => 'slug',
				'ensureUnique' => true, // уникальность в базе
				'replacement' => '-',
				'lowercase' => true,
				'immutable' => true,
				// 'translit' => true,
				// If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
				'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
			],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['parentId', 'isRemoveImage'], 'safe'],
		]);
	}
	/**
	 * Хлебные крошки в адмике
	 * @return [type] [description]
	 */
	public function getAdminBreadcumbs()
	{
		$parents = $this->parents()->orderBy('level ASC')->all();
		foreach ($parents as $data) {
			$breadcrumbs[] = ['label' => $data->name, 'url' => $data->adminCreateUrl];
		}
		$breadcrumbs[] = ['label' => $this->name, 'url' => $this->adminCreateUrl];

		return $breadcrumbs;
	}

	public function getAdminCreateUrl()
	{
		return Url::toRoute(['/admin/catalog','parent'=>$this->id]);
	}

	public function search($params)
	{
		$query = self::find();

		if(isset($params['parent']) AND ($parent = self::findOne(['id'=>$params['parent']]))){
			$childrenIds = $parent->children(1)->select('id')->asArray();
			$query->where(['id'=>$childrenIds]);
		}else{
			$roots = self::find()->roots()->select('id')->asArray()->all();
			$query->where(['id'=>$roots]);
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> [
				'defaultOrder' => ['position' => SORT_ASC],
			]
		]);

		if (!$this->load($params)) {
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id'       => $this->id,
			'position' => $this->position,
			'status'   => $this->status,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'slug', $this->slug]);


		return $dataProvider;
	}

	public function saveImage($file)
	{
		$image = $this->getImage();

		if($this->isRemoveImage && $image){
			$this->removeImage($image);
		}

		if($file){
			if($image){
				$this->removeImage($image);
			}

			$file->saveAs('uploads/runtime/'.$file->name);
			$this->attachImage('uploads/runtime/'.$file->name);
			@unlink('uploads/runtime/'.$file->name);
		}
	}

	/**
	 * Если есть родитель, то добавляем в родитель
	 * иначе создаем новое дерево
	 * @param  integer $parentID [description]
	 * @return [type]            [description]
	 */
	public function saveNav($parentID = 0)
	{
		$parent = self::findOne(['id'=>$parentID]);
		return $parent ? $this->appendTo($parent) : ($this->isNewRecord ? $this->makeRoot() : $this->save());
	}

	public function afterDelete() {
		parent::afterDelete();

		if($this->getImage()){
			$this->removeImage($this->getImage());
		}
	}
}
