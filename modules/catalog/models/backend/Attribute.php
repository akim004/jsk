<?php

namespace app\modules\catalog\models\backend;

use Yii;
use yii\helpers\ArrayHelper;

class Attribute extends \app\modules\catalog\models\common\Attribute
{
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}
}
