<?php

namespace app\modules\catalog\models\backend;

use Yii;
use app\modules\catalog\Module;
use yii\helpers\ArrayHelper;


class AttributeGroup extends \app\modules\catalog\models\common\AttributeGroup
{
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}
}
