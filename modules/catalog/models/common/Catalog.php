<?php

namespace app\modules\catalog\models\common;

use Yii;
use app\modules\catalog\Module;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class Catalog extends ActiveRecord
{
	const ACTIVE = 1;
	const NOT_ACTIVE = 0;


	public static function tableName()
	{
		return '{{%catalog}}';
	}

	public function behaviors()
	{
		return [
			['class' => \app\behaviors\RouteBehavior::className()],
			['class' => \app\behaviors\SeoBehavior::className()],
			['class' => \app\behaviors\SitemapBehavior::className()],
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 1,
				/*'maxImageWidth' => '1000',
				'maxImageHeight' => '1000',
				'maxSize' => '5',*/
			],
			'tree' => [
				'class' => NestedSetsBehavior::className(),
				'treeAttribute'  => 'root',
				'leftAttribute'  => 'lft',
				'rightAttribute' => 'rgt',
				'depthAttribute' => 'level',
			],
		];


	}

	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_ALL,
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE     => Module::t('module', 'Active'),
			self::NOT_ACTIVE => Module::t('module', 'Hidden'),
		];
	}

	public static function find()
	{
		return new CatalogQuery(get_called_class());
	}

	public function rules()
	{
		return [
			[['name'], 'required'],
			[['name', 'slug'], 'string', 'max' => 255],
			[['position', 'description'], 'safe'],

			[['status'], 'integer', 'integerOnly' => true],

			[['name', 'slug'], 'safe', 'on' => 'search'],

		];
	}

	public function attributeLabels(){

		return [
			'id'          => Module::t('module','Id'),
			'name'        => Module::t('module','Name'),
			'slug'        => Module::t('module','Slug'),
			'position'    => Module::t('module','Position'),
			'status'      => Module::t('module','Status'),
			'imageUpload' => Module::t('module','ImageUpload'),
			'parentId'    => Module::t('module','ParentId'),
		];
	}

	public function scenarios(){
		return Model::scenarios();
	}

	// Родитель
	public function getParent()
	{
		return $this->isRoot() ? 0 : $this->parents(1)->one();
	}

	/**
	 * В древовидном виде
	 * @return [type] [description]
	 */
	public static function getTree($withoutId)
	{
		$tree = [];
		$list = self::find()->roots()->all();
		foreach ($list as $data) {
			if($data->id == $withoutId){
				continue;
			}
			$tree[$data->id] = $data->name;
			foreach ($data->children()->all() as $children) {
				if($children->id == $withoutId){
					continue;
				}
				$tree[$children->id] = str_repeat("-- ", $children->level).$children->name;
			}
		}
		return $tree;
	}

	public static function getListData($modelId = null)
	{
		$tree =self::getTree($modelId);

		return $tree;
	}

	public function getCatalogItems()
	{
		 return $this->hasMany(Item::className(), ['id' => 'item_id'])
      			->viaTable('catalog_item', ['catalog_id' => 'id']);
	}

	public function getImageUrl($size = '')
	{
		return $this->image ? $this->image->getPath($size) : '';
	}

	public function getCreateUrl()
	{
		return  Url::to(['/catalog/default/view',
					'slug' => $this->slug
				]);
	}
}
