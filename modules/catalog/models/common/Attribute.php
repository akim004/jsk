<?php

namespace app\modules\catalog\models\common;

use Yii;
use app\modules\catalog\Module;

/**
 * This is the model class for table "{{%attribute}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $position
 */
class Attribute extends \yii\db\ActiveRecord
{
    const TYPE_STRING = 1;
    const TYPE_TEXT = 2;
    const TYPE_CHECKBOX = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attribute}}';
    }

    public static function getTypeArray()
    {
        return [
            self::TYPE_STRING   => 'Поле ввода',
            self::TYPE_TEXT     => 'TextArea',
            self::TYPE_CHECKBOX => 'Checkbox',
        ];
    }

    public static function getTypeInput($type)
    {
        switch ($type) {
            case self::TYPE_STRING:
                return 'text';
                break;
            case self::TYPE_TEXT:
                return 'text';
                break;
            case self::TYPE_CHECKBOX:
                return 'checkbox';
                break;
            default:
                return 'text';
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['position', 'parent_id', 'type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Module::t('module', 'ID'),
            'name'      => Module::t('module', 'Name'),
            'type'      => Module::t('module', 'Type'),
            'parent_id' => Module::t('module', 'Parent'),
            'position'  => Module::t('module', 'Position'),
        ];
    }
}
