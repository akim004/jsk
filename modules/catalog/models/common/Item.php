<?php

namespace app\modules\catalog\models\common;

use Yii;
use app\modules\catalog\Module;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class Item extends ActiveRecord implements \yz\shoppingcart\CartPositionProviderInterface
{
	public function getCartPosition($params = [])
    {
        return \Yii::createObject([
            'class' => '\app\modules\shop\models\common\ProductCartPosition',
            'id' => md5(serialize(['1', '555', 'черный'])),
        ]);
    }

	/**
	 * Загружаемая картинка
	 * @var UploadedFile
	 */
	public $imageUpload;

	const ACTIVE = 1;
	const NOT_ACTIVE = 0;


	public static function tableName()
	{
		return '{{%item}}';
	}

	public function behaviors()
	{
		return [
			['class' => \app\behaviors\SeoBehavior::className()],
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 1,
				'maxImageWidth' => '1500', // @todo ругается на Yii::$app->getModule("image"), не смог сериализовать
				'maxImageHeight' => '1500', // при добавлении в корзину, пока костыль явно указываем размеры изображения
				'maxSize' => '5',
			],
		];


	}

	public static function find()
	{
		return new ItemQuery(get_called_class());
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE => Module::t('module', 'Active'),
			self::NOT_ACTIVE  => Module::t('module', 'Hidden'),
		];
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::ACTIVE,
				'text' => Module::t('module', 'Active'),
			],
			[
				'value' => self::NOT_ACTIVE,
				'text' => Module::t('module', 'Hidden'),
			],
		];
	}

	public function rules()
	{
		return [
			[['name'], 'required'],
			[['name', 'slug', 'article'], 'string', 'max' => 255],
			[['position', 'description'], 'safe'],
			[['top', 'discount'], 'boolean'],

			[['status', 'price'], 'integer', 'integerOnly' => true],

			[['name', 'slug'], 'safe', 'on' => 'search'],

		];
	}

	public function attributeLabels(){

		return [
			'id'          => Module::t('module','Id'),
			'name'        => Module::t('module','Name'),
			'slug'        => Module::t('module','Slug'),
			'position'    => Module::t('module','Position'),
			'status'      => Module::t('module','Status'),
			'article'     => Module::t('module','Article'),
			'price'       => Module::t('module','Price'),
			'imageUpload' => Module::t('module','ImageUpload'),
			'top'         => Module::t('module','Top item'),
			'discount'    => Module::t('module','Discount item'),
		];
	}

	public function afterDelete() {
		parent::afterDelete();
		$this->deleteImages();
	}

	protected function deleteImages()
	{
		if($this->getImages()[0]){
			foreach ($this->getImages() as $image) {
				$this->removeImage($image);
			}
		}
	}

	public function saveImages($files)
	{
		if($files){
			foreach ($files as $file) {
				$file->saveAs('uploads/runtime/'.$file->name);
				$this->attachImage('uploads/runtime/'.$file->name);
				@unlink('uploads/runtime/'.$file->name);
			}
		}
	}
}
