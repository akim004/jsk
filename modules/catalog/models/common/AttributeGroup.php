<?php

namespace app\modules\catalog\models\common;

use Yii;
use app\modules\catalog\Module;
use app\modules\catalog\models\common\Attribute;

/**
 * This is the model class for table "{{%attribute_group}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $position
 */
class AttributeGroup extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%attribute_group}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['position'], 'integer'],
			[['name'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'       => Module::t('module', 'ID'),
			'name'     => Module::t('module', 'Name'),
			'position' => Module::t('module', 'Position'),
		];
	}

	public static function getList()
	{
		return self::find()->select(['name','id'])->indexBy('id')->column();
	}

	public function getMAttrubutes()
	{
		 return $this->hasMany(Attribute::className(), ['parent_id' => 'id'])->orderBy('position ASC');
	}
}
