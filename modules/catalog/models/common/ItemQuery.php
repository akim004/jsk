<?php

namespace app\modules\catalog\models\common;

use app\modules\catalog\models\common\Item;

class ItemQuery extends \yii\db\ActiveQuery
{
    public function active()
	{
		return $this->andWhere(['status' => Item::ACTIVE]);
	}

	public function top()
	{
		return $this->andWhere(['top' => 1]);
	}

	public function discount()
	{
		return $this->andWhere(['discount' => 1]);
	}
}