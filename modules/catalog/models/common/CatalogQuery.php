<?php

namespace app\modules\catalog\models\common;

use app\modules\catalog\models\common\Catalog;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class CatalogQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function active()
	{
		return $this->andWhere(['status' => Catalog::ACTIVE]);
	}
}