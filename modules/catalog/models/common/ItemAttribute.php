<?php

namespace app\modules\catalog\models\common;

use Yii;

/**
 * This is the model class for table "{{%item_attribute}}".
 *
 * @property integer $item_id
 * @property integer $attribute_id
 * @property string $value
 */
class ItemAttribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'attribute_id'], 'integer'],
            [['value'], 'required'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id'      => 'Item ID',
            'attribute_id' => 'Attribute ID',
            'value'        => 'Value',
        ];
    }
}
