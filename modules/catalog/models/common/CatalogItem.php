<?php

namespace app\modules\catalog\models\common;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class CatalogItem extends ActiveRecord
{
	public static function tableName()
	{
		return '{{%catalog_item}}';
	}

	public function rules()
	{
		return [
			[['catalog_id', 'item_id'], 'integer'],
		];
	}
}
