<?php

namespace app\modules\catalog\models\frontend;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class Catalog extends \app\modules\catalog\models\common\Catalog
{
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'sitemap' => [
				'class' => \himiklab\sitemap\behaviors\SitemapBehavior::className(),
				'scope' => function ($model) {
					$model->joinWith(['sitemap sitemap'])->active()->andWhere('sitemap.enable = 1');
				},
				'dataClosure' => function ($model) {
					$sitemap = \app\modules\seo\models\Sitemap::find()->where(['item_id' => $model->id, 'class' => 'Catalog'])->one();
					return [
						'loc' => $model->createUrl,
						'lastmod' => time(),
						'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
						'priority' => $sitemap ? $sitemap->priority : 0.5,
					];
				}
			],
		]);
	}

	public function search($params)
	{
		$query = self::find();

		$roots = self::find()->roots()->select('id')->asArray()->all();
		$query->where(['id'=>$roots])->active();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> [
				'defaultOrder' => ['position' => SORT_ASC],
			]
		]);

		if (!$this->load($params)) {
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id'       => $this->id,
			'position' => $this->position,
			'status'   => self::ACTIVE,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'slug', $this->slug]);


		return $dataProvider;
	}

}
