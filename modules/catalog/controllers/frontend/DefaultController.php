<?php

namespace app\modules\catalog\controllers\frontend;

use Yii;
use app\modules\catalog\models\frontend\Catalog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends \app\components\FrontendController
{
    public function actionIndex()
    {
    	$searchModel = new Catalog(['scenario' => 'search']);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'listDataProvider' => $dataProvider,
		]);
    }

    public function actionView($slug)
    {
        $catalog = $this->findModel($slug);

        return $this->render('view', [
        		'catalog' => $catalog,
        	]);
    }

    protected function findModel($slug)
	{
		if (($model = Catalog::find()->where(['slug' => $slug])->active()->one()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
