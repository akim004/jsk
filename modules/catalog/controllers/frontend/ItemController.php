<?php

namespace app\modules\catalog\controllers\frontend;

use yii\web\Controller;

class ItemController extends \app\components\FrontendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
