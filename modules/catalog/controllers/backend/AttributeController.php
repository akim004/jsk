<?php

namespace app\modules\catalog\controllers\backend;

use Yii;
use app\components\UrlP;
use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Attribute;
use app\modules\catalog\models\backend\AttributeGroup;
use app\modules\catalog\models\backend\AttributeSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AttributeController implements the CRUD actions for Attribute model.
 */
class AttributeController extends \app\components\BackendController
{
    public $breadcrumbs = [];
    public function actions()
    {
        return [
            'sort' => [
                'class' => \himiklab\sortablegrid\SortableGridAction::className(),
                'modelName' => Attribute::className(),
            ],
        ];
    }

    public function beforeAction($action)
    {
        if($parent = AttributeGroup::findOne(['id'=>Yii::$app->request->get('pid', 0)])){
            array_unshift($this->breadcrumbs, ['label' => $parent->name, 'url' => ['/admin/catalog/attribute-group']]);
        }
        return parent::beforeAction($action);
    }
    /**
     * Lists all Attribute models.
     * @return mixed
     */
    public function actionIndex()
    {
        $parentId = Yii::$app->request->get('pid');

        $searchModel = new AttributeSearch(['parent_id' => $parentId]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Attribute model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $parentID = Yii::$app->request->get('pid');

        $model = new Attribute();
        $model->parent_id = $parentID;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully attribute'));

            if(isset($_GET['wait'])){
                return $this->redirect(UrlP::toRoute(['update', 'id' => $model->id, 'wait' => 0]));
            }
            return $this->redirect(UrlP::toRoute(['index']));


        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Attribute model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully changed attribute'));

            if(isset($_GET['wait'])){
                return $this->redirect(UrlP::toRoute(['update', 'id' => $model->id, 'wait' => 0]));
            }
            return $this->redirect(UrlP::toRoute(['index']));
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Attribute model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Attribute model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attribute the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attribute::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
