<?php

namespace app\modules\catalog\controllers\backend;

use Yii;
use app\components\BackendController;
use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Catalog;
use app\modules\catalog\models\backend\Item;
use dosamigos\editable\EditableAction;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ItemController extends BackendController
{
	public $title = 'Товар';
	public $breadcrumbs = [];

	public function actions()
	{
		return [
			'editable' => [
				'class' => EditableAction::className(),
				'modelClass' => Item::className(),
				'forceCreate' => false
			],
			'editableiamge' => [
                'class' => \dosamigos\editable\EditableAction::className(),
                'modelClass' => \rico\yii2images\models\Image::className(),
                'forceCreate' => false
            ],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridAction::className(),
				'modelName' => Item::className(),
			],
		];
	}

	/*public function beforeAction($action)
	{
		if($parent = Catalog::findOne(['id'=>Yii::$app->request->get('pid', 0)])){
			$this->breadcrumbs = $parent->adminBreadcumbs;
			// array_unshift($this->breadcrumbs, ['label' => $this->title, 'url' => ['index']]);
		}
		return parent::beforeAction($action);
	}*/

	public function actionIndex()
	{
		$searchModel = new Item(['scenario'=>'search']);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate()
	{
		$model = new Item();

		if($model->load(Yii::$app->request->post())){
			$model->imageUpload = UploadedFile::getInstances($model, 'imageUpload');
			if ($model->save()) {
				$model->saveImages($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully added item'));

				if(isset($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}

				return $this->redirect(['index']);
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionFileUpload()
	{
		$modelId = Yii::$app->request->post('modelId');
		$model = $this->findModel($modelId);
		if($model){
			$model->imageUpload = UploadedFile::getInstances($model, 'imageUpload');
			$model->saveImages($model->imageUpload);
		}

		echo '{}';
		exit();
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->categoryes = ArrayHelper::map($model->relationsCatalogItem, 'catalog_id', 'catalog_id');

		if($model->load(Yii::$app->request->post())){
			$model->imageUpload = UploadedFile::getInstances($model, 'imageUpload');
			if ($model->save()) {
				$model->saveImages($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully changed item'));

				if(isset($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		if (!Yii::$app->request->isAjax) {
			return $this->redirect(['index']);
		}
	}

	public function actionDeleteImage($id)
    {
        $img = \rico\yii2images\models\Image::findOne($id);
        $model = $this->findModel($img->itemId);
        $model->removeImage($img);

		die();
        // return $this->redirect(['update', 'id' => $model->id]);
    }

	protected function findModel($id)
	{
		if (($model = Item::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
