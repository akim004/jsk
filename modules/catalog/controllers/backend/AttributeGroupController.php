<?php

namespace app\modules\catalog\controllers\backend;

use Yii;
use app\modules\catalog\Module;
use app\modules\catalog\models\backend\AttributeGroup;
use app\modules\catalog\models\backend\AttributeGroupSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AttributeController implements the CRUD actions for Attribute model.
 */
class AttributeGroupController extends \app\components\BackendController
{
    public function actions()
    {
        return [
            'sort' => [
                'class' => \himiklab\sortablegrid\SortableGridAction::className(),
                'modelName' => AttributeGroup::className(),
            ],
        ];
    }
    /**
     * Lists all AttributeGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttributeGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new AttributeGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttributeGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully attributeGroup'));

            if(isset($_GET['wait'])){
                return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
            }
            return $this->redirect(['index']);


        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AttributeGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully changed attributeGroup'));

            if(isset($_GET['wait'])){
                return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AttributeGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AttributeGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttributeGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttributeGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
