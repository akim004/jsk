<?php

namespace app\modules\catalog\controllers\backend;

use Yii;
use app\components\BackendController;
use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Catalog;
use dosamigos\editable\EditableAction;
use dosamigos\grid\ToggleAction;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class DefaultController extends BackendController
{
	public $title = 'Каталог';
	public $breadcrumbs = [];

	public function actions()
	{
		return [
			'editable' => [
				'class' => EditableAction::className(),
				'modelClass' => Catalog::className(),
				'forceCreate' => false
			],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridAction::className(),
				'modelName' => Catalog::className(),
			],
			'toggle' => [
				'class' => ToggleAction::className(),
				'modelClass' => Catalog::className(),
				'onValue' => 1,
				'offValue' => 0,
			],

		];
	}


	public function beforeAction($action)
	{
		if($parent = Catalog::findOne(['id'=>Yii::$app->request->get('parent', 0)])){
			$this->breadcrumbs = $parent->adminBreadcumbs;
		}
		array_unshift($this->breadcrumbs, ['label' => $this->title, 'url' => ['index']]);
		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		$searchModel = new Catalog(['scenario'=>'search']);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate()
	{
		$parentID = Yii::$app->request->get('parent');

		$model = new Catalog();
		$model->parentId = $parentID;

		if($model->load(Yii::$app->request->post())){
			$model->imageUpload = UploadedFile::getInstance($model, 'imageUpload');
			if ($model->saveNav($model->parentId)) {
				$model->saveImage($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully added'));

				if(isset($_GET['wait'])){
					return $this->redirect(['update', 'parent'=>$model->parentId, 'id' => $model->id, 'wait' => 0]);
				}

				return $this->redirect(['index', 'parent'=>$model->parentId]);
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->parentId = $model->parents(1)->one() ? $model->parents(1)->one()->id : 0;

		if($model->load(Yii::$app->request->post())){
			$model->imageUpload = UploadedFile::getInstance($model, 'imageUpload');
			if ($model->saveNav($model->parentId)) {
				$model->saveImage($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'Successfully changed'));

				if(isset($_GET['wait'])){
					return $this->redirect(['update', 'parent'=>$model->parentId, 'id' => $model->id, 'wait' => 0]);
				}
				return $this->redirect(['index', 'parent'=>$model->parentId]);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->deleteWithChildren();

		if (!Yii::$app->request->isAjax) {
			return $this->redirect(['index', 'parent'=>Yii::$app->request->get('parent')]);
		}

	}

	protected function findModel($id)
	{
		if (($model = Catalog::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
