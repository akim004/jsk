<?php

return [
	'Create catalog' => 'Создать каталог',
	'Catalogs' => 'Навигации',
	'Catalog' => 'Каталог',
	'Delete' => 'Удалить',
	'Update' => 'Редактировать',
	'Update catalog' => 'Редактировать каталог',
	'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить?',

	'Create' => 'Создать',
	'Apply'  => 'Сохранить',
	'Cancel' => 'Отменить',
	'Update and close' => 'Сохранить и выйти',

	'Successfully added' => 'Каталог успешно добавлен',
	'Successfully changed' => 'Каталог успешно изменен',

	'Successfully added attribute' => 'Атрибут успешно добавлен',
	'Successfully changed attribute' => 'Атрибут успешно изменен',

	'Create' => 'Создать',
	'Apply'  => 'Сохранить',
	'Cancel' => 'Отменить',
	'Update and close' => 'Сохранить и выйти',

	'id'          => 'ID',
	'Name'        => 'Название',
	'Slug'        => 'Алиас',
	'Position'    => 'Позиция в списке',
	'Status'      => 'Статус',
	'ImageUpload' => 'Картинка',
	'ParentId'    => 'Родитель',
	'Count subcategory' => 'Количество подкатегории',


	'Article' => 'Артикул',
	'Price' => 'Цена',
	'Create item' => 'Создать товар',
	'Items' => 'Товары',
	'Item' => 'Товар',
	'Update item' => 'Редактировать товар',

	'Successfully added item' => 'Товар успешно добавлен',
	'Successfully changed item' => 'Товар успешно изменен',

	'Categoryes' => 'Категории',

	'Attributes'         => 'Атрибуты',
	'AttributeGroups'         => 'Группы атрибутов',
	'Type'               => 'Тип атрибута',
	'Parent'             => 'Родитель',
	'Create Attribute'   => 'Создать атрибуты',
	'Create AttributeGroup'   => 'Создать группу атрибутов',
	'Attribute'          => 'Атрибут',
	'AttributeGroup'          => 'Группа атрибутов',
	'Update Attribute: ' => 'Редактировать атрибут: ',
	'Update AttributeGroup: ' => 'Редактировать группу атрибутов: ',

	'Active' => 'Активный',
	'Hidden' => 'Скрытый',

	'Discount item' => 'Акция',
	'Top item' => 'Популярное',
];