<?php

use app\modules\catalog\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\backend\AttributeGroup */

$this->title = Module::t('module', 'Create AttributeGroup');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'AttributeGroups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attributeGroup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
