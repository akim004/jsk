<?php

use app\modules\catalog\Module;
use app\modules\catalog\models\backend\AttributeGroup;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\catalog\models\backend\AttributeGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'AttributeGroups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attributeGroup-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('module', 'Create AttributeGroup'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'format' => 'raw',
                'attribute' => 'name',
                'value' => function($data){
                    return Html::a($data->name,\yii\helpers\Url::to(['/admin/catalog/attribute', 'pid' => $data->id]),['data-pjax' => 0]);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'urlCreator'=>function($action, $model, $key, $index){
                   return [$action,'id'=>$model->id,'parent'=>Yii::$app->request->get('pid')];
                },
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
