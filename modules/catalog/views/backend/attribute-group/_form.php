<?php

use app\modules\catalog\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\backend\Attribute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attribute-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <hr>
    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Module::t('module', 'Apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>
