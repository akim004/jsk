<?php

use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Catalog;
// use yii\grid\GridView;
use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;
// use \yiister\gentelella\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\catalog\models\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $this->context->title;

$parent = array_pop($this->context->breadcrumbs);
$this->context->breadcrumbs[] = isset($parent['label']) ? $parent['label'] : $this->title;

$this->params['breadcrumbs'] = $this->context->breadcrumbs;
?>
<div class="catalog-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a(Module::t('module', 'Create catalog'), ['create', 'parent'=>Yii::$app->request->get('parent')], ['class' => 'btn btn-success']) ?>
	</p>

	<?php \yii\widgets\Pjax::begin([
		'id' => 'pjax-container',
		'timeout' => 20000,
		// 'enablePushState' => false,
	]); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			// 'id',
			[
				'format' => 'raw',
				'attribute' => 'name',
				'value' => function($data) {
					return Html::a($data->name, ['index', 'parent' => $data->id], ['data-pjax' => 0]);
				}
			],
			/*[
                'format' => 'raw',
                'header' => 'Товары',
                'value' => function($data){
                    return Html::a('Товары',\yii\helpers\Url::to(['/admin/catalog/item', 'pid' => $data->id]),['data-pjax' => 0]);
                },
            ],*/

			/*[
				'class'  => dosamigos\grid\EditableColumn::className(),
				'type' => 'text',
				'url' => ['editable'],
				'attribute' => 'name',
				'editableOptions' => [
					'mode' => 'pop', // 'inline'
				]
			],
			[
				'class'  => dosamigos\grid\EditableColumn::className(),
				'type' => 'text',
				'url' => ['editable'],
				'attribute' => 'slug',
				'editableOptions' => [
					'mode' => 'pop',
				]
			],*/
			[
				'header' => Module::t('module', 'Count subcategory'),
				'value' => function($data){
					return count($data->children()->all());
				},
				'contentOptions' => ['style' => 'text-align: center;width: 100px'],
			],
			[
			    'class' => \dosamigos\grid\ToggleColumn::className(),
			    'attribute' => 'status',
			    // 'onValue' => 1,
			    'onLabel' => Catalog::getStatusesArray()[Catalog::ACTIVE],
			    'offLabel' => Catalog::getStatusesArray()[Catalog::NOT_ACTIVE],
			    'contentOptions' => ['class' => 'text-center'],
			    'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
			    'filter' => Catalog::getStatusesArray(),
			    'contentOptions' => ['style' => 'text-align: center;width: 120px'],
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}{delete}',
				/*'urlCreator'=>function($action, $model, $key, $index){
				   return [$action,'id'=>$model->id,'parent'=>Yii::$app->request->get('parent')];
				},*/
				'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
				'buttons' => [
					'delete' => function ($url) {
						return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
							'title' => Module::t('module', 'Delete'),
							'aria-label' => Module::t('module', 'Delete'),
							'onclick' => "
								if (confirm('Удалить каталог?')) {
									$.ajax('$url', {
										type: 'POST'
									}).done(function(data) {
										$.pjax.reload({container: '#pjax-container', timeout: 10000});
									});
								}
								return false;",
						]);
					},
				],
			],
		],
	]); ?>
	<?php \yii\widgets\Pjax::end(); ?>

</div>
