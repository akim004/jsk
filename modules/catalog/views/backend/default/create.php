<?php

use app\modules\navigation\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\navigation\models\Navigation */

$this->title = Module::t('module', 'CREATE').' '.$this->context->title;

$this->params['breadcrumbs'] = $this->context->breadcrumbs;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="navigation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
