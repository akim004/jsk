<?php

use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Catalog;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="catalog-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<?= $form->errorSummary($model) ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
		<li><a href="#settings">SEO</a></li>
		<li><a href="#sitemap">Sitemap</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">


			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'parentId')->dropDownList(['0'=>'Нет']+Catalog::getListData($model->id)) ?>

			<?= $form->field($model, 'status')->dropDownList(Catalog::getStatusesArray()) ?>
			<?//= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>

			<div >
				<?$preview = $model->getImage() ? [Html::img('/'.$model->getImage()->getPathToOrigin(), ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon']),] : false;

					echo $form->field($model, 'imageUpload')->widget(FileInput::classname(), [
						'options' => [
							'accept' => 'imageUpload/*',
							'class' => 'for-remove-function',
						],
						'pluginOptions' => [
							'maxImageWidth' => $model->maxImageWidth,
							'maxImageHeight' => $model->maxImageHeight,
							'initialPreview'   => $preview,
							'initialCaption'   => "Загрузите картинку",
							'overwriteInitial' => true,
							'showUpload'       => false,
							'allowedFileExtensions' => ['jpg','gif','png'],
						]
					]);
				?>
			</div>
			<?= $form->field($model, 'isRemoveImage')->hiddenInput(['class' => 'removeImage'])->label(false); ?>
		</div>
		<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\RouteForm::widget(['model' => $model]) ?>
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>
		<div class="tab-pane x_content" id="sitemap">
			<?= \app\widgets\SitemapForm::widget(['model' => $model]) ?>
		</div>
	</div>

	<hr />

	<div class="form-group">
		<?= Html::submitButton(Module::t('module', 'Apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update and close'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
		<?= Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
