<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\navigation\models\Navigation */

$this->title = 'Редактирование '.$this->context->title.' : '.$model->name;

$this->params['breadcrumbs'] = $this->context->breadcrumbs;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="navigation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
