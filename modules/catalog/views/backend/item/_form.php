<?php

use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Attribute;
use app\modules\catalog\models\backend\Catalog;
use app\modules\catalog\models\backend\Item;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="catalog-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
		<li><a href="#extra">Дополнительные</a></li>
		<li><a href="#settings">SEO</a></li>
		<?if(!$model->isNewRecord){?>
			<li><a href="#images">Загруженные фотографии</a></li>
		<?}?>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'status')->dropDownList(Item::getStatusesArray()) ?>

				<label> <?=$model->attributeLabels()['categoryes']?> </label>
				<?echo \kartik\select2\Select2::widget([
				    'model' => $model,
				    'attribute' => 'categoryes',
				    'data' => Catalog::getListData(),
				    'options' => ['multiple' => true, 'placeholder' => 'Выберите категории...'],
				    'pluginOptions' => [
				    	'tags' => true,
				    	'maximumInputLength' => 10
				        // 'allowClear' => true
				    ],
				]);?>
				<br>
			<div >
				<label> <?= $model->attributeLabels()['imageUpload']?></label>
				<?
					echo \kartik\file\FileInput::widget([
						'id' => 'images-upload',
					    'name' => 'Item[imageUpload][]',
					    'language' => 'ru',
					    'options' => ['multiple' => true],

					    'pluginOptions' => [
					    	'uploadExtraData' => [
					            'modelId' => $model->id,
					        ],
					    	'previewFileType' => 'any',
					        'maxImageWidth' => $model->maxImageWidth,
							'maxImageHeight' => $model->maxImageHeight,
							'initialCaption'   => "Загрузите фотографии",
							'overwriteInitial' => true,
							'showUpload'       => true,
							'allowedFileExtensions' => ['jpg','gif','png'],
					    	'uploadUrl' => $model->isNewRecord ? '' : \yii\helpers\Url::to(['file-upload']),
					    ],
					]);
					?>
			</div>
		</div>
		<div class="tab-pane x_content" id="extra">
			<div class="panel panel-default">
				<div class="panel-heading">Теги</div>
				<div class="panel-body">
					<?= $form->field($model, 'top')->checkbox() ?>
					<?= $form->field($model, 'discount')->checkbox() ?>
				</div>
			</div>
			<?
				$groups = \app\modules\catalog\models\backend\AttributeGroup::find()->all();
				foreach ($groups as $data) {?>
					<div class="panel panel-default">
					  <div class="panel-heading"><?=$data->name;?></div>
					  <div class="panel-body">
						<?foreach ($data->mAttrubutes as $attribute) {?>
							<?
							switch ($attribute->type) {
								case Attribute::TYPE_STRING:
									echo Html::label($attribute->name, 'attr'.$attribute->id);
									echo Html::textInput('Item[mattributes]['.$attribute->id.']', $model->getAttributeValue($attribute->id), ['class' => 'form-control', 'id'=>'attr'.$attribute->id]);
									break;
								case Attribute::TYPE_TEXT:
									echo Html::label($attribute->name, 'attr'.$attribute->id);
									echo Html::textArea('Item[mattributes]['.$attribute->id.']', $model->getAttributeValue($attribute->id), ['class' => 'form-control', 'id'=>'attr'.$attribute->id]);
									break;
								case Attribute::TYPE_CHECKBOX:
									echo Html::checkbox('Item[mattributes]['.$attribute->id.']', $model->getAttributeValue($attribute->id),['id'=>'attr'.$attribute->id]);
									echo Html::label($attribute->name, 'attr'.$attribute->id);
									break;
								default:
									echo Html::label($attribute->name, 'attr'.$attribute->id);
									echo Html::textInput('Item[mattributes]['.$attribute->id.']', $model->getAttributeValue($attribute->id), ['class' => 'form-control', 'id'=>'attr'.$attribute->id]);
									break;
							}?><br>
						<?}?>
					  </div>
					</div>
				<?}
			?>
		</div>
		<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>
		<div class="tab-pane x_content" id="images">
			<?
			    $query = \rico\yii2images\models\Image::find();
			    $query->andFilterWhere([
			        'itemId' => $model->primaryKey,
			        'modelName' => Yii::$app->getModule('yii2images')->getShortClass($model)
			    ]);
			    $dataProvider = new \yii\data\ActiveDataProvider([
			                            'query' => $query,
			                        ]);
		    ?>
		    <?php \yii\widgets\Pjax::begin([
		    			'id' => 'files-container',
		    			'timeout' => 10000,
		    ]); ?>
			    <?= \yii\grid\GridView::widget([
			        'dataProvider' => $dataProvider,
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],
			            'id',
			            [
			                'format' => 'html',
			                'label' => 'Фотография',
			                'value' => function (\rico\yii2images\models\Image $data) {
			                    return Html::img($data->getUrl('100x'));
			                },
			            ],
			            [
			                'class' => \dosamigos\grid\EditableColumn::className(),
			                'header' => 'Подписи к картинкам',
			                'attribute' => 'name',
			                'url' => ['editableiamge'],
			                'type' => 'text',
			                'editableOptions' => [  ]
			            ],
			            [
			                'format' => 'raw',
			                'label' => '',
			                'value' => function ($data) {
			                        return Html::a('Удалить', \yii\helpers\Url::to(['delete-image', 'id' => $data->id]),
			                        	[
			                        		'class' => 'pjax-link',
			                        		'data-pjax' => 0,
			                        		'data-pjax-id' => 'files-container',
			                        		'data-confirm-text' => 'Вы уверены',
			                        	]);
			                    },
			            ],
			        ],
			    ]); ?>
			<?php \yii\widgets\Pjax::end(); ?>
		</div>
	</div>

	

	<hr />

	<div class="form-group">
		<?= Html::submitButton(Module::t('module', 'Apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update and close'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
		<?= Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

<?
$script = <<< JS
	$(function(){
		$('#images-upload').on('fileuploaded', function(event, data, previewId, index) {
		    /*var form = data.form, files = data.files, extra = data.extra,
		        response = data.response, reader = data.reader;*/
		    $.pjax.reload({container:'#files-container',timeout: 10000,});
		});
	});
JS;

 $this->registerJs($script, yii\web\View::POS_READY);?>

</div>
