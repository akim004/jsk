<?php

use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Item;
use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;

$this->title = $this->context->title;

$parent = array_pop($this->context->breadcrumbs);
$this->context->breadcrumbs[] = isset($parent['label']) ? $parent['label'] : $this->title;

$this->params['breadcrumbs'] = $this->context->breadcrumbs;
?>
<div class="catalog-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a(Module::t('module', 'Create item'), ['create', 'parent'=>Yii::$app->request->get('parent')], ['class' => 'btn btn-success']) ?>
	</p>

	<?php \yii\widgets\Pjax::begin([
		'id' => 'pjax-container',
		'timeout' => 20000,
	]); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			// 'id',
			'name',
			[
                'class'     => \app\components\EditableColumn::className(),
                'type'      => 'select',
                'url'       => ['editable'],
                'attribute' => 'status',
                'format' => 'raw',
                'filter'    => Item::getStatusesArray(),
                'value' => function($data) {
                    return Item::getStatusesArray()[$data->status];
                },
                'editableOptions' => [
                    'mode' => 'pop', // 'inline'
                    'source' => json_encode(Item::getStatusesArrayForSource()),
                    'placement' => 'bottom',
                ],
            ],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}{delete}',
				/*'urlCreator'=>function($action, $model, $key, $index){
				   return [$action,'id'=>$model->id];
				},*/
				'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
				'buttons' => [
					'delete' => function ($url) {
						return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
							'title' => Module::t('module', 'Delete'),
							'aria-label' => Module::t('module', 'Delete'),
							'onclick' => "
								if (confirm('Удалить товар?')) {
									$.ajax('$url', {
										type: 'POST'
									}).done(function(data) {
										$.pjax.reload({container: '#pjax-container', timeout: 10000});
									});
								}
								return false;
							",
						]);
					},
				],
			],
		],
	]); ?>
	<?php \yii\widgets\Pjax::end(); ?>

</div>
