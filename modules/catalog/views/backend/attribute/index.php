<?php

use app\components\UrlP;
use app\modules\catalog\Module;
use app\modules\catalog\models\backend\Attribute;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\catalog\models\backend\AttributeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'Attributes');
$this->params['breadcrumbs'] = $this->context->breadcrumbs;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('module', 'Create Attribute'), UrlP::toRoute(['create']), ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'type',
                'value' => function($data){
                    return Attribute::getTypeArray()[$data->type];
                },
                'filter' => Attribute::getTypeArray(),
                'contentOptions' => ['style' => 'text-align: center;width: 120px'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'urlCreator'=>function($action, $model, $key, $index){
                   return [$action,'id'=>$model->id,'pid'=>Yii::$app->request->get('pid')];
                },
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
