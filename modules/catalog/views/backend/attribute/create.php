<?php

use app\components\UrlP;
use app\modules\catalog\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\backend\Attribute */

$this->title = Module::t('module', 'Create Attribute');
$this->params['breadcrumbs'] = $this->context->breadcrumbs;
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Attributes'), 'url' => UrlP::toRoute(['index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
