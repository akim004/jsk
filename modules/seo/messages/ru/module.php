<?php

return [
	'Create' => 'Создать',
	'Save' => 'Сохранить',
	'Success' => 'Данные успешно сохранены',

	'Seo H1'          => 'H1 (SEO)',
	'Seo Title'       => 'Заголовок (SEO)',
	'Seo Keywords'    => 'Ключевые слова (SEO)',
	'Seo Description' => 'Описание (SEO)',

	'ID'       => 'id',
	'Enable'   => 'Включить в sitemap',
	'Priority' => 'Приоритет',
	'Url'      => 'Url',
	'Class'    => 'Класс',
	'Item ID'  => 'Id сущности',
];