<?php

namespace app\modules\seo;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	use \app\modules\seo\ModuleTrait;

	public function bootstrap($app)
	{
		$app->i18n->translations['modules/seo/*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'forceTranslation' => true,
			'basePath' => '@app/modules/seo/messages',
			'fileMap' => [
				'modules/seo/module' => 'module.php',
			],
		];

		$app->urlManager->registerModuleRules($this->getModule());

	}
}