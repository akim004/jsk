<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/seo/migrations --interactive=0
 */

namespace app\modules\seo;

use Yii;

class Module extends \app\components\Module
{
	public $controllerNamespace = 'app\modules\seo\controllers';

	public $identityClass = '\app\modules\seo\models\Seo';

	public function init()
	{
		parent::init();
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/seo/' . $category, $message, $params, $language);
	}

	public static function getUrlRules()
	{
		return [
		];
	}
}
