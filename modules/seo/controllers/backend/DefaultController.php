<?php

namespace app\modules\seo\controllers\backend;

use Yii;
use app\components\BackendController;
use app\modules\seo\Module;
use app\modules\seo\models\Seo;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends BackendController
{

	public function actionIndex(){
	}
}
