<?php
namespace app\modules\seo\models;

use Yii;
use app\modules\seo\Module;
use app\validators\EscapeValidator;
use yii\db\ActiveRecord;

class Seo extends ActiveRecord
{
	public static function tableName()
	{
		return '{{%seo}}';
	}

	public function rules()
	{
		return [
			[['h1', 'title', 'keywords', 'description'], 'trim'],
			[['h1', 'title', 'keywords', 'description'], 'string', 'max' => 255],
			[['h1', 'title', 'keywords', 'description'], EscapeValidator::className()],
		];
	}

	public function attributeLabels()
	{
		return [
			'h1'          => Module::t('module', 'Seo H1'),
			'title'       => Module::t('module', 'Seo Title'),
			'keywords'    => Module::t('module', 'Seo Keywords'),
			'description' => Module::t('module', 'Seo Description'),
		];
	}

	public function isEmpty()
	{
		return (!$this->h1 && !$this->title && !$this->keywords && !$this->description);
	}
}