<?php

namespace app\modules\seo\models;

use Yii;
use app\modules\seo\Module;

/**
 * This is the model class for table "{{%sitemap}}".
 *
 * @property integer $id
 * @property integer $enable
 * @property double $priority
 * @property string $url
 * @property string $class
 * @property integer $item_id
 */
class Sitemap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sitemap}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['enable', 'item_id'], 'integer'],
            [['priority'], 'number'],
            [['class', 'item_id'], 'required'],
            [['url'], 'string', 'max' => 100],
            [['class'], 'string', 'max' => 30],
            [['priority'], 'default', 'value' => 0.5],
            [['enable'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => Module::t('module', 'ID'),
            'enable'   => Module::t('module', 'Enable'),
            'priority' => Module::t('module', 'Priority'),
            'url'      => Module::t('module', 'Url'),
            'class'    => Module::t('module', 'Class'),
            'item_id'  => Module::t('module', 'Item ID'),
        ];
    }

    public function isEmpty()
    {
        return (!$this->priority);
    }
}
