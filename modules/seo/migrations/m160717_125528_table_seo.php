<?php

use yii\db\Schema;
use yii\db\Migration;

class m160717_125528_table_seo extends Migration
{
   public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%seo}}', [
			'id'          => Schema::TYPE_PK,
			'h1'          => Schema::TYPE_STRING . '(255) NOT NULL',
			'title'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'keywords'    => Schema::TYPE_TEXT . ' NOT NULL',
			'description' => Schema::TYPE_TEXT . ' NOT NULL',
			'class'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'item_id'     => Schema::TYPE_INTEGER . '(11) NULL DEFAULT NULL',
		], $tableOptions);

		// $this->createIndex('idx_config_param', '{{%config}}', 'param');
	}

	public function down()
	{
		$this->dropTable('{{%seo}}');
	}
}
