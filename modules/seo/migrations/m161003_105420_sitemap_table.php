<?php

use yii\db\Migration;
use yii\db\Schema;

class m161003_105420_sitemap_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sitemap}}', [
            'id'       => Schema::TYPE_PK,
            'enable'   => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 1',
            'priority' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0.5',
            'url'      => Schema::TYPE_STRING . '(100) NOT NULL',
            'class'    => Schema::TYPE_STRING . '(30) NOT NULL',
            'item_id'  => Schema::TYPE_INTEGER . '(11) NULL DEFAULT NULL',
        ], $tableOptions);

        $this->createIndex('idx_sitemap_item_id', '{{%sitemap}}', 'item_id');
        $this->createIndex('idx_sitemap_class', '{{%sitemap}}', 'class');
    }

    public function down()
    {
        $this->dropTable('{{%sitemap}}');
    }
}
