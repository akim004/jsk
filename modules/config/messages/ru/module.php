<?php

return [
	'Create'  => 'Создать',
	'Save'    => 'Сохранить',
	'Success' => 'Данные успешно сохранены',

	'id'      => 'ID',
	'param'   => 'Параметр (уникальный)',
	'value'   => 'Значение',
	'default' => 'Значение по умолчанию',
	'label'   => 'Заголовок',
	'type'    => 'Тип (string, text, checkbox)',
];