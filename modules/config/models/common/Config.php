<?php

namespace app\modules\config\models\common;

use Yii;
use app\modules\config\Module;

/**
 * This is the model class for table "{{%config}}".
 *
 * @property integer $id
 * @property integer $param
 * @property string $value
 * @property string $default
 * @property string $label
 * @property string $type
 */
class Config extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%config}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['param', 'value', 'default', 'label'], 'safe'],
			[['module'], 'string', 'max' => 128],
			[['value', 'default'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'      => Module::t('module','id'),
			'param'   => Module::t('module','param'),
			'value'   => Module::t('module','value'),
			'default' => Module::t('module','default'),
			'label'   => Module::t('module','label'),
			'type'    => Module::t('module','type'),
		];
	}

	public static function getModuleConfig(\app\components\ModuleInterface $module)
	{
		$moduleSource = get_class($module);
		return self::find()
						->select(['IF( `value` = "", `default`, `value` ) AS `value`','param'])
						->where(['module' => $moduleSource])
						->indexBy('param')
						->column();
	}
}
