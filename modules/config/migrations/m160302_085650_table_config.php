<?php

use yii\db\Schema;
use yii\db\Migration;

class m160302_085650_table_config extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%config}}', [
			'id'      => Schema::TYPE_PK,
			'module'  => Schema::TYPE_STRING . '(128) NOT NULL',
			'param'   => Schema::TYPE_STRING . '(128) NOT NULL',
			'value'   => Schema::TYPE_TEXT . ' NOT NULL',
			'default' => Schema::TYPE_TEXT . ' NOT NULL',
			'label'   => Schema::TYPE_STRING . '(255) NOT NULL',
			'type'    => Schema::TYPE_STRING . '(128) NULL DEFAULT NULL',
		], $tableOptions);

		$this->createIndex('idx_config_param', '{{%config}}', 'param');

		$this->batchInsert('{{%config}}', ['id', 'module','param','value','default','label','type'], [
			['1', 'app\modules\main\Module', 'name','Название сайта','Мой сайт','Название сайта', 'text'],
			['2', 'app\modules\seo\Module', 'seoTitle','Заголовок всех страниц','Мой сайт','Заголовок всех страниц', 'text'],
			['3', 'app\modules\seo\Module', 'seoDescription','Описание всех страниц','Мой сайт','Описание всех страниц', 'text'],
			['4', 'app\modules\seo\Module', 'seoKeywords','Ключевые слова всех страниц','Мой сайт','Ключевые слова всех страниц', 'text'],
			['5', 'app\modules\main\Module', 'seoTitle','Заголовок главной страницы','Мой сайт','Заголовок главной страницы', 'string'],
			['6', 'app\modules\main\Module', 'seoDescription','Описание главной страницы','Мой сайт','Описание главной страницы', 'text'],
			['7', 'app\modules\main\Module', 'seoKeywords','Ключевые слова главной страницы','Мой сайт','Ключевые слова главной страницы', 'text'],
			['8', 'app\modules\image\Module', 'maxImageWidth','1500','1500','Максимальная ширина изображения (px)', 'string'],
			['9', 'app\modules\image\Module', 'maxImageHeight','1500','1500','Максимальная высота изображения (px)', 'string'],
			['10', 'app\modules\image\Module', 'maxSize','5','5','Максимальный размер (Мб)', 'string'],
			['11', 'app\modules\main\Module', 'robots','Текст для robots.txt','Мой сайт','Текст для robots.txt', 'text'],
		]);
	}

	public function down()
	{
		$this->dropTable('{{%config}}');
	}
}
