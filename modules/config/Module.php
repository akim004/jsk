<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/config/migrations --interactive=0
 */

namespace app\modules\config;

use Yii;

class Module extends \app\components\Module
{
	public $controllerNamespace = 'app\modules\config\controllers';

	public function init()
	{
		parent::init();
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/config/' . $category, $message, $params, $language);
	}

	public static function getUrlRules()
	{
		return [
			'admin/config/<module:[\w\-]+>' => 'admin/config/default/index',
		];
	}
}
