<?php

use app\modules\config\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>

	<?php foreach($items as $i => $item): ?>
		<?php if (!$item->type || $item->type == 'string'): ?>
			<tr>
				<td colspan="2"><h4><?=$item->label?></h4></td>
			</tr>
			<tr>
				<td><?= $form->field($item, "[$i]value")->textInput(['style' => 'width:95%;'])->label(false);?></td>
			</tr>
		<?php elseif ($item->type == 'text'): ?>
			<tr>
				<td colspan="2"><h4><?=$item->label?></h4></td>
			</tr>
			<tr>
				<td><?= $form->field($item, "[$i]value")->textArea(['style' => 'width:95%;height:160px;'])->label(false);?></td>
			</tr>
		<?php elseif ($item->type == 'checkbox'): ?>
			<tr>
				<td>
					<?= $form->field($item, "[$i]value")->checkBox() ?>
					<?= $form->field($item, "[$i]value")->label(['class' => 'label-sib-checkbox'])?>
				</td>
			</tr>
		<?php endif; ?>
	<?php endforeach; ?>
	<hr>
	<div class="form-group">
		<?= Html::submitButton(Module::t('module', 'Save'), ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
