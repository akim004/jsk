<?php

namespace app\modules\config\controllers\backend;

use Yii;
use app\components\BackendController;
use app\modules\config\Module;
use app\modules\config\models\common\Config;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ConfigController implements the CRUD actions for Config model.
 */
class DefaultController extends BackendController
{

	public function actionIndex($module = ''){

		$module = Yii::$app->getModule($module);
		$moduleSource = get_class($module);
		$items = Config::find()->where(['module'=>$moduleSource])->orderBy('position ASC')->all();

		if(isset($_POST['Config'])){
			$valid=true;
			foreach($items as $i=>$item){

				if(isset($_POST['Config'][$i])){
					$item->value = $_POST['Config'][$i]['value'];
				}

				$valid=$item->save() && $valid;

				if($item->param == 'robots'){
					$this->createRobots($item->value);
				}
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'Success'));
			}
		}
		return $this->render('_form',[
					'items' => $items
				]);
	}

	private function createRobots($value)
	{
		$fileText = fopen('robots.txt', 'w+');
		$test = fwrite($fileText, $value);
		fclose($fileText);
	}
}
