<?php

namespace app\modules\config;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    use \app\modules\config\ModuleTrait;

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/config/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@app/modules/config/messages',
            'fileMap' => [
                'modules/config/module' => 'module.php',
            ],
        ];

        $app->urlManager->registerModuleRules($this->getModule());

        foreach ($app->modules as $key => $module) {
            if($module instanceof \app\components\ModuleInterface){
                $module->configFromSource = \app\modules\config\models\common\Config::getModuleConfig($module);
                $module->registerConfig($module->configFromSource);
            }
        }
    }
}