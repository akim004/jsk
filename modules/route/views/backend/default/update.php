<?php

use app\modules\route\Module;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Route\models\Route */

$this->title = Module::t('module', 'Update {modelClass}: ', [
    'modelClass' => 'Route',
]);
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Route'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('module', 'Update');
?>
<div class="Route-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
