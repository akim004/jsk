<?php

use app\modules\route\Module;
use app\modules\route\models\Route;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Page\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'Routes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Route-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Module::t('module', 'Create Route'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php \yii\widgets\Pjax::begin([
		'id' => 'pjax-container',
		'timeout' => 20000,
	]); ?>

		<?= \yii\grid\GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				// 'id',
				[
					'class'     => \dosamigos\grid\EditableColumn::className(),
					'type'      => 'text',
					'url'       => ['editable'],
					'attribute' => 'alias',
				],
				[
					'class'     => \dosamigos\grid\EditableColumn::className(),
					'type'      => 'text',
					'url'       => ['editable'],
					'attribute' => 'origin',
				],
				[
					'class'     => \app\components\EditableColumn::className(),
					'type'      => 'select',
					'url'       => ['editable'],
					'attribute' => 'status',
					'format' => 'raw',
					'filter'    => Route::getStatusesArray(),
					'value' => function($data) {
						return Route::getStatusesArray()[$data->status];
					},
					'editableOptions' => [
						'mode' => 'pop', // 'inline'
						'source' => json_encode(Route::getStatusesArrayForSource()),
						'placement' => 'bottom',
					],
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{update}{delete}',
					'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
					'buttons' => [
						'delete' => function ($url) {
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
								'title' => Module::t('module', 'Delete'),
								'aria-label' => Module::t('module', 'Delete'),
								'onclick' => "
									if (confirm('Удалить url?')) {
										$.ajax('$url', {
											type: 'POST'
										}).done(function(data) {
											$.pjax.reload({container: '#pjax-container', timeout: 10000});
										});
									}
									return false;
								",
							]);
						},
					],
				],
			],
		]); ?>
	<?php \yii\widgets\Pjax::end(); ?>

</div>
