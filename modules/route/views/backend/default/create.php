<?php

use app\modules\route\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\route\models\Page */

$this->title = Module::t('module', 'Create Route');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Routes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
