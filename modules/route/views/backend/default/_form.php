<?php

use app\modules\route\Module;
use app\modules\route\models\Route;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="page-form">

	<?php $form = ActiveForm::begin([
	]); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'origin')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'status')->dropDownList(Route::getStatusesArray()) ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton(Module::t('module', 'Apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update and close'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
		<?= Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
