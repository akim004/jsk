<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/route/migrations --interactive=0
 */

namespace app\modules\route;

use Yii;

class Module extends \app\components\Module
{
	public $controllerNamespace = 'app\modules\route\controllers';

	public $identityClass = '\app\modules\route\models\Route';

	public function init()
	{
		parent::init();
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/route/' . $category, $message, $params, $language);
	}

	public static function getUrlRules()
	{
		$routes = \app\modules\route\models\Route::find()->select(['origin','alias'])->indexBy('alias')->column();
		$request = new \yii\web\Request();

		$rules = [];
		foreach ($routes as $pattern => $route) {
			$request->setPathInfo($route);
			$parseInfo = Yii::$app->urlManager->parseRequest($request);
			$rule = [
				'pattern'  => $pattern,
				'route'    => $parseInfo[0],
				'defaults' => $parseInfo[1],
			];
			$rules[] = $rule;
		}

		return $rules;
	}
}
