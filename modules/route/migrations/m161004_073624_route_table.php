<?php

use yii\db\Migration;
use yii\db\Schema;

class m161004_073624_route_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%route}}', [
			'id'         => Schema::TYPE_PK,
			'item_id'    => Schema::TYPE_INTEGER . '(11) NULL DEFAULT 0',
			'position'   => Schema::TYPE_INTEGER . '(11) NULL DEFAULT 0',
			'class'      => Schema::TYPE_STRING . '(255) NULL',
			'alias'      => Schema::TYPE_STRING . '(255) NULL',
			'origin'     => Schema::TYPE_STRING . '(255) NULL',
			'status'     => Schema::TYPE_SMALLINT . ' NULL DEFAULT 0',
			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);

		$this->createIndex('idx_route_item_id', '{{%route}}', 'item_id');
		$this->createIndex('idx_route_alias', '{{%route}}', 'alias');
		$this->createIndex('idx_route_origin', '{{%route}}', 'origin');
		$this->createIndex('idx_route_class', '{{%route}}', 'class');
	}

	public function down()
	{
		$this->dropTable('{{%route}}');
	}
}
