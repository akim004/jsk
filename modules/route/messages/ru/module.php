<?php

return [
	'ID'         => 'Id',
	'Item ID'    => 'Сущность',
	'Position'   => 'Позиция в списке',
	'Class'      => 'Класс',
	'Alias'      => 'Псевдоним (alias)',
	'Origin'     => 'Оригинал (url)',
	'Status'     => 'Статус',
	'Created At' => 'Дата создания',
	'Updated At' => 'Дата обновления',

	'Routes' => 'Пути',
	'Route' => 'Путь',
	'Create Route' => 'Создать путь',

	'Active'     => 'Активный',
	'Not active' => 'Скрытый',

	'Create' => 'Создать',
	'Apply'  => 'Сохранить',
	'Cancel' => 'Отменить',
	'Update and close' => 'Сохранить и выйти',

	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать путь: ',

	'successfully added' => 'Путь успешно добавлена',
	'successfully changed' => 'Путь успешно изменена',
];