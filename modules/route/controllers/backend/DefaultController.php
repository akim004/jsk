<?php

namespace app\modules\route\controllers\backend;

use Yii;
use app\modules\route\Module;
use app\modules\route\models\Route;
use app\modules\route\models\RouteSearch;
use dosamigos\editable\EditableAction;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DefaultController implements the CRUD actions for Route model.
 */
class DefaultController extends \app\components\BackendController
{

	public function actions()
	{
		return [
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridAction::className(),
				'modelName' => Route::className(),
			],
			'editable' => [
				'class' => EditableAction::className(),
				'modelClass' => Route::className(),
				'forceCreate' => false
			],
		];
	}

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Route models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = Yii::createObject(RouteSearch::className());
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Creates a new Route model.
	 * If creation is successful, the browser will be redirected to the 'view' Route.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Route();

		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){

				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully added'));

				if(!empty($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}

				return $this->redirect(['index']);

			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Route model.
	 * If update is successful, the browser will be redirected to the 'view' Route.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
			if ($model->save()) {
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully changed'));

				if(!empty($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Route model.
	 * If deletion is successful, the browser will be redirected to the 'index' Route.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		if (!Yii::$app->request->isAjax) {
			return $this->redirect(['index']);
		}
	}

	/**
	 * Finds the Route model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Route the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Route::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested Route does not exist.');
		}
	}
}
