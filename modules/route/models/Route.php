<?php

namespace app\modules\route\models;

use Yii;
use app\modules\route\Module;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%route}}".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $position
 * @property string $class
 * @property string $alias
 * @property string $origin
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Route extends \yii\db\ActiveRecord
{
	const ACTIVE = 0;
	const NOT_ACTIVE = 1;
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%route}}';
	}

	public function behaviors()
	{
		return [
			['class' => \yii\behaviors\TimestampBehavior::className()],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['alias', 'unique'],
			[['item_id', 'position', 'status', 'created_at', 'updated_at'], 'integer'],
			[['alias', 'origin'], 'required'],
			[['class', 'alias', 'origin'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'         => Module::t('module', 'ID'),
			'item_id'    => Module::t('module', 'Item ID'),
			'position'   => Module::t('module', 'Position'),
			'class'      => Module::t('module', 'Class'),
			'alias'      => Module::t('module', 'Alias'),
			'origin'     => Module::t('module', 'Origin'),
			'status'     => Module::t('module', 'Status'),
			'created_at' => Module::t('module', 'Created At'),
			'updated_at' => Module::t('module', 'Updated At'),
		];
	}

	/**
	 * @inheritdoc
	 * @return RouteQuery the active query used by this AR class.
	 */
	public static function find()
	{
		$routeQuery = new RouteQuery(get_called_class());

		return $routeQuery->orderBy(['position' => SORT_ASC]);
	}

	public function isEmpty()
	{
		return (!$this->alias);
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::ACTIVE,
				'text' => Module::t('module', 'Active'),
			],
			[
				'value' => self::NOT_ACTIVE,
				'text' => Module::t('module', 'Not active'),
			],
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE     => Module::t('module', 'Active'),
			self::NOT_ACTIVE => Module::t('module', 'Not active'),
		];
	}
}
