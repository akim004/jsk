<?php

namespace app\modules\route;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	use \app\modules\route\traits\ModuleTrait;

	public function bootstrap($app)
	{
		$app->i18n->translations['modules/route/*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'forceTranslation' => true,
			'basePath' => '@app/modules/route/messages',
			'fileMap' => [
				'modules/route/module' => 'module.php',
			],
		];

		$app->urlManager->registerModuleRules($this->getModule());

	}
}