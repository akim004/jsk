<?php

use app\modules\admin\Module;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \app\modules\user\models\User */

$this->title = Yii::t('app', 'Панель администратора');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-default-index">
    <h1><?= Html::encode($this->title) ?></h1>
    	<a href="<?=Url::to('/admin/default/clear')?>" class="btn btn-default"><?=Module::t('module', 'Clear cache')?></a>
    <p>
        <?//= Html::a(Yii::t('app', 'ADMIN_USERS'), ['users/index'], ['class' => 'btn btn-primary']) ?>
    </p>
</div>