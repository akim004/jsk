<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use app\modules\admin\AdminThemeAsset;
use app\modules\admin\Module;
use app\modules\admin\ModuleAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

AdminThemeAsset::register($this);
$bundle = yiister\gentelella\assets\Asset::register($this);
ModuleAsset::register($this);
$controller = $this->context;
$module = $controller->module;

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta charset="<?= Yii::$app->charset ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="nav-md main-main">
<?php $this->beginBody(); ?>
<div class="container body">

	<div class="main_container">

		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">

				<div class="navbar nav_title" style="border: 0;">
					<a href="<?=Url::to(['/admin'])?>" class="site_title"><i class="fa fa-paw"></i> <span><?=Module::t('module', 'Dashboard')?></span></a>
				</div>
				<div class="clearfix"></div>

				<!-- menu prile quick info -->
				<div class="profile">
					<div class="profile_pic">
						<img src="http://placehold.it/128x128" alt="..." class="img-circle profile_img">
					</div>
					<div class="profile_info">
						<span>Добро пожаловать,</span>
						<h2><?//=Html::encode(Yii::$app->user->identity->username)?></h2>
					</div>
				</div>
				<!-- /menu prile quick info -->

				<br />

				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

					<div class="menu_section">
						<h3>Меню</h3>
						<?=
						\yiister\gentelella\widgets\Menu::widget(
							[
								'items' => array_filter([
									[
										'label' => 'Пользователи',
										'url' => ['/admin/user/default/index'],
										'active' => $module->id === 'user',
										"icon" => "user",
										// 'visible' => Yii::$app->getUser()->can('moder'),
									],
									[
										'label' => 'Настройки',
										// 'url' => ['/admin/config/default/index'],
										"url" => "#",
										'active' => $module->id === 'config',
										"icon" => "cogs",
										'items' => [
											[
												'label' => 'Приложения',
												'url' => ['/admin/config/main'],
											],
											[
												'label' => 'Новости',
												'url' => ['/admin/config/news'],
											],
											[
												'label' => 'Категории',
												'url' => ['/admin/config/catalog'],
											],
											[
												'label' => 'Пользователи',
												'url' => ['/admin/config/user'],
											],
											[
												'label' => 'Фотографии',
												'url' => ['/admin/config/image'],
											],
											[
												'label' => 'Seo',
												'url' => ['/admin/config/seo'],
											],
											[
												'label' => 'Другие',
												'url' => ['/admin/config'],
											],
										]
										// 'visible' => Yii::$app->getUser()->can('moder'),
									],
									[
										'label' => 'Проект',
										// 'url' => ['/admin/config/default/index'],
										"url" => "#",
										'active' => $module->id === 'project',
										"icon" => "home",
										'items' => [
											[
												'label' => 'Комплекс',
												'url' => ['/admin/project/complex'],
												'active' => $module->id === 'project',
											],
											[
												'label' => 'Блоки',
												'url' => ['/admin/project/block'],
											],
											[
												'label' => 'Дома',
												'url' => ['/admin/project/house'],
											],
											[
												'label' => 'Подъезды',
												'url' => ['/admin/project/pd'],
											],
											[
												'label' => 'Этажи',
												'url' => ['/admin/project/floor'],
											],
											[
												'label' => 'Квартиры',
												'url' => ['/admin/project/flat'],
											],
										]
										// 'visible' => Yii::$app->getUser()->can('moder'),
									],
									[
										'label' => 'Навигация',
										'url' => ['/admin/navigation/default/index'],
										'active' => $module->id === 'navigation',
										"icon" => "navicon",
									],
									[
										'label' => 'Каталог',
										'url' => '#',
										'active' => $module->id === 'catalog',
										"icon" => "navicon",
										'items' => [
											[
												'label' => 'Все категории',
												'url' => ['/admin/catalog/default/index'],
												'active' => $this->context->id === 'default' && $module->id === 'catalog',
											],
											[
												'label' => 'Товары',
												'url' => ['/admin/catalog/item/index'],
												'active' => $this->context->id === 'item',
											],
											[
												'label' => 'Атрибуты',
												'url' => ['/admin/catalog/attribute-group/index'],
												'active' => $this->context->id === 'attribute-group' || $this->context->id === 'attribute',
											],
										]
									],
									[
										'label' => 'Баннер',
										'url' => ['/admin/slider/default/index'],
										'active' => $module->id === 'slider',
										"icon" => "navicon",
									],
									[
										'label' => 'Контакты',
										'url' => ['/admin/contact/default/index'],
										'active' => $module->id === 'contact',
										"icon" => "navicon",
									],
									[
										'label' => 'Стат.страницы',
										'url' => ['/admin/page/default/index'],
										'active' => $module->id === 'page',
										"icon" => "print",
									],
									[
										'label' => 'Новости',
										'url' => ['/admin/news/default/index'],
										'active' => $module->id === 'news',
										"icon" => "newspaper-o",
									],
									[
										'label' => 'Текстовые блоки',
										'url' => ['/admin/blockText/default/index'],
										'active' => $module->id === 'blockText',
										"icon" => "newspaper-o",
									],
									[
										'label' => 'Портфолио',
										'url' => ['/admin/portfolio/default/index'],
										'active' => $module->id === 'portfolio',
										"icon" => "newspaper-o",
									],
									[
										'label' => 'Пути',
										'url' => ['/admin/route/default/index'],
										'active' => $module->id === 'route',
										"icon" => "newspaper-o",
									],
								]),
							]
						)
						?>
					</div>

				</div>
				<!-- /sidebar menu -->

				<!-- /menu footer buttons -->
				<div class="sidebar-footer hidden-small">
					<a href="<?=Url::to(['/admin/config/main']);?>" data-toggle="tooltip" data-placement="top" title="Settings">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="FullScreen">
						<?/*<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>*/?>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Lock">
						<?/*<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
					</a>*/?>
					<a href="<?=Url::to(['/logout']);?>" data-method="post" data-toggle="tooltip" data-placement="top" title="Logout">
						<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
					</a>
				</div>
				<!-- /menu footer buttons -->
			</div>
		</div>

		<!-- top navigation -->
		<div class="top_nav">

			<div class="nav_menu">
				<nav class="" role="navigation">
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>

					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<img src="http://placehold.it/128x128" alt=""><?=Yii::$app->user->identity->username?>
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<?/*<li><a href="javascript:;">  Profile</a>
								</li>
								<li>
									<a href="javascript:;">
										<span class="badge bg-red pull-right">50%</span>
										<span>Settings</span>
									</a>
								</li>
								<li>
									<a href="javascript:;">Help</a>
								</li>*/?>
								<li><a href="<?=Url::to(['/logout']);?>" data-method="post"><i class="fa fa-sign-out pull-right"></i> Выход</a>
								</li>
							</ul>
						</li>

						<?/*<li role="presentation" class="dropdown">
							<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-envelope-o"></i>
								<span class="badge bg-green">6</span>
							</a>
							<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
								<li>
									<a>
					  <span class="image">
										<img src="http://placehold.it/128x128" alt="Profile Image" />
									</span>
					  <span>
										<span>John Smith</span>
					  <span class="time">3 mins ago</span>
					  </span>
					  <span class="message">
										Film festivals used to be do-or-die moments for movie makers. They were where...
									</span>
									</a>
								</li>
								<li>
									<a>
					  <span class="image">
										<img src="http://placehold.it/128x128" alt="Profile Image" />
									</span>
					  <span>
										<span>John Smith</span>
					  <span class="time">3 mins ago</span>
					  </span>
					  <span class="message">
										Film festivals used to be do-or-die moments for movie makers. They were where...
									</span>
									</a>
								</li>
								<li>
									<a>
					  <span class="image">
										<img src="http://placehold.it/128x128" alt="Profile Image" />
									</span>
					  <span>
										<span>John Smith</span>
					  <span class="time">3 mins ago</span>
					  </span>
					  <span class="message">
										Film festivals used to be do-or-die moments for movie makers. They were where...
									</span>
									</a>
								</li>
								<li>
									<a>
					  <span class="image">
										<img src="http://placehold.it/128x128" alt="Profile Image" />
									</span>
					  <span>
										<span>John Smith</span>
					  <span class="time">3 mins ago</span>
					  </span>
					  <span class="message">
										Film festivals used to be do-or-die moments for movie makers. They were where...
									</span>
									</a>
								</li>
								<li>
									<div class="text-center">
										<a href="/">
											<strong>See All Alerts</strong>
											<i class="fa fa-angle-right"></i>
										</a>
									</div>
								</li>
							</ul>
						</li>*/?>

					</ul>
				</nav>
			</div>

		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main" style="box-sizing: content-box;">
			<?php if (isset($this->params['h1'])): ?>
				<div class="page-title">
					<div class="title_left">
						<h1><?= $this->params['h1'] ?></h1>
					</div>
					<div class="title_right">
						<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search for...">
								<span class="input-group-btn">
								<button class="btn btn-default" type="button">Go!</button>
							</span>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<div class="clearfix"></div>
			<?//= Alert::widget() ?>
			<?= \yiister\gentelella\widgets\FlashAlert::widget(['showHeader' => true]) ?>
			<?= yii\widgets\Breadcrumbs::widget([
				'homeLink' => [
					'label' => 'Админка',
					'url' => Url::to('/admin'),
				],
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<?= $content ?>
		</div>
		<!-- /page content -->
		<!-- footer content -->
		<?/*<footer>
			<div class="pull-right">
				Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com" rel="nofollow" target="_blank">Colorlib</a><br />
				Extension for Yii framework 2 by <a href="http://yiister.ru" rel="nofollow" target="_blank">Yiister</a>
			</div>
			<div class="clearfix"></div>
		</footer>*/?>
		<!-- /footer content -->
	</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
	<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
	</ul>
	<div class="clearfix"></div>
	<div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>