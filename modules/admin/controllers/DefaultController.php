<?php

namespace app\modules\admin\controllers;

use app\components\BackendController;
use app\modules\admin\Module;
use yii\web\Controller;

class DefaultController extends BackendController
{
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
				'view' => '@yiister/gentelella/views/error',
			],
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionClear()
	{
		\Yii::$app->cache->flush();
		\Yii::$app->getSession()->setFlash('success', Module::t('module', 'Success clear cache'));

		return $this->render('index');
	}

	public function actionUploadContentImage()
	{
		$DS  = DIRECTORY_SEPARATOR;
		$storeFolder = 'uploads/static/files';

		if (!empty($_FILES)) {

			$info = new \SplFileInfo(basename($_FILES['file']['name']));
			$name = $info->getBasename('.'.$info->getExtension()).'_'.time();
			$fileName = $name.'.'.$info->getExtension();

			$tempFile = $_FILES['file']['tmp_name'];

			$targetPath = $storeFolder . $DS;
			$targetFile =  $targetPath. $fileName;

			move_uploaded_file($tempFile,$targetFile);

			$result['link'] = '/files/'.$fileName;
			echo json_encode($result);
		}
	}

	public function actionLoadContentImage()
	{
		// $filelist = glob('files/*');

		$filelist = $this->rsearch('uploads/static/files', '/.*\.[\w+\d*]+/');

	    foreach ($filelist as $fileinfo) {
	    	$dir = explode(DIRECTORY_SEPARATOR,$fileinfo);
	    	array_pop($dir);
	    	$tag = array_pop($dir);
	    	if($tag == 'files'){
	    		$tag = 'На главной';
	    	}
	        $file['url'] = '/'.$fileinfo;
	        $file['thumb'] = '/'.$fileinfo;
	        $file['tag'] = $tag;

	        $files[] = $file;
	    }



	    return json_encode($files);
	}

	protected function rsearch($folder, $pattern) {
	    $dir = new \RecursiveDirectoryIterator($folder);
	    $ite = new \RecursiveIteratorIterator($dir);
	    $files = new \RegexIterator($ite, $pattern, \RegexIterator::GET_MATCH);
	    $fileList = array();
	    foreach($files as $file) {
	        $fileList = array_merge($fileList, $file);
	    }
	    return $fileList;
	}
}
