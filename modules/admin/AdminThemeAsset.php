<?php

namespace app\modules\admin;

use yii\web\AssetBundle;

class AdminThemeAsset extends AssetBundle
{
	public $sourcePath = '@vendor/bower/gentelella/vendors/';
	public $css = [
		'pnotify/dist/pnotify.css',
	];
	public $js = [
		'pnotify/dist/pnotify.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapPluginAsset',
		'rmrevin\yii\fontawesome\AssetBundle',
	];
}
