<?php

namespace app\modules\project;

use yii\web\AssetBundle;

class ModuleAsset extends AssetBundle
{

	public $sourcePath = '@app/modules/project/assets/';
	public $css = [
		'css/module.css',
		'js/liCover/liCover.css',
	];
	public $js = [
		'js/module.js',
		'js/liCover/jquery.liCover.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
	];
}
