<?php

return [
	'Complex' => 'Комплекс',
	'Block'   => 'Блок',
	'House'   => 'Дом',
	'Pd'      => 'Подъезд',
	'Floor'   => 'Этаж',
	'Flat'    => 'Квартира',

	'Create complex' => 'Создать комплекс',
	'Create block'   => 'Создать блок',
	'Create house'   => 'Создать дом',
	'Create pd'      => 'Создать подъезд',
	'Create floor'   => 'Создать этаж',
	'Create flat'    => 'Создать квартиру',

	'Create'                => 'Создать',
	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать: ',

	'Houses' => 'Дома',
	'Blocks' => 'Блоки',
	'Houses' => 'Дома',
	'Pds'    => 'Подъезды',
	'Floors' => 'Этажи',
	'Flats'  => 'Квартиры',

	'name' => 'Название',

	'apply' => 'Применить',
	'cancel' => 'Отмена',
	'imageUpload' => 'Картинка',
	'Name' => 'Название',

	'successfully added' => 'Комплекс успешно добавлен',
	'successfully changed' => 'Комплекс успешно изменен',

	'mm'     => 'Площадь (m2)',
	'price'  => 'Цена',
	'status' => 'Статус',
	'number' => 'Номер квартиры',
	'rooms'  => 'Количество комнат',
	'area'   => 'area',
];