<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/complex/migrations --interactive=0
 *
 */

namespace app\modules\project;

use Yii;

class Module extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\project\controllers';

    public function init()
	{
		parent::init();
		// custom initialization code goes here
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/project/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [
			'admin/project/<complex:\d+>' => 'admin/project/block/index',
			'admin/project/<complex:\d+>/<block:\d+>' => 'admin/project/house/index',
			'admin/project/<complex:\d+>/<block:\d+>/<house:\d+>' => 'admin/project/pd/index',
			'admin/project/<complex:\d+>/<block:\d+>/<house:\d+>/<pd:\d+>' => 'admin/project/floor/index',
			'admin/project/<complex:\d+>/<block:\d+>/<house:\d+>/<pd:\d+>/<floor:\d+>' => 'admin/project/flat/index',

			/*'project/<complex:\d+>' => 'project/default/block',
			'project/<complex:\d+>/<block:\d+>' => 'project/default/house',
			'project/<complex:\d+>/<block:\d+>/<house:\d+>' => 'project/default/pd',
			'project/<complex:\d+>/<block:\d+>/<house:\d+>/<pd:\d+>' => 'project/default/floor',
			'project/<complex:\d+>/<block:\d+>/<house:\d+>/<pd:\d+>/<floor:\d+>' => 'project/default/flat',*/

			'project' => 'project/default/pd',
			'project/<pd:\d+>' => 'project/default/floor',
		];
	}

	public static function conversionCoordCanvasToSvg($coordsCanvas){
		if($coordsCanvas){
			$area = explode(',',$coordsCanvas);
			$begin = $area[0].','.$area[1].' ';

			array_shift($area);
			array_shift($area);

			$end = implode(',',$area);

			return 'M '.$begin.' L '.$end;
		}else{
			return '';
		}
	}
}
