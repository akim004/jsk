<?php

namespace app\modules\project\controllers\frontend;

use Yii;
use app\modules\project\models\frontend\Complex;
use app\modules\project\models\frontend\Floor;
use app\modules\project\models\frontend\Pd;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends \app\components\FrontendController
{
	public function actionIndex()
	{
		return $this->redirect(['pd']);
		// return $this->render('index');
	}

	public function actionBlock()
	{
		return $this->render('block');
	}

	public function actionHouse()
	{
		return $this->render('house');
	}

	public function actionPd()
	{
		$complex = $this->findComplex();

		$pds = Pd::find()->all();

		return $this->render('pd', [
				'complex' => $complex,
				'pds' => $pds
			]);
	}

	public function actionFloor($pd)
	{
		$pd = $this->findPd($pd);

		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = trim(Response::FORMAT_JSON);

			$result['content'] = $this->renderAjax('floor', [
				'pd' => $pd,
				'floors' => $pd->floors,
			]);

			return $result;
		}

		return $this->render('floor', [
			'pd' => $pd,
			'floors' => $pd->floors,
		]);
	}

	public function actionFlat()
	{
		return $this->render('flat');
	}

	protected function findFloor($id)
	{
		if (($model = Floor::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findPd($id)
	{
		if (($model = Pd::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findComplex()
	{
		if (($model = Complex::find()->one()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
