<?php

namespace app\modules\project\controllers\backend;

use Yii;
use app\modules\project\Module;
use app\modules\project\components\UrlP;
use app\modules\project\models\backend\Flat;
use app\modules\project\models\backend\FlatSearch;
use dosamigos\editable\EditableAction;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class FlatController extends \app\components\BackendController
{

	public function actions()
	{
		return [

		];
	}

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all News models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new FlatSearch();
		$searchModel->complex_id = Yii::$app->request->get('complex');
		$searchModel->block_id = Yii::$app->request->get('block');
		$searchModel->house_id = Yii::$app->request->get('house');
		$searchModel->pd_id = Yii::$app->request->get('pd');
		$searchModel->floor_id = Yii::$app->request->get('floor');

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single House model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new House model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Flat();
		$model->complex_id = Yii::$app->request->get('complex');
		$model->block_id = Yii::$app->request->get('block');
		$model->house_id = Yii::$app->request->get('house');
		$model->pd_id = Yii::$app->request->get('pd');
		$model->floor_id = Yii::$app->request->get('floor');

		if ($model->load(Yii::$app->request->post())) {
			$model->imageUpload = UploadedFile::getInstance($model, 'imageUpload');
			if($model->save()){
				$model->saveImage($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully added'));

				if(!empty($_GET['wait'])){
					return $this->redirect(UrlP::toRoute(['update', 'id' => $model->id, 'wait' => 0]));
				}
				return $this->redirect(UrlP::toRoute(['index']));

			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing House model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
			$model->imageUpload = UploadedFile::getInstance($model, 'imageUpload');
			if ($model->save()) {
				$model->saveImage($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully changed'));

				if(!empty($_GET['wait'])){
					return $this->redirect(UrlP::toRoute(['update', 'id' => $model->id, 'wait' => 0]));
				}
				return $this->redirect(UrlP::toRoute(['index']));
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing House model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(UrlP::toRoute(['index']));
	}

	/**
	 * Finds the House model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return House the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Flat::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
