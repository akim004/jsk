<?php

namespace app\modules\project\controllers\backend;

use Yii;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class DefaultController extends \app\components\BackendController
{

	/**
	 * Lists all News models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		return $this->render('index', [
		]);
	}
}
