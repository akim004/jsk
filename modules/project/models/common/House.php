<?php

namespace app\modules\project\models\common;

use Yii;
use app\modules\project\Module;

/**
 * This is the model class for table "{{%complex}}".
 *
 * @property integer $id
 * @property string $name
 */
class House extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%house}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['complex_id', 'block_id'], 'integer'],
			[['name'], 'string', 'max' => 255],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'         => Module::t('module', 'ID'),
			'name'       => Module::t('module', 'name'),
			'complex_id' => Module::t('module', 'complex_id'),
			'block_id'   => Module::t('module', 'block_id'),
		];
	}
}
