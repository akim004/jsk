<?php

namespace app\modules\project\models\common;

use Yii;
use app\modules\project\Module;

/**
 * This is the model class for table "{{%complex}}".
 *
 * @property integer $id
 * @property string $name
 */
class Complex extends \yii\db\ActiveRecord
{
	public $imageUpload;

	public function behaviors()
	{
		return [
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 1,
				/*'maxImageWidth' => '1000',
				'maxImageHeight' => '1000',
				'maxSize' => '5',*/
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%complex}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['imageUpload'], 'safe'],
			[['name'], 'string', 'max' => 255],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'Id'),
			'name'        => Module::t('module', 'Name'),
			'imageUpload' => Module::t('module', 'imageUpload'),
		];
	}
}
