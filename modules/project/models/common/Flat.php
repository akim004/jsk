<?php

namespace app\modules\project\models\common;

use Yii;
use app\modules\project\Module;
use app\modules\project\models\common\Floor;

/**
 * This is the model class for table "{{%complex}}".
 *
 * @property integer $id
 * @property string $name
 */
class Flat extends \yii\db\ActiveRecord
{
	const STATUS_FREE = 0;
	const STATUS_BOUGHT = 1;
	const STATUS_RESERVE = 2;

	public $imageUpload;

	public function behaviors()
	{
		return [
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 1,
				/*'maxImageWidth' => '1000',
				'maxImageHeight' => '1000',
				'maxSize' => '5',*/
			],
		];
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%flat}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['number'], 'required'],
			[['complex_id', 'block_id','house_id','pd_id', 'floor_id'], 'integer'],
			[['name'], 'string', 'max' => 255],
			[['mm','price'], 'string', 'max' => 255],
			[['area'], 'string', 'max' => 2000],
			[['number','rooms', 'status'], 'integer'],
			[['imageUpload'], 'safe'],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'ID'),
			'name'        => Module::t('module', 'name'),
			'mm'          => Module::t('module', 'mm'),
			'price'       => Module::t('module', 'price'),
			'status'      => Module::t('module', 'status'),
			'number'      => Module::t('module', 'number'),
			'rooms'       => Module::t('module', 'rooms'),
			'area'        => Module::t('module', 'area'),
			'complex_id'  => Module::t('module', 'complex_id'),
			'block_id'    => Module::t('module', 'block_id'),
			'house_id'    => Module::t('module', 'house_id'),
			'pd_id'       => Module::t('module', 'pd_id'),
			'floor_id'    => Module::t('module', 'floor_id'),
			'imageUpload' => Module::t('module', 'imageUpload'),
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::STATUS_FREE    => 'Свободно',
			self::STATUS_BOUGHT  => 'Забронирован',
			self::STATUS_RESERVE => 'Продано',
		];
	}

	public function getFloor()
    {
        return $this->hasOne(Floor::className(), ['id' => 'floor_id']);
    }
}
