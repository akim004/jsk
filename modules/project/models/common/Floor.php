<?php

namespace app\modules\project\models\common;

use Yii;
use app\modules\project\Module;
use app\modules\project\models\common\FloorQuery;
use app\modules\project\models\common\Pd;

/**
 * This is the model class for table "{{%complex}}".
 *
 * @property integer $id
 * @property string $name
 */
class Floor extends \yii\db\ActiveRecord
{
	public $imageUpload;

	public function behaviors()
	{
		return [
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 1,
				/*'maxImageWidth' => '1000',
				'maxImageHeight' => '1000',
				'maxSize' => '5',*/
			],
		];
	}

	public static function find()
	{
		$floorQuery = new FloorQuery(get_called_class());

		return $floorQuery->orderBy(['name' => SORT_ASC]);
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%floor}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['complex_id', 'block_id','house_id','pd_id'], 'integer'],
			[['name'], 'string', 'max' => 255],
			[['area'], 'string', 'max' => 2000],
			[['imageUpload'], 'safe'],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'ID'),
			'name'        => Module::t('module', 'name'),
			'area'        => Module::t('module', 'area'),
			'complex_id'  => Module::t('module', 'complex_id'),
			'block_id'    => Module::t('module', 'block_id'),
			'house_id'    => Module::t('module', 'house_id'),
			'pd_id'       => Module::t('module', 'pd_id'),
			'imageUpload' => Module::t('module', 'imageUpload'),
		];
	}

	public function getPd()
    {
        return $this->hasOne(Pd::className(), ['id' => 'pd_id']);
    }
}
