<?php

namespace app\modules\project\models\backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\project\models\backend\Floor;

/**
 * FloorSearch represents the model behind the search form about `app\modules\Floor\models\Floor`.
 */
class FloorSearch extends Floor
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id','complex_id','block_id','house_id','pd_id'], 'integer'],
			[['name'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Floor::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => false,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id'         => $this->id,
			'complex_id' => $this->complex_id,
			'block_id'   => $this->block_id,
			'house_id'   => $this->house_id,
			'pd_id'      => $this->pd_id,
		]);

		$query->andFilterWhere(['like', 'name', $this->name]);

		return $dataProvider;
	}
}
