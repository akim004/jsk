<?php

namespace app\modules\project\models\backend;

use Yii;
use app\modules\project\Module;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%complex}}".
 *
 * @property integer $id
 * @property string $name
 */
class Complex extends \app\modules\project\models\common\Complex
{
	/**
	 * Флаг проверки удалить ли картинку
	 * @var bool
	 */
	public $isRemoveImage;

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			['class' => \app\behaviors\SeoBehavior::className()],
		]);
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['isRemoveImage'], 'safe'],
		]);
	}

	public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'imageUpload' => Module::t('module', 'imageUpload'),
		]);
	}

	public function afterDelete() {
		parent::afterDelete();

		if($this->getImage()){
			$this->removeImage($this->getImage());
		}
	}

	public static function getShapePluginObject($string)
	{
		if($string){
			$areaPoints = explode(',', $string);
			foreach ($areaPoints as $key => $data) {
				if(!($key % 2)){
					$point = [
						'x' => (float)$areaPoints[$key],
						'y' => (float)$areaPoints[$key+1],
					];
					$points[] = $point;
				}
			}
			return json_encode($points);
		}

		return '[]';
	}

	public function saveImage($file = null)
	{
		$image = $this->getImage();

		if($this->isRemoveImage && $image){
			$this->removeImage($image);
		}

		if($file){
			if($image){
				$this->removeImage($image);
			}

			$file->saveAs('uploads/runtime/'.$file->name);
			$this->attachImage('uploads/runtime/'.$file->name);
			@unlink('uploads/runtime/'.$file->name);
		}
	}
}
