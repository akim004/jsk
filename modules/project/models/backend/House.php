<?php

namespace app\modules\project\models\backend;

use Yii;

/**
 * This is the model class for table "{{%House}}".
 *
 * @property integer $id
 * @property string $name
 */
class House extends \app\modules\project\models\common\House
{
	public function behaviors()
	{
		return [
			['class' => \app\behaviors\SeoBehavior::className()],
		];
	}

	public function afterDelete() {
		parent::afterDelete();
	}
}
