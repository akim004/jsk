<?php

namespace app\modules\project\models\backend;

use Yii;

/**
 * This is the model class for table "{{%Block}}".
 *
 * @property integer $id
 * @property string $name
 */
class Block extends \app\modules\project\models\common\Block
{
	public function behaviors()
	{
		return [
			['class' => \app\behaviors\SeoBehavior::className()],
		];
	}

	public function afterDelete() {
		parent::afterDelete();
	}
}
