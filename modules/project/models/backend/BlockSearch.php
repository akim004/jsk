<?php

namespace app\modules\project\models\backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\project\models\backend\Block;

/**
 * BlockSearch represents the model behind the search form about `app\modules\Block\models\Block`.
 */
class BlockSearch extends Block
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'complex_id'], 'integer'],
			[['name'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Block::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => false,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id'         => $this->id,
			'complex_id' => $this->complex_id,
		]);

		$query->andFilterWhere(['like', 'name', $this->name]);

		return $dataProvider;
	}
}
