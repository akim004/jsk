<?php

namespace app\modules\project\models\backend;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%Floor}}".
 *
 * @property integer $id
 * @property string $name
 */
class Floor extends \app\modules\project\models\common\Floor
{
	/**
	 * Флаг проверки удалить ли картинку
	 * @var bool
	 */
	public $isRemoveImage;

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			['class' => \app\behaviors\SeoBehavior::className()],
		]);
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['isRemoveImage'], 'safe'],
		]);
	}

	public function afterDelete() {
		parent::afterDelete();

		if($this->getImage()){
			$this->removeImage($this->getImage());
		}
	}

	public function getPdImageUrl($size = '')
	{
		return $this->pd->image ? '/'.$this->pd->image->getPathToOrigin() : '';
	}

	public function saveImage($file = null)
	{
		$image = $this->getImage();

		if($this->isRemoveImage && $image){
			$this->removeImage($image);
		}

		if($file){
			if($image){
				$this->removeImage($image);
			}

			$file->saveAs('uploads/runtime/'.$file->name);
			$this->attachImage('uploads/runtime/'.$file->name);
			@unlink('uploads/runtime/'.$file->name);
		}
	}
}
