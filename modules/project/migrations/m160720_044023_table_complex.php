<?php

use yii\db\Migration;
use yii\db\Schema;

class m160720_044023_table_complex extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%complex}}', [
			'id'        => Schema::TYPE_PK,
			'name'      => Schema::TYPE_STRING . '(255) NOT NULL',
		], $tableOptions);

		$this->createTable('{{%block}}', [
			'id'         => Schema::TYPE_PK,
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'complex_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createTable('{{%house}}', [
			'id'         => Schema::TYPE_PK,
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'complex_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'block_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createTable('{{%pd}}', [
			'id'         => Schema::TYPE_PK,
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'area'       => Schema::TYPE_STRING . '(2000) NOT NULL',
			'complex_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'block_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'house_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);
		$this->createIndex('idx-pd-name', '{{%pd}}', 'name');
		$this->createIndex('idx-pd-complex-id', '{{%pd}}', 'complex_id');
		$this->createIndex('idx-pd-block-id', '{{%pd}}', 'block_id');
		$this->createIndex('idx-pd-house-id', '{{%pd}}', 'house_id');

		$this->createTable('{{%floor}}', [
			'id'         => Schema::TYPE_PK,
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'area'       => Schema::TYPE_STRING . '(2000) NOT NULL',
			'complex_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'block_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'house_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'pd_id'      => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);
		$this->createIndex('idx-floor-name', '{{%floor}}', 'name');
		$this->createIndex('idx-floor-complex-id', '{{%floor}}', 'complex_id');
		$this->createIndex('idx-floor-block-id', '{{%floor}}', 'block_id');
		$this->createIndex('idx-floor-house-id', '{{%floor}}', 'house_id');
		$this->createIndex('idx-floor-pd-id', '{{%floor}}', 'pd_id');

		$this->createTable('{{%flat}}', [
			'id'         => Schema::TYPE_PK,
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'mm'         => Schema::TYPE_STRING . '(255) NOT NULL DEFAULT 0',
			'price'      => Schema::TYPE_STRING . '(255) NOT NULL DEFAULT 0',
			'status'     => Schema::TYPE_SMALLINT . '(2) NOT NULL DEFAULT 0',
			'number'     => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'rooms'      => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'area'       => Schema::TYPE_STRING . '(2000) NOT NULL',
			'complex_id' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'block_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'house_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'pd_id'      => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'floor_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);
		$this->createIndex('idx-flat-name', '{{%flat}}', 'name');
		$this->createIndex('idx-flat-complex-id', '{{%flat}}', 'complex_id');
		$this->createIndex('idx-flat-block-id', '{{%flat}}', 'block_id');
		$this->createIndex('idx-flat-house-id', '{{%flat}}', 'house_id');
		$this->createIndex('idx-flat-pd-id', '{{%flat}}', 'pd_id');
		$this->createIndex('idx-flat-price', '{{%flat}}', 'price');
		$this->createIndex('idx-flat-mm', '{{%flat}}', 'mm');
		$this->createIndex('idx-flat-rooms', '{{%flat}}', 'rooms');
	}

	public function down()
	{
		$this->dropTable('{{%complex}}');
		$this->dropTable('{{%block}}');
		$this->dropTable('{{%house}}');
		$this->dropTable('{{%pd}}');
		$this->dropTable('{{%floor}}');
		$this->dropTable('{{%flat}}');
	}
}
