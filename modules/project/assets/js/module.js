function updateSvg(eWidth, eHeight, left, top){

	var svg = $('.svg');
	var svgHeight = svg.attr('data-height');
	var svgWidth = svg.attr('data-width');

	var koofH = eHeight / svgHeight;
	var koofW = eWidth / svgWidth;

	svg.find('g').attr('transform','matrix('+koofW+',0,0,'+koofH+',0,0)');
	svg.find('g').attr('data-koof',koofW);
}