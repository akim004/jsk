<?php

use app\modules\project\Module;
use app\modules\project\components\UrlP;
use yii\helpers\Html;


if(Yii::$app->request->get('complex')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Complex'), 'url' => ['complex/index']];
}
if(Yii::$app->request->get('block')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Block'), 'url' => UrlP::toRoute(['block/index'])];
}

$this->title = Module::t('module', 'Update {modelClass}: ', [
    'modelClass' => 'House',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'House'), 'url' => UrlP::toRoute(['index'])];
$this->params['breadcrumbs'][] = Module::t('module', 'Update');
?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
