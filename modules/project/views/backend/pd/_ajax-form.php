<?php

use app\modules\project\Module;
use app\modules\project\models\backend\Block;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="news-form">
	<?php $form = ActiveForm::begin([
		'id' => 'ajax-form',
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>
