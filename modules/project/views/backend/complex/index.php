<?php

use app\modules\project\Module;
use app\modules\project\models\backend\Complex;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Complex\models\ComplexSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'Complex');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Complex-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Module::t('module', 'Create complex'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'name',
			[
				'format' => 'html',
				'label' => Module::t('module', 'Blocks'),
				'value' => function($data) {
					return Html::a(Module::t('module', 'Blocks'), ['block/index', 'complex' => $data->id]);
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}{delete}',
				'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
			],
		],
	]); ?>

</div>
