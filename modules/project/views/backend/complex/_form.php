<?php

use app\modules\project\Module;
use app\modules\project\models\backend\Complex;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="news-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<?= $form->errorSummary($model); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
		<li><a href="#settings">SEO</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<div >
				<?$preview = $model->getImage() ? [Html::img('/'.$model->getImage()->getPathToOrigin(), ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon']),] : false;

					echo $form->field($model, 'imageUpload')->widget(FileInput::classname(), [
						'options' => [
							'accept' => 'imageUpload/*',
							'class' => 'for-remove-function',
						],
						'pluginOptions' => [
							'maxImageWidth' => $model->maxImageWidth,
							'maxImageHeight' => $model->maxImageHeight,
							'initialPreview'   => $preview,
							'initialCaption'   => "Загрузите фото",
							'overwriteInitial' => true,
							'showUpload'       => false,
							'allowedFileExtensions' => ['jpg','gif','png'],
						]
					]);
				?>
			</div>
			<?= $form->field($model, 'isRemoveImage')->hiddenInput(['class' => 'removeImage'])->label(false); ?>
		</div>
		<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Module::t('module', 'apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::a(Module::t('module', 'cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
