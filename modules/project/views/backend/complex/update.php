<?php

use app\modules\project\Module;
use yii\helpers\Html;

$this->title = Module::t('module', 'Update {modelClass}: ', [
    'modelClass' => 'Complex',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Complex'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('module', 'Update').' : '.$model->name;
?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
