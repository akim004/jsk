<?php

use app\modules\project\Module;
use yii\helpers\Html;


$this->title = Module::t('module', 'Create complex');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Complex'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
