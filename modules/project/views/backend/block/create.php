<?php

use app\modules\project\Module;
use app\modules\project\components\UrlP;
use yii\helpers\Html;

if(Yii::$app->request->get('complex')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Complex'), 'url' => ['complex/index']];
}
$this->title = Module::t('module', 'Create block');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Block'), 'url' => UrlP::toRoute(['index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
