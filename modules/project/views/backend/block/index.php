<?php

use app\modules\project\Module;
use app\modules\project\components\UrlP;
use app\modules\project\models\backend\Block;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Block\models\BlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'Block');
if(Yii::$app->request->get('complex')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Complex'), 'url' => ['complex/index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Block-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<p>
		<?= Html::a(Module::t('module', 'Create block'), UrlP::toRoute(['create']), ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'name',
			[
				'format' => 'html',
				'label' => Module::t('module', 'Houses'),
				'value' => function($data) {
					return Html::a(Module::t('module', 'Houses'), ['house/index', 'complex' => $data->complex_id, 'block' => $data->id,]);
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}{delete}',
				'urlCreator'=>function($action, $model, $key, $index){
					return UrlP::toRoute([$action, 'id' => $model->id]);
				},
				'contentOptions' => [
					'style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;',
				],
			],
		],
	]); ?>

</div>
