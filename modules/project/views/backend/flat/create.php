<?php

use app\modules\project\Module;
use app\modules\project\components\UrlP;
use yii\helpers\Html;

if(Yii::$app->request->get('complex')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Complex'), 'url' => ['complex/index']];
}
if(Yii::$app->request->get('block')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Block'), 'url' => UrlP::toRoute(['block/index'])];
}
if(Yii::$app->request->get('house')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'House'), 'url' => UrlP::toRoute(['house/index'])];
}
if(Yii::$app->request->get('pd')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Pd'), 'url' => UrlP::toRoute(['pd/index'])];
}
if(Yii::$app->request->get('floor')){
	$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Floor'), 'url' => UrlP::toRoute(['floor/index'])];
}
$this->title = Module::t('module', 'Create flat');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Flat'), 'url' => UrlP::toRoute(['index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
