<?php

use app\modules\project\Module;
use app\modules\project\models\backend\Block;
use app\modules\project\models\backend\Flat;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="news-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<?= $form->errorSummary($model); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
		<li><a href="#settings">SEO</a></li>
		<li><a href="#area">Выделения</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?//= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'mm')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'rooms')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'status')->dropDownList(Flat::getStatusesArray()) ?>
			<?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
			<div >
				<?$preview = $model->getImage() ? [Html::img('/'.$model->getImage()->getPathToOrigin(), ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon']),] : false;

					echo $form->field($model, 'imageUpload')->widget(FileInput::classname(), [
						'options' => [
							'accept' => 'imageUpload/*',
							'class' => 'for-remove-function',
						],
						'pluginOptions' => [
							'maxImageWidth' => $model->maxImageWidth,
							'maxImageHeight' => $model->maxImageHeight,
							'initialPreview'   => $preview,
							'initialCaption'   => "Загрузите фото",
							'overwriteInitial' => true,
							'showUpload'       => false,
							'allowedFileExtensions' => ['jpg','gif','png'],
						]
					]);
				?>
			</div>
			<?= $form->field($model, 'isRemoveImage')->hiddenInput(['class' => 'removeImage'])->label(false); ?>
		</div>
		<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>
		<div class="tab-pane x_content" id="area">
			<div id="content-holder" style="position:relative;width:1000px;height: 500px;overflow: auto;">
				<img src="<?=$model->getFloorImageUrl()?>">
			</div>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Module::t('module', 'apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::a(Module::t('module', 'cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

<?

$points = \app\modules\project\models\backend\Complex::getShapePluginObject($model->area);

$script = <<< JS
	$(function(){
		$('#content-holder').polyDraw({
			pointCount: 3,
			WhereToSaveId: 'flat-area',
			points: $points,
		});
	})
JS;

$this->registerJs($script, yii\web\View::POS_END);?>

	<?php ActiveForm::end(); ?>

</div>
