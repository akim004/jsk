<?php


$pdImage = $pd->getImage();
$imageSize = $pdImage->getSizes();
?>

<div class="news-default-index">
	<div class="cover" id="js-big-image">
			<img  src="/<?=$pdImage->getPath()?>">
	    <svg class="svg" data-width="<?=$imageSize['width']?>" data-height="<?=$imageSize['height']?>">
			<g >
				<?foreach ($floors as $data) {?>
						<path

							id="path<?=$data->id?>"
							data-edit="admin"
							data-points = '<?=\app\modules\project\models\backend\Complex::getShapePluginObject($data->area);?>'
							data-to-save = "floor-area",
							data-edit-url="<?=\yii\helpers\Url::to(['/admin/project/floor/update', 'id' => $data->id]);?>"

							data-id="<?=$data->id?>"
							data-name="<?=$data->name?>"
							d="<?=Yii::$app->getModule('project')->conversionCoordCanvasToSvg($data->area);?>"
							fill="rgba(255,255,255,0.1)"
						/>
				<?}?>
			</g>
		</svg>
	</div>
</div>

<div class="admin-edit">
	<a href="/google.com"></a>
</div>