<?php


$complexImage = $complex->getImage();
$imageSize = $complexImage->getSizes();
?>

<div class="news-default-index">
	<div class="cover" id="js-big-image">
			<img  src="/<?=$complexImage->getPath()?>">
	    <svg class="svg" data-width="<?=$imageSize['width']?>" data-height="<?=$imageSize['height']?>">
			<g id="get-pd">
				<?foreach ($pds as $data) {?>
						<path

							id="path<?=$data->id?>"
							data-edit="admin"
							data-points = '<?=\app\modules\project\models\backend\Complex::getShapePluginObject($data->area);?>'
							data-to-save = "pd-area",
							data-edit-url="<?=\yii\helpers\Url::to(['/admin/project/pd/update', 'id' => $data->id]);?>"

							data-id="<?=$data->id?>"
							data-name="<?=$data->name?>"
							d="<?=Yii::$app->getModule('project')->conversionCoordCanvasToSvg($data->area);?>"
							fill="rgba(255,255,255,0.1)"
						/>
				<?}?>
			</g>
		</svg>
	</div>
</div>

<div class="admin-edit">
	<a href="/google.com"></a>
</div>
