<?php

namespace app\modules\project\components;

class UrlP extends \yii\helpers\BaseUrl
{
    public static function toRoute($route, $scheme = false){

    	if (is_array($route)) {
    		$url = $route + \Yii::$app->request->get();
        }

    	return parent::toRoute($url, $scheme = false);
    }
}