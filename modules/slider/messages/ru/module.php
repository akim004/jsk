<?php

return [
	'Sliders'               => 'Баннеры',
	'Slider'               => 'Баннер',
	'Create Slider'         => 'Создать баннер',
	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать баннер: ',

	'Create' => 'Создать',
	'Apply'  => 'Сохранить',
	'Cancel' => 'Отменить',
	'Update and close' => 'Сохранить и выйти',

	'successfully added' => 'Баннер успешно добавлен',
	'successfully changed' => 'Баннер успешно изменен',

	'id'          => 'ID',
	'created_at'  => 'Создан',
	'updated_at'  => 'Обновлен',
	'id'          => 'ID',
	'name'        => 'Заголовок',
	'short'       => 'Короткое описание',
	'description' => 'Полное описание',
	'slug'        => 'Алиас',
	'published'   => 'Статус',
	'position'    => 'Позиция в списке',
	'imageUpload' => 'Картинка',
	
	'Active' => 'Активный',
	'Hidden' => 'Скрытый',
];