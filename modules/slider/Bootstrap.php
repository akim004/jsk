<?php

namespace app\modules\slider;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    use \app\modules\slider\ModuleTrait;

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/slider/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@app/modules/slider/messages',
            'fileMap' => [
                'modules/slider/module' => 'module.php',
            ],
        ];

        $app->urlManager->registerModuleRules($this->getModule());
    }
}