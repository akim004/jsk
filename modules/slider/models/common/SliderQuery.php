<?php

namespace app\modules\slider\models\common;

use \app\modules\slider\models\common\Slider;

class SliderQuery extends \yii\db\ActiveQuery
{
	public function active()
	{
		return $this->andWhere(['published' => Slider::ACTIVE]);
	}
}