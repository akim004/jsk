<?php

namespace app\modules\slider\models\common;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\slider\models\common\Slider;

/**
 * SliderSearch represents the model behind the search form about `app\modules\Slider\models\Slider`.
 */
class SliderSearch extends Slider
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'created_at', 'updated_at', 'position', 'published'], 'integer'],
			[['name', 'short', 'description', 'slug'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Slider::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => false,
			'sort'=> [
				'defaultOrder' => ['position' => SORT_ASC],
			],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id'         => $this->id,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'position'   => $this->position,
			'published'  => $this->published,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'short', $this->short])
			->andFilterWhere(['like', 'description', $this->description])
			->andFilterWhere(['like', 'slug', $this->slug]);

		return $dataProvider;
	}
}
