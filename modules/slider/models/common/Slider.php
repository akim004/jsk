<?php

namespace app\modules\slider\models\common;

use Yii;
use app\modules\slider\Module;
use app\modules\slider\models\common\SliderQuery;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property integer $
 * @property integer $updated_at
 * @property string $name
 * @property string $short
 * @property string $description
 * @property integer $date
 * @property integer $position
 * @property string $slug
 * @property integer $published
 */
class Slider extends \yii\db\ActiveRecord
{
	/**
	 * Загружаемая картинка
	 * @var UploadedFile
	 */
	public $imageUpload;

	/**
	 * Флаг проверки удалить ли картинку
	 * @var bool
	 */
	public $isRemoveImage;

	const ACTIVE = 0;
	const NOT_ACTIVE = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%slider}}';
	}

	public static function find()
	{
		$sliderQuery = new SliderQuery(get_called_class());
		return $sliderQuery->orderBy(['position'=> SORT_ASC]);
	}

	public function behaviors()
	{
		return [
			['class' => \yii\behaviors\TimestampBehavior::className()],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 1,
				/*'maxImageWidth' => '1000',
				'maxImageHeight' => '1000',
				'maxSize' => '5',*/
			],
		];


	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['description'], 'string'],
			[['name', 'slug'], 'string', 'max' => 255],
			[['short'], 'string', 'max' => 500],
			[['position', 'isRemoveImage', 'published'], 'safe'],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'id'),
			'created_at'  => Module::t('module', 'created_at'),
			'updated_at'  => Module::t('module', 'updated_at'),
			'id'          => Module::t('module', 'id'),
			'name'        => Module::t('module', 'name'),
			'short'       => Module::t('module', 'short'),
			'description' => Module::t('module', 'description'),
			'slug'        => Module::t('module', 'slug'),
			'published'   => Module::t('module', 'published'),
			'position'    => Module::t('module', 'position'),
			'imageUpload' => Module::t('module', 'imageUpload'),
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE => Module::t('module', 'Active'),
			self::NOT_ACTIVE  => Module::t('module', 'Hidden'),
		];
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::ACTIVE,
				'text' => Module::t('module', 'Active'),
			],
			[
				'value' => self::NOT_ACTIVE,
				'text' => Module::t('module', 'Hidden'),
			],
		];
	}

	public function afterDelete() {
		parent::afterDelete();
		if($this->getImage()){
			$this->removeImage($this->getImage());
		}
	}

	public function saveImage($file)
	{
		$image = $this->getImage();

		if($this->isRemoveImage && $image){
			$this->removeImage($image);
		}

		if($file){
			if($image){
				$this->removeImage($image);
			}

			$file->saveAs('uploads/runtime/'.$file->name);
			$this->attachImage('uploads/runtime/'.$file->name);
			@unlink('uploads/runtime/'.$file->name);
		}
	}

	public function beforeSave($insert)
	{
	    if (parent::beforeSave($insert)) {
	 		$this->name = preg_replace('/<p[^>]*?>/', '', $this->name);
			$this->name = str_replace('</p>', '<br />', $this->name);

	        return true;
	    }
	    return false;
	}

	public function getImageUrl($size = '')
	{
		return $this->image ? $this->image->getPath($size, true) : '';
		// return $this->image ? 'uploads/cache/b/'.$this->image->urlAlias.'.jpg' : '';
	}

	public static function getImagesUrlArray()
	{
		$array = [];
		$sliders = self::find()->active()->all();
		foreach ($sliders as $slider) {
			$array[] = '/'.$slider->getImageUrl('', true);
		}

		return $array ? implode(',', $array) : '';
	}

}
