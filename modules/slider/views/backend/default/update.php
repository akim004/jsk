<?php

use app\modules\slider\Module;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\slider */

$this->title = Module::t('module', 'Update {modelClass}: ', [
    'modelClass' => 'Sliders',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Sliders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('module', 'Update');
?>
<div class="Slider-update">

    <h1><?= $this->title ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
