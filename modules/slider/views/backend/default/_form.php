<?php

use app\modules\slider\Module;
use app\modules\slider\models\common\Slider;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="Slider-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">

			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?/*= $form->field($model, 'name')->widget(\vova07\imperavi\Widget::className(), [
			    'settings' => [
			        'lang' => 'ru',
			        'minHeight' => 50,
			        'buttons' => array('html', 'bold'),
			        'paragraphize' => false,
			        'pastePlainText' => true,
			        // 'plugins' => [
			        //     'clips',
			        //     'fullscreen'
			        // ]
			    ]
			]);*/?>

			<?//= $form->field($model, 'short')->textarea(['maxlength' => true]) ?>

			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'published')->dropDownList(Slider::getStatusesArray()) ?>

			<div >
				<?$preview = $model->getImage() ? [Html::img('/'.$model->getImage()->getPathToOrigin(), ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon']),] : false;

					echo $form->field($model, 'imageUpload')->widget(FileInput::classname(), [
						'options' => [
							'accept' => 'imageUpload/*',
							'class' => 'for-remove-function',
						],
						'pluginOptions' => [
							'maxImageWidth' => $model->maxImageWidth,
							'maxImageHeight' => $model->maxImageHeight,
							'initialPreview'   => $preview,
							'initialCaption'   => "Загрузите картинку",
							'overwriteInitial' => true,
							'showUpload'       => false,
							'allowedFileExtensions' => ['jpg','gif','png'],
						]
					]);
				?>
			</div>
			<?= $form->field($model, 'isRemoveImage')->hiddenInput(['class' => 'removeImage'])->label(false); ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton(Module::t('module', 'Apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update and close'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
		<?= Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
