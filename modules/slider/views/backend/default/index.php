<?php

use app\modules\slider\Module;
use app\modules\slider\models\common\Slider;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Slider\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'Sliders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Module::t('module', 'Create Slider'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php \yii\widgets\Pjax::begin([
		'id' => 'pjax-container',
		'timeout' => 20000,
	]); ?>
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'name',
				[
					'class'     => \app\components\EditableColumn::className(),
					'type'      => 'select',
					'url'       => ['editable'],
					'attribute' => 'published',
					'format' => 'raw',
					'filter'    => Slider::getStatusesArray(),
					'value' => function($data) {
						return Slider::getStatusesArray()[$data->published];
					},
					'editableOptions' => [
						'mode' => 'pop', // 'inline'
						'source' => json_encode(Slider::getStatusesArrayForSource()),
						'placement' => 'bottom',
					],
				],
				[
					'format' => 'raw',
					'header' => 'Изображение',
					'value' => function($data){
						return $data->getImageUrl('100x') ? Html::img('/'.$data->getImageUrl('100x')) : '';
					},
				],
				// 'short',
				// 'created_at',
				// 'updated_at',
				// 'description:ntext',
				// 'date',
				// 'position',
				// 'slug',
				// 'published',

				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{update}{delete}',
					'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
					'buttons' => [
						'delete' => function ($url) {
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
								'title' => Module::t('module', 'Delete'),
								'aria-label' => Module::t('module', 'Delete'),
								'onclick' => "
									if (confirm('Удалить баннер?')) {
										$.ajax('$url', {
											type: 'POST'
										}).done(function(data) {

											$.pjax.reload({container: '#pjax-container', timeout: 10000});
										});
									}
									return false;
								",
							]);
						},
					],
				],
			],
		]); ?>
	<?php \yii\widgets\Pjax::end(); ?>

</div>
