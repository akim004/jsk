<?php

use app\modules\slider\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\banner\models\Slider */

$this->title = Module::t('module', 'Create Slider');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Slider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
