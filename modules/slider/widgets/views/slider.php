<?if($sliders){?>
	<div class="inner">
		<ul>
			<?foreach ($sliders as $slider) {?>
				<li>
					<a href="<?=$slider->slug ? $slider->slug : '#'?>" class="bg">
						<img src="<?=$slider->getImageUrl()?>" alt=""/>
					</a>
					<?/*<div class="entry">
						<div class="text">
							Официальный представитель
						</div>
						<div class="title">
							ITALICA
						</div>
						<div class="bt">
							<a href="">в каталог</a>
						</div>
					</div>*/?>
				</li>
			<?}?>
		</ul>
	</div>
	<?if(count($sliders) > 1){?>
		<div class="arr-l"></div>
		<div class="arr-r"></div>
	<?}?>

	<div class="page"></div>
<?}?>