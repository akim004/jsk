<?php
namespace app\modules\slider\widgets;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use \app\modules\slider\models\common\Slider;

class SliderView extends Widget
{
    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $sliders = Slider::find()->active()->all();

        echo $this->render('slider', [
            'sliders' => $sliders
        ]);
    }

}