<?php

use yii\db\Migration;
use yii\db\Schema;

class m160804_143232_table_banner extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%slider}}', [
			'id'          => Schema::TYPE_PK,
			'created_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'short'       => Schema::TYPE_STRING . '(500) NULL DEFAULT NULL',
			'module'      => Schema::TYPE_STRING . '(128) NULL DEFAULT NULL',
			'description' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
			'position'    => Schema::TYPE_INTEGER . '(2) NULL DEFAULT NULL DEFAULT 0',
			'item_id'     => Schema::TYPE_INTEGER . '(11) NULL DEFAULT NULL DEFAULT 0',
			'slug'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'published'   => Schema::TYPE_SMALLINT . '(2) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createIndex('idx_slider_name', '{{%slider}}', 'name');
		$this->createIndex('idx_slider_slug', '{{%slider}}', 'slug');
	}

	public function down()
	{
		$this->dropTable('{{%slider}}');
	}
}
