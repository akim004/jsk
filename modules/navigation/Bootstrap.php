<?php

namespace app\modules\navigation;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	use \app\modules\navigation\ModuleTrait;

	public function bootstrap($app)
	{
		$app->i18n->translations['modules/navigation/*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'forceTranslation' => true,
			'basePath' => '@app/modules/navigation/messages',
			'fileMap' => [
				'modules/navigation/module' => 'module.php',
			],
		];

		$app->urlManager->registerModuleRules($this->getModule());

		\Yii::$container->set('app\modules\navigation\components\NavigationInterface', 'app\modules\navigation\models\common\Navigation');

	}
}