<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/navigation/migrations --interactive=0
 *
 * composer require creocoder/yii2-nested-sets
 * composer require zelenin/yii2-slug-behavior "~1.5.1"
 *
 * https://github.com/CostaRico/yii2-images
 */

namespace app\modules\navigation;

use Yii;

class Module extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\navigation\controllers';

    public function init()
	{
		parent::init();
		// custom initialization code goes here
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/navigation/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [];
	}
}
