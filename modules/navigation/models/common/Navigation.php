<?php

namespace app\modules\navigation\models\common;

use Yii;
use app\modules\navigation\Module;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class Navigation extends ActiveRecord implements \app\modules\navigation\components\NavigationInterface
{
	const ACTIVE = 1;
	const NOT_ACTIVE = 0;


	public static function tableName()
	{
		return '{{%navigation}}';
	}

	public function behaviors()
	{
		return [
			'tree' => [
				'class' => NestedSetsBehavior::className(),
				'treeAttribute'  => 'root',
				'leftAttribute'  => 'lft',
				'rightAttribute' => 'rgt',
				'depthAttribute' => 'level',
			],
		];
	}

	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_ALL,
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE     => 'Активный',
			self::NOT_ACTIVE => 'Скрытый',
		];
	}

	public static function find()
	{
		$navigationQuery = new NavigationQuery(get_called_class());

		return $navigationQuery->orderBy('lft');
	}

	public function rules()
	{
		return [
			[['name'], 'required'],
			[['name', 'slug'], 'string', 'max' => 255],
			[['position'], 'safe'],
			[['status'], 'integer', 'integerOnly' => true],
			[['name', 'slug'], 'safe', 'on' => 'search'],
		];
	}

	public function attributeLabels(){

		return [
			'id'          => Module::t('module', 'id'),
			'name'        => Module::t('module', 'name'),
			'slug'        => Module::t('module', 'slug'),
			'position'    => Module::t('module', 'position'),
			'status'      => Module::t('module', 'status'),
		];
	}

	public function scenarios(){
		return Model::scenarios();
	}

	// Родитель
	public function getParent()
	{
		return $this->isRoot() ? 0 : $this->parents(1)->one();
	}

	/**
	 * В древовидном виде
	 * @return [type] [description]
	 */
	public static function getTree($withoutId)
	{
		$tree = [];
		$list = self::find()->roots()->all();
		foreach ($list as $data) {
			if($data->id == $withoutId){
				continue;
			}
			$tree[$data->id] = $data->name;
			foreach ($data->children()->all() as $children) {
				if($children->id == $withoutId){
					continue;
				}
				$tree[$children->id] = str_repeat("-- ", $children->level).$children->name;
			}
		}
		return $tree;
	}

	public static function getListData($modelId = null)
	{
		$tree =self::getTree($modelId);

		return $tree;
	}

}
