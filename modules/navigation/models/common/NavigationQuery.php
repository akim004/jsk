<?php

namespace app\modules\navigation\models\common;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use \app\modules\navigation\models\common\Navigation;

class NavigationQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function active()
	{
		return $this->andWhere(['status' => Navigation::ACTIVE]);
	}
}