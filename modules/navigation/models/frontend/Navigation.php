<?php

namespace app\modules\navigation\models\frontend;

use Yii;
use app\modules\navigation\Module;
use yii\helpers\Url;

class Navigation extends \app\modules\navigation\models\common\Navigation
{
	public function getCreateUrl()
	{
		return Url::to([''.$this->slug]);
	}
}
