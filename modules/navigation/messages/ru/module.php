<?php

return [
	'CREATE NAVIGATION' => 'Создать навигацию',
	'NAVIGATIONS' => 'Навигации',
	'NAVIGATION' => 'Навигация',
	'CREATE' => 'Создать',
	'DELETE' => 'Удалить',
	'UPDATE' => 'Редактировать',
	'UPDATE NAVIGATION' => 'Редактировать навигацию',
	'ARE YOU SURE YOU WANT TO DELETE THIS ITEM?' => 'Вы уверены, что хотите удалить?',

	'successfully added' => 'Навигация успешно добавлена',
	'successfully changed' => 'Навигация успешно изменена',

	'apply' => 'Применить',
	'cancel' => 'Отмена',

	'id'          => 'ID',
	'name'        => 'Название',
	'slug'        => 'Алиас',
	'position'    => 'Позиция',
	'status'      => 'Статус',
	'imageUpload' => 'Картинка',
	'parentId'    => 'Родитель',
	'Page'        => 'Страница',

	'Count subcategory' => 'Количесто подменю',
];