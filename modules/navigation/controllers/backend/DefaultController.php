<?php

namespace app\modules\navigation\controllers\backend;

use Yii;
use app\components\BackendController;
use app\modules\navigation\Module;
use app\modules\navigation\models\backend\Navigation;
use dosamigos\editable\EditableAction;
use dosamigos\grid\ToggleAction;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class DefaultController extends BackendController
{
	public $title = 'Навигация';
	public $breadcrumbs = [];

	public function actions()
	{
		return [
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridAction::className(),
				'modelName' => Navigation::className(),
			],
			/*'editable' => [
				'class' => EditableAction::className(),
				'modelClass' => Navigation::className(),
				'forceCreate' => false
			],*/
			'toggle' => [
				'class' => ToggleAction::className(),
				'modelClass' => Navigation::className(),
				'onValue' => 1,
				'offValue' => 0,
			],

		];
	}


	public function beforeAction($action)
	{
		if($parent = Navigation::findOne(['id'=>Yii::$app->request->get('parent', 0)])){
			$this->breadcrumbs = $parent->adminBreadcumbs;
		}
		array_unshift($this->breadcrumbs, ['label' => $this->title, 'url' => ['index']]);
		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		$searchModel = new Navigation(['scenario'=>'search']);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate()
	{
		$parentID = Yii::$app->request->get('parent');

		$model = new Navigation();
		$model->parentId = $parentID;

		if($model->load(Yii::$app->request->post())){
			$model->imageUpload = UploadedFile::getInstance($model, 'imageUpload');
			if ($model->saveNav($model->parentId)) {
				$model->saveImage($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully added'));

				if(isset($_GET['wait'])){
					return $this->redirect(['update', 'parent'=>$parentID, 'id' => $model->id, 'wait' => 0]);
				}

				return $this->redirect(['index', 'parent'=>$parentID]);
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->parentId = $model->parents(1)->one() ? $model->parents(1)->one()->id : 0;
		$model->page = $model->slug;

		if($model->load(Yii::$app->request->post())){
			$model->imageUpload = UploadedFile::getInstance($model, 'imageUpload');
			if ($model->saveNav($model->parentId)) {
				$model->saveImage($model->imageUpload);
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully changed'));

				if(isset($_GET['wait'])){
					return $this->redirect(['update', 'parent'=>$model->parentId, 'id' => $model->id, 'wait' => 0]);
				}
				return $this->redirect(['index', 'parent'=>$model->parentId]);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->deleteWithChildren();

		return $this->redirect(['index', 'parent'=>Yii::$app->request->get('parent')]);
	}

	protected function findModel($id)
	{
		if (($model = Navigation::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
