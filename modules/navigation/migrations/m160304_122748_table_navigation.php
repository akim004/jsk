<?php

use yii\db\Schema;
use yii\db\Migration;

class m160304_122748_table_navigation extends Migration
{
   public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%navigation}}', [
			'id'       => Schema::TYPE_PK,
			'name'     => Schema::TYPE_STRING . '(128) NOT NULL UNIQUE',
			'slug'     => Schema::TYPE_STRING . '(128) NOT NULL',
			'position' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'status'   => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0',
			'lft'      => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'rgt'      => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'level'    => Schema::TYPE_INTEGER . '(11) NOT NULL',
			'root'     => Schema::TYPE_INTEGER . '(11) NOT NULL',
		], $tableOptions);

		// $this->createIndex('idx_user_status', '{{%user}}', 'status');
	}

	public function down()
	{
		$this->dropTable('{{%navigation}}');
	}
}
