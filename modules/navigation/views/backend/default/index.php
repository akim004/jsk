<?php

use app\modules\navigation\Module;
use app\modules\navigation\models\backend\Navigation;
use yii\helpers\Html;
// use \yiister\gentelella\widgets\grid\GridView;
use himiklab\sortablegrid\SortableGridView as GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\navigation\models\NavigationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $this->context->title;

$parent = array_pop($this->context->breadcrumbs);
$this->context->breadcrumbs[] = isset($parent['label']) ? $parent['label'] : $this->title;

$this->params['breadcrumbs'] = $this->context->breadcrumbs;
?>
<div class="navigation-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
		<?= Html::a(Module::t('module', 'CREATE NAVIGATION'), ['create', 'parent'=>Yii::$app->request->get('parent')], ['class' => 'btn btn-success']) ?>
	</p>
<?php \yii\widgets\Pjax::begin(); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'format' => 'raw',
				'attribute' => 'name',
				'value' => function($data) {
					return Html::a($data->name, ['index', 'parent' => $data->id], ['data-pjax' => 0]);
				}
			],
			/*[
				'class'  => dosamigos\grid\EditableColumn::className(),
				'type' => 'text',
				'url' => ['editable'],
				'attribute' => 'name',
				'editableOptions' => [
					'mode' => 'pop', // 'inline'
				]
			],
			[
				'class'  => dosamigos\grid\EditableColumn::className(),
				'type' => 'text',
				'url' => ['editable'],
				'attribute' => 'slug',
				'editableOptions' => [
					'mode' => 'pop',
				]
			],*/
			[
			    'class' => \dosamigos\grid\ToggleColumn::className(),
			    'attribute' => 'status',
			    // 'onValue' => 1,
			    'onLabel' => Navigation::getStatusesArray()[Navigation::ACTIVE],
			    'offLabel' => Navigation::getStatusesArray()[Navigation::NOT_ACTIVE],
			    'contentOptions' => ['class' => 'text-center'],
			    'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
			    'filter' => Navigation::getStatusesArray(),
			    'contentOptions' => ['style' => 'text-align: center;width: 120px'],
			],
			[
				'header' => Module::t('module', 'Count subcategory'),
				'value' => function($data){
					return count($data->children()->all());
				},
				'contentOptions' => ['style' => 'text-align: center;width: 100px'],
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}{delete}',
				'urlCreator'=>function($action, $model, $key, $index){
				   return [$action,'id'=>$model->id,'parent'=>Yii::$app->request->get('parent')];
				},
				'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
			],
		],
	]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
