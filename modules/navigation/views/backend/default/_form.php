<?php

use app\modules\navigation\Module;
use app\modules\navigation\models\backend\Navigation;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="navigation-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<?= $form->errorSummary($model) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'parentId')->dropDownList(['0'=>'Нет']+Navigation::getListData()) ?>

	<?= $form->field($model, 'status')->dropDownList(Navigation::getStatusesArray()) ?>

	<?= $form->field($model, 'page')->dropDownList(Navigation::getPageList()) ?>
	<?//= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>

	<div >
		<?$preview = $model->getImage() ? [Html::img('/'.$model->getImage()->getPathToOrigin(), ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon']),] : false;

			echo $form->field($model, 'imageUpload')->widget(FileInput::classname(), [
				'options' => [
					'accept' => 'imageUpload/*',
					'class' => 'for-remove-function',
				],
				'pluginOptions' => [
					'maxImageWidth' => $model->maxImageWidth,
					'maxImageHeight' => $model->maxImageHeight,
					'initialPreview'   => $preview,
					'initialCaption'   => "Загрузите картинку",
					'overwriteInitial' => true,
					'showUpload'       => false,
					'allowedFileExtensions' => ['jpg','gif','png'],
				]
			]);
		?>
	</div>
	<?= $form->field($model, 'isRemoveImage')->hiddenInput(['class' => 'removeImage'])->label(false); ?>

	<hr />

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'CREATE') : Module::t('module', 'UPDATE'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Module::t('module', 'apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::a(Module::t('module', 'cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
