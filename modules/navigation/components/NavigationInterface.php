<?php

namespace app\modules\navigation\components;

interface NavigationInterface
{
	/**
	 * Список пунктов меню для привязки к странице
	 * @return array ['alias' => 'name']
	 */
    public static function getListData();

}