<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/blockText/migrations --interactive=0
 *
 * \himiklab\sortablegrid\SortableGridBehavior
 */

namespace app\modules\blockText;

use Yii;

class Module extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\blockText\controllers';

    public function init()
	{
		parent::init();

		// custom initialization code goes here
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/blockText/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [];
	}
}
