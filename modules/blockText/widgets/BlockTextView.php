<?php
namespace app\modules\blockText\widgets;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use \app\modules\blockText\models\common\BlockText;

class BlockTextView extends Widget
{
    public $param;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $blockText = \app\modules\blockText\models\common\BlockText::find()->active()->andWhere(['param' => $this->param])->one();

        echo $this->render('blockText', [
            'blockText' => $blockText
        ]);
    }

}