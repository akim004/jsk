<?php

use yii\db\Migration;
use yii\db\Schema;

class m160810_084634_table_block_text extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%block_text}}', [
            'id'          => Schema::TYPE_PK,
            'created_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
            'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
            'description' => Schema::TYPE_TEXT ,
            'position'    => Schema::TYPE_INTEGER . '(2) NULL DEFAULT NULL DEFAULT 0',
            'published'   => Schema::TYPE_SMALLINT . '(2) NOT NULL DEFAULT 0',
            'param'       => Schema::TYPE_STRING . '(255) UNIQUE NOT NULL',
            'module'        => Schema::TYPE_STRING . '(30) NULL',
        ], $tableOptions);

        $this->createIndex('idx_block-text-name', '{{%block_text}}', 'name');
    }

    public function down()
    {
        $this->dropTable('{{%block_text}}');
    }
}
