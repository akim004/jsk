<?php

use app\modules\blockText\Module;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\blockText\models\BlockText */

$this->title = Module::t('module', 'Update {modelClass}: ', [
    'modelClass' => 'BlockTexts',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'BlockTexts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('module', 'Update');
?>
<div class="banner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
