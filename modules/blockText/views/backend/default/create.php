<?php

use app\modules\banner\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\banner\models\Banner */

$this->title = Module::t('module', 'Create Banner');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Banner'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
