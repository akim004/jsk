<?php

use app\modules\blockText\Module;
use app\modules\blockText\models\common\BlockText;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Banner\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'BlockTexts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('module', 'Create BlockText'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'class'     => \app\components\EditableColumn::className(),
                'type'      => 'select',
                'url'       => ['editable'],
                'attribute' => 'published',
                'format' => 'raw',
                'filter'    => BlockText::getStatusesArray(),
                'value' => function($data) {
                    return BlockText::getStatusesArray()[$data->published];
                },
                'editableOptions' => [
                    'mode' => 'pop', // 'inline'
                    'source' => json_encode(BlockText::getStatusesArrayForSource()),
                    'placement' => 'bottom',
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
            ],
        ],
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
