<?php

namespace app\modules\blockText\models\common;

use Yii;
use app\modules\blockText\Module;

class BlockText extends \yii\db\ActiveRecord
{
	const ACTIVE = 0;
	const NOT_ACTIVE = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%block_text}}';
	}

	public static function find()
	{
		return new BlockTextQuery(get_called_class());
	}

	public function behaviors()
	{
		return [
			['class' => \yii\behaviors\TimestampBehavior::className()],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		];


	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['description'], 'string'],
			[['name', 'param'], 'string', 'max' => 255],
			[['position', 'published'], 'safe'],
			[['param'], 'unique'],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'id'),
			'created_at'  => Module::t('module', 'created_at'),
			'updated_at'  => Module::t('module', 'updated_at'),
			'id'          => Module::t('module', 'id'),
			'name'        => Module::t('module', 'name'),
			'description' => Module::t('module', 'description'),
			'slug'        => Module::t('module', 'slug'),
			'published'   => Module::t('module', 'published'),
			'position'    => Module::t('module', 'position'),
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE => 'Активный',
			self::NOT_ACTIVE  => 'Скрытый',
		];
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::ACTIVE,
				'text' => 'Активный',
			],
			[
				'value' => self::NOT_ACTIVE,
				'text' => 'Скрытый',
			],
		];
	}

	public function afterDelete() {
		parent::afterDelete();
	}
}
