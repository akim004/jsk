<?php

namespace app\modules\blockText\models\common;

use \app\modules\blockText\models\common\BlockText;

class BlockTextQuery extends \yii\db\ActiveQuery
{
	public function active()
	{
		return $this->andWhere(['published' => BlockText::ACTIVE]);
	}
}