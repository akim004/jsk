<?php

return [
	'BlockTexts'               => 'Текстовые блоки',
	'Create BlockText'         => 'Создать текстовый блок',
	'Create'                => 'Создать',
	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать текстовый блок: ',

	'apply' => 'Применить',
	'cancel' => 'Отмена',

	'successfully added' => 'Блок успешно добавлен',
	'successfully changed' => 'Блок успешно изменен',

	'id'          => 'ID',
	'created_at'  => 'Создан',
	'updated_at'  => 'Обновлен',
	'name'        => 'Заголовок',
	'description' => 'Полное описание',
	'published'   => 'Статус',
	'position'    => 'Позиция в списке',
];