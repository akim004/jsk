<?php

namespace app\modules\blockText;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    use \app\modules\blockText\ModuleTrait;

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/blockText/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@app/modules/blockText/messages',
            'fileMap' => [
                'modules/blockText/module' => 'module.php',
            ],
        ];

        $app->urlManager->registerModuleRules($this->getModule());
    }
}