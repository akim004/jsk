<?php

namespace app\modules\main\controllers;

use Yii;

class DefaultController extends \app\components\FrontendController
{
	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '@yiister/gentelella/views/error',
            ],
        ];
    }

    public function actionIndex()
    {
        /*$user = \app\modules\user\models\common\User::find()->one();
        Yii::$app->getModule('user')->mailer->sendConfirmEmailMessage($user);
                echo '<pre>';
                print_r($user);
                exit();*/

        return $this->render('index');
    }
}
