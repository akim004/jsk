<?php

use app\widgets\AlertMessage;
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;


/* @var $this \yii\web\View */
/* @var $content string */

\app\modules\project\ModuleAsset::register($this);
AppAsset::register($this);
\app\assets\AppAdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=\Yii::$app->getModule('main')->getConfig('metatags')?>
	<title><?= Html::encode($this->context->title) ?></title>
	<?=$this->registerMetaTag(['name' => 'description', 'content' => $this->context->description]);?>
	<?=$this->registerMetaTag(['name' => 'keywords', 'content' => $this->context->keywords]);?>
	<?= Html::csrfMetaTags() ?>
	<?php $this->head() ?>
</head>
<body>

<?= AlertMessage::widget(['message' => \Yii::$app->session->getFlash('success')]);?>


<div id="modal">
	<div id="modalContent">
		Текст
	</div>
</div>

<?php $this->beginBody() ?>
	<div class="wrap">
		<?php
			NavBar::begin([
				'brandLabel' => 'My Company',
				'brandUrl' => Yii::$app->homeUrl,
				'options' => [
					'class' => 'navbar-inverse navbar-fixed-top',
				],
			]);
			echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-right'],
				'items' => array_filter([
					['label' => Yii::t('app', 'NAV_HOME'), 'url' => ['/main/default/index']],
					['label' => 'Комплекс', 'url' => ['/project/default/index']],
					['label' => 'Контакты', 'url' => ['/contact/default/index']],
					Yii::$app->user->isGuest ?
						['label' => 'Регистрация', 'url' => ['/user/default/signup']] :
						false,
					Yii::$app->user->isGuest ?
						['label' => 'Вход', 'url' => ['/user/default/login']] : false,
					!Yii::$app->user->isGuest ?
						['label' => 'Профиль', 'url' => ['/user/default/index']] :	false,
					!Yii::$app->user->isGuest ?
						['label' => 'Админка', 'url' => ['/admin/default/index']] :	false,
					!Yii::$app->user->isGuest ?
						['label' => 'Выход (' . Yii::$app->user->identity->username . ')',
							'url' => ['/user/default/logout'],
							'linkOptions' => ['data-method' => 'post']] :
						false,
				]),
			]);
			NavBar::end();
		?>

		<div class="container" id="main-content">
			<?= Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<?= Alert::widget() ?>
			<?= $content ?>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="pull-left">&copy; My Company <?= date('Y') ?></p>
			<p class="pull-right"><?= Yii::powered() ?></p>
		</div>
	</footer>

<?php $this->endBody() ?>
<?=\Yii::$app->getModule('main')->getConfig('analytica')?>
</body>
</html>
<?php $this->endPage() ?>
