<?php

use app\assets\AppAsset;
use app\assets\LoaderAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
LoaderAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<link href="/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<?=\Yii::$app->getModule('main')->getConfig('metatags')?>
	<title><?= Html::encode($this->context->title) ?></title>
	<?=$this->registerMetaTag(['name' => 'description', 'content' => $this->context->description]);?>
	<?=$this->registerMetaTag(['name' => 'keywords', 'content' => $this->context->keywords]);?>
	<?= Html::csrfMetaTags() ?>

	<link rel="apple-touch-icon" sizes="57x57" href="/img/apple-touch-icons/57.png" />
	<link rel="apple-touch-icon" sizes="60x60" href="/img/apple-touch-icons/60.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/img/apple-touch-icons/72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="/img/apple-touch-icons/76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/img/apple-touch-icons/114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="/img/apple-touch-icons/120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="/img/apple-touch-icons/144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="/img/apple-touch-icons/152.png" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=cyrillic" rel="stylesheet">

	<?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="viewport-wrapper">

<div class="site-header">

	<div class="top">
		<div class="wrapper">

			<div class="logo">
				<a href="/"><img src="/img/logo.png" alt=""/></a>
			</div>

			<div class="menu">
				<?= \app\modules\navigation\widgets\NavigationView::widget(['type' => 'top']) ?>
			</div>

			<div class="r">

				<div class="phone">
					<a href="tel://<?=Yii::$app->getModule('main')->getConfig('phone')?>"><?=preg_replace('/(^\+{0,1}\d{1}\s*\({0,1}\d{3}\){0,1})()/', '<span> ${1} </span>', \Yii::$app->getModule('main')->getConfig('phone'))?></a>
				</div>

				<div class="login">
					<?if(Yii::$app->user->isGuest){?>
						<a href="" class="open-login">войти</a>
					<?}else{?>
						<a href="" class=""><?=Yii::$app->user->identity->id?></a>
					<?}?>
				</div>

				<div class="cart">
					<a href="">корзина</a>
				</div>

			</div>

		</div>
	</div>

	<div class="catalog wrapper">
		<?= \app\modules\catalog\widgets\CatalogView::widget() ?>
	</div>

</div><!-- .site-header -->

<?=$content?>

<div class="site-footer">
	<div class="wrapper">

		<div class="l">

			<div class="logo">
				<a href=""><img src="img/logo-footer.png" alt=""/></a>
			</div>

			<div class="copy">
				<?=date('Y')?> &copy;
				<?=\Yii::$app->getModule('main')->getConfig('name')?>
			</div>

			<div class="counter">
				<?=\Yii::$app->getModule('main')->getConfig('analytica')?>
			</div>

		</div>

		<div class="menu">

			<ul>
				<li><a href="">О компании</a></li>
				<li><a href="">Новости</a></li>
				<li><a href="">акции</a></li>
				<li><a href="">вакансии</a></li>
				<li><a href="">фотогалереяv</a></li>
				<li><a href="">Контакты</a></li>
			</ul>

			<ul>
				<li><a href="">Услуги</a></li>
				<li><a href="">доставка и оплата</a></li>
				<li><a href="">дилерам</a></li>
				<li><a href="">вопрос-ответ</a></li>
			</ul>

			<ul>
				<li><a href="">корзина</a></li>
				<li><a href="">личный кабинет</a></li>
			</ul>

		</div>

		<div class="phone">

			<div class="title">
				Контактный телефон
			</div>

			<div class="box">
				<a href="tel://<?=Yii::$app->getModule('main')->getConfig('phone')?>"><?=preg_replace('/(^\+{0,1}\d{1}\s*\({0,1}\d{3}\){0,1})()/', '<span> ${1} </span>', \Yii::$app->getModule('main')->getConfig('phone'))?></a>
			</div>

		</div>

		<div class="dekart">
			Сделано в <a href="http://www.dekartmedia.ru/" target=_blank>Декарт медиа</a>
		</div>

	</div>
</div><!-- .site-footer -->

</div><!-- .viewport-wrapper -->

<div class="dialog-question">
	<form action="#">

		<div class="item">
			<div class="level">Имя*</div>
			<div class="value">
				<input type="text" name=""/>
			</div>
		</div>

		<div class="item">
			<div class="level">Вопрос*</div>
			<div class="value">
				<textarea name=""></textarea>
			</div>
		</div>

		<div class="bt">

			<div class="text">
				<span>*</span> - поля обязательные для заполнения
			</div>

			<button type="submit">задать вопрос</button>

		</div>

	</form>
</div><!-- .dialog-question -->

<?if(Yii::$app->user->isGuest){?>
	<div class="dialog-login">
		<?= \app\modules\user\widgets\Login::widget();?>
	</div><!-- .dialog-login -->

	<div class="dialog-register">
		<?= \app\modules\user\widgets\Signup::widget();?>
	</div><!-- .dialog-register -->
<?}?>
<div class="dialog-checkout">
	<form action="#">

		<div class="l">

			<div class="item">
				<div class="level">ФИО*</div>
				<div class="value">
					<input type="text" name=""/>
				</div>
			</div>

			<div class="item">
				<div class="level">Телефон*</div>
				<div class="value">
					<input type="text" name=""/>
				</div>
			</div>

			<div class="item">
				<div class="level">E-mail</div>
				<div class="value">
					<input type="text" name=""/>
				</div>
			</div>

		</div>

		<div class="r">

			<div class="item">
				<div class="level">Адрес доставки*</div>
				<div class="value">
					<textarea name=""></textarea>
				</div>
			</div>

		</div>

		<div class="clear_fix"></div>

		<div class="bt">

			<div class="text">
				<span>*</span> - поля обязательные для заполнения
			</div>

			<button type="submit">подтвердить</button>

		</div>

	</form>
</div><!-- .dialog-checkout -->

<div class="dialog-thank">
	<div class="text"></div>
	<div class="bt">
		<a href="" class="button">продолжить</a>
	</div>
</div>

<?php $this->endBody() ?>
<?=\Yii::$app->getModule('main')->getConfig('analytica')?>
</body>
</html>
<?php $this->endPage() ?>