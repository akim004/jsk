<?php

if(Yii::$app->getModule('main')->getConfig('seoTitle')){
	$this->context->title = Yii::$app->getModule('main')->getConfig('seoTitle');
}
if(Yii::$app->getModule('main')->getConfig('seoDescription')){
	$this->context->description = Yii::$app->getModule('main')->getConfig('seoDescription');
}
if(Yii::$app->getModule('main')->getConfig('seoKeywords')){
	$this->context->keywords = Yii::$app->getModule('main')->getConfig('seoKeywords');
}

?>
<div class="main-default-index">
	<?= \app\modules\navigation\widgets\NavigationView::widget() ?>

	<?= \app\modules\slider\widgets\SliderView::widget() ?>

	<?= \app\modules\blockText\widgets\BlockTextView::widget(['param' => 'main']) ?>

	<?= \app\modules\user\widgets\Login::widget();?>

	<?= \app\modules\user\widgets\Signup::widget();?>

	<div class="jumbotron">
		<?php $widget = \app\widgets\ContentTools::begin(['language' => 'ru', 'tag' => 'div']); ?>
			<p>
			<?=($block = \app\modules\blockText\models\common\BlockText::find()->where(['param' => $widget->id, 'module' => 'main'])->active()->one()) ? $block->description : '';?>
			</p>
		<?php \app\widgets\ContentTools::end(); ?>
		<h1>Congratulations!</h1>

		<p class="lead">You have successfully created your Yii-powered application.</p>

		<p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
	</div>

	<div class="body-content">

		<div class="row">
			<div class="col-lg-4">
				<h2>Heading</h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
					ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
					fugiat nulla pariatur.</p>

				<p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
			</div>
			<div class="col-lg-4">
				<h2>Heading</h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
					ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
					fugiat nulla pariatur.</p>

				<p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
			</div>
			<div class="col-lg-4">
				<h2>Heading</h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
					ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
					fugiat nulla pariatur.</p>

				<p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
			</div>
		</div>

	</div>
</div>