<?php

if(Yii::$app->getModule('main')->getConfig('seoTitle')){
	$this->context->title = Yii::$app->getModule('main')->getConfig('seoTitle');
}
if(Yii::$app->getModule('main')->getConfig('seoDescription')){
	$this->context->description = Yii::$app->getModule('main')->getConfig('seoDescription');
}
if(Yii::$app->getModule('main')->getConfig('seoKeywords')){
	$this->context->keywords = Yii::$app->getModule('main')->getConfig('seoKeywords');
}

?>

	<div class="slider-promo wrapper">
		<?= \app\modules\slider\widgets\SliderView::widget() ?>
	</div>

	<div class="product-coll wrapper">

	<div class="subtitle">
		Коллекции фасадов
	</div>

	<ul>

		<li class="type-1">
			<a href="">
				<span class="img">
					<img src="uploads/prod-coll-1.jpg" alt=""/>
				</span>
				<span class="entry">
					<span class="title">
						Прованс
					</span>
					<span class="in">
						<span>в каталог</span>
					</span>
				</span>
			</a>
		</li>

		<li class="type-2">
			<a href="">
				<span class="img">
					<img src="uploads/prod-coll-2.jpg" alt=""/>
				</span>
				<span class="entry">
					<span class="title">
						Модерн
					</span>
					<span class="in">
						<span>в каталог</span>
					</span>
				</span>
			</a>
		</li>

		<li class="type-3">
			<a href="">
				<span class="img">
					<img src="uploads/prod-coll-3.jpg" alt=""/>
				</span>
				<span class="entry">
					<span class="title">
						Классика
					</span>
					<span class="in">
						<span>в каталог</span>
					</span>
				</span>
			</a>
		</li>

	</ul>

	</div><!-- .product-coll -->

	<div class="inform-promo">
	<div class="wrapper">

		<div class="subtitle">
			Преимущества
		</div>

		<ul>

			<li>
				<div class="img">
					<img src="img/i-promo-1.png" alt=""/>
				</div>
				<div class="title">
					Только <span>прямые поставки</span>
				</div>

			<li>
				<div class="img">
					<img src="img/i-promo-2.png" alt=""/>
				</div>
				<div class="title">
					<span>Эксклюзивные фасады</span> от ITALICA
				</div>

			<li>
				<div class="img">
					<img src="img/i-promo-3.png" alt=""/>
				</div>
				<div class="title">
					<span>Зарекомендованный</span> продавец на рынке
				</div>

			<li>
				<div class="img">
					<img src="img/i-promo-4.png" alt=""/>
				</div>
				<div class="title">
					Постоянные <span>скидки и акции</span>
				</div>

		</ul>

	</div>
	</div><!-- .inform-promo -->

	<div class="product-promo wrapper">
		<?= \app\modules\catalog\widgets\MainItems::widget([
			'topPageSize' => \Yii::$app->getModule('catalog')->getConfig('topPageSize'),
			'discountPageSize' => \Yii::$app->getModule('catalog')->getConfig('discountPageSize'),
		]) ?>
	</div><!-- .product-promo -->

	<?= \app\modules\news\widgets\NewsView::widget() ?>

	<div class="inform-about wrapper">

	<h2 class="subtitle">О нас</h2>

	<div class="intro">
		Итальянские производители умело сочетают вековые традиции деревообработки с современными технологиями. Готовые изделия имеют не только неповторимый дизайн, но и отличаются практичностью и износоустойчивостью. В продаже мебельные фасады представлены весьма разнообразно и широко. В наличии компании «МакБерри» следующие коллекции:
	</div>

	<div class="line"></div>

	<div class="box">

		<div class="item">
			<p><strong class="blue">Classic</strong> – это элегантность и изящество, благородная роскошь и вековые традиции,стиль уверенных и респектабельных людей.</p>
			<p>Коллекция фасадов CLASSIC является истинным воплощением классического стиля в мебельном производстве: гармоничные геометрические формы, плавные линии, изысканные декоративные элементы (узоры, накладки, витражи), натуральное дерево – отличительные особенности, которые всегда остаются востребованными и актуальными.</p>
			<p>Цветовая гамма фасадов этой коллекции весьма разнообразна: от естественных оттенков дерева до ярких и насыщенных тонов.</p>
		</div>

		<div class="item">
			<p><strong class="blue">Modern</strong> – это новаторство и современные тенденции, динамика и актуальность, стиль активных и свободных людей.</p>
			<p>Коллекция MODERN является четким отражением модернового стиля в мебельном декоре: лаконичные и минималистичные орнаменты, прямые и элегантные линии, природный материал – отличительные черты, столь свойственные дизайну современного дома. В цветовой гамме этой коллекции преобладают мягкие и глубокие оттенки, максимально приближенные к натуральным, такие, как белый, фисташковый, синий, древесный и т.п.</p>
		</div>

		<div class="item">
			<p><strong class="blue">Modern</strong> – это новаторство и современные тенденции, динамика и актуальность, стиль активных и свободных людей.</p>
			<p>Коллекция MODERN является четким отражением модернового стиля в мебельном декоре: лаконичные и минималистичные орнаменты, прямые и элегантные линии, природный материал – отличительные черты, столь свойственные дизайну современного дома. В цветовой гамме этой коллекции преобладают мягкие и глубокие оттенки, максимально приближенные к натуральным, такие, как белый, фисташковый, синий, древесный и т.п.</p>
		</div>

	</div>

	</div><!-- .inform-about -->