<?php

/**
 * composer require 2amigos/yii2-ckeditor-widget:~1.0
 */

namespace app\modules\main;

class Module extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\main\controllers';

    /**
     * Название сайта
     */
    private $name = 'Название приложения';


    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/main/' . $category, $message, $params, $language);
	}

    /**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [
			// 'contact' => 'main/contact/index',
			'<_action:error>' => 'main/default/<_action>'
		];
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		return $this->name = $name;
	}

}
