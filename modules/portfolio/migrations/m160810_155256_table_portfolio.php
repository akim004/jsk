<?php

use yii\db\Migration;
use yii\db\Schema;

class m160810_155256_table_portfolio extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%portfolio}}', [
			'id'          => Schema::TYPE_PK,
			'created_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'description' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
			'position'    => Schema::TYPE_INTEGER . '(2) NULL DEFAULT NULL DEFAULT 0',
			'slug'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'published'   => Schema::TYPE_SMALLINT . '(2) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createIndex('idx-portfolio-name', '{{%portfolio}}', 'name');
		$this->createIndex('idx-portfolio-slug', '{{%portfolio}}', 'slug');
	}

	public function down()
	{
		$this->dropTable('{{%portfolio}}');
	}
}
