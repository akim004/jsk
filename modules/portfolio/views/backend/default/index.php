<?php

use app\modules\portfolio\Module;
use app\modules\portfolio\models\backend\Portfolio;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Portfolio\models\PortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('module', 'Portfolios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('module', 'Create Portfolio'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'class'     => \app\components\EditableColumn::className(),
                'type'      => 'select',
                'url'       => ['editable'],
                'attribute' => 'published',
                'format' => 'raw',
                'filter'    => Portfolio::getStatusesArray(),
                'value' => function($data) {
                    return Portfolio::getStatusesArray()[$data->published];
                },
                'editableOptions' => [
                    'mode' => 'pop', // 'inline'
                    'source' => json_encode(Portfolio::getStatusesArrayForSource()),
                    'placement' => 'bottom',
                ],
            ],
            // 'created_at',
            // 'updated_at',
            // 'description:ntext',
            // 'date',
            // 'position',
            // 'slug',
            // 'published',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
            ],
        ],
    ]); ?>

</div>
