<?php

use app\modules\portfolio\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\portfolio\models\Portfolio */

$this->title = Module::t('module', 'Create Portfolio');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Portfolio'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
