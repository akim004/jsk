<?php

use app\modules\banner\Module;
use app\modules\banner\models\common\Banner;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="banner-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
		<li><a href="#settings">SEO</a></li>
		<?if(!$model->isNewRecord){?>
			<li><a href="#images">Загруженные фотографии</a></li>
		<?}?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'published')->dropDownList(Banner::getStatusesArray()) ?>

			<div >
				<label> <?= $model->attributeLabels()['imageUpload']?></label>
				<?
					echo \kartik\file\FileInput::widget([
						'id' => 'images-upload',
					    'name' => 'Portfolio[imageUpload][]',
					    'language' => 'ru',
					    'options' => ['multiple' => true],

					    'pluginOptions' => [
					    	'uploadExtraData' => [
					            'modelId' => $model->id,
					        ],
					    	'previewFileType' => 'any',
					        'maxImageWidth' => $model->maxImageWidth,
							'maxImageHeight' => $model->maxImageHeight,
							'initialCaption'   => "Загрузите фотографии",
							'overwriteInitial' => true,
							'showUpload'       => true,
							'allowedFileExtensions' => ['jpg','gif','png'],
					    	'uploadUrl' => $model->isNewRecord ? '' : \yii\helpers\Url::to(['file-upload']),
					    ],
					]);
					?>
			</div>
		</div>
		<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>
		<div class="tab-pane x_content" id="images">
			<?
			    $query = \rico\yii2images\models\Image::find();
			    $query->andFilterWhere([
			        'itemId' => $model->primaryKey,
			        'modelName' => Yii::$app->getModule('yii2images')->getShortClass($model)
			    ]);
			    $dataProvider = new \yii\data\ActiveDataProvider([
			                            'query' => $query,
			                        ]);
		    ?>
		    <?php \yii\widgets\Pjax::begin([
		    			'id' => 'files-container',
		    			'timeout' => 10000,
		    ]); ?>
			    <?= \yii\grid\GridView::widget([
			        'dataProvider' => $dataProvider,
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],
			            'id',
			            [
			                // 'attribute' => 'img',
			                'format' => 'html',
			                'label' => 'Фотография',
			                'value' => function (\rico\yii2images\models\Image $data) {
			                    return Html::img($data->getUrl('100x'));
			                },
			            ],
			            [
			                'class' => \dosamigos\grid\EditableColumn::className(),
			                'header' => 'Подписи к картинкам',
			                'attribute' => 'name',
			                'url' => ['editableiamge'],
			                'type' => 'text',
			                'editableOptions' => [  ]
			            ],
			            [
			                'format' => 'raw',
			                'label' => '',
			                'value' => function ($data) {
			                        return Html::a('Удалить', \yii\helpers\Url::to(['delete-image', 'id' => $data->id]),
			                        	[
			                        		'class' => 'pjax-link',
			                        		'data-pjax' => 0,
			                        		'data-pjax-id' => 'files-container',
			                        		'data-confirm-text' => 'Вы уверены',
			                        	]);
			                    },
			            ],
			        ],
			    ]); ?>
			<?php \yii\widgets\Pjax::end(); ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Module::t('module', 'apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::a(Module::t('module', 'cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>


<?
$script = <<< JS
	$(function(){
		$('#images-upload').on('fileuploaded', function(event, data, previewId, index) {
		    /*var form = data.form, files = data.files, extra = data.extra,
		        response = data.response, reader = data.reader;*/
		    $.pjax.reload({container:'#files-container',timeout: 10000,});
		});
	});
JS;

 $this->registerJs($script, yii\web\View::POS_READY);?>

</div>
