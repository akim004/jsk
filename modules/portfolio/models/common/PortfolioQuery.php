<?php

namespace app\modules\portfolio\models\common;

use app\modules\portfolio\models\common\Portfolio;

class PortfolioQuery extends \yii\db\ActiveQuery
{

    public function active()
	{
		return $this->andWhere(['published' => Portfolio::ACTIVE]);
	}
}