<?php

namespace app\modules\portfolio\models\common;

use Yii;
use app\modules\portfolio\Module;
use app\modules\portfolio\models\common\PortfolioQuery;

class Portfolio extends \yii\db\ActiveRecord
{
		/**
	 * Загружаемая картинка
	 * @var UploadedFile
	 */
	public $imageUpload;

	const ACTIVE = 0;
	const NOT_ACTIVE = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%portfolio}}';
	}

	public static function find()
	{
		$portfolioQuery = new PortfolioQuery(get_called_class());

		return $portfolioQuery->orderBy(['position' => SORT_ASC]);
	}

	public function behaviors()
	{
		return [
			['class' => \app\behaviors\SeoBehavior::className()],
			'image' => [
				'class' => '\app\behaviors\ImageBehavior',
				'enableBehaviorValidate' => true,
				'uploadAttribute' => 'imageUpload',
				'maxFiles' => 20,
				/*'maxImageWidth' => '1000',
				'maxImageHeight' => '1000',
				'maxSize' => '5',*/
			],
		];


	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['description'], 'string'],
			[['name', 'slug'], 'string', 'max' => 255],
			[['position','published'], 'safe'],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'id'),
			'created_at'  => Module::t('module', 'created_at'),
			'updated_at'  => Module::t('module', 'updated_at'),
			'id'          => Module::t('module', 'id'),
			'name'        => Module::t('module', 'name'),
			'description' => Module::t('module', 'description'),
			'slug'        => Module::t('module', 'slug'),
			'published'   => Module::t('module', 'published'),
			'position'    => Module::t('module', 'position'),
			'imageUpload' => Module::t('module', 'imageUpload'),
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE => 'Активный',
			self::NOT_ACTIVE  => 'Скрытый',
		];
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::ACTIVE,
				'text' => 'Активный',
			],
			[
				'value' => self::NOT_ACTIVE,
				'text' => 'Скрытый',
			],
		];
	}

	public function afterDelete() {
		parent::afterDelete();
		$this->deleteImages();
	}

	protected function deleteImages()
	{
		if($this->getImages()){
			foreach ($this->getImages() as $image) {
				$this->removeImage($image);
			}
		}
	}

	public function saveImages($files)
	{
		if($files){
			foreach ($files as $file) {
				$file->saveAs('uploads/runtime/'.$file->name);
				$this->attachImage('uploads/runtime/'.$file->name);
				@unlink('uploads/runtime/'.$file->name);
			}
		}
	}
}
