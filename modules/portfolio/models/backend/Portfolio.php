<?php

namespace app\modules\portfolio\models\backend;

use Yii;
use app\modules\portfolio\Module;
use yii\helpers\ArrayHelper;

class Portfolio extends \app\modules\portfolio\models\common\Portfolio
{
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			['class' => \yii\behaviors\TimestampBehavior::className()],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [

		]);
	}


}
