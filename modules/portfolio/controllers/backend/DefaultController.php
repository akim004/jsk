<?php

namespace app\modules\portfolio\controllers\backend;

use Yii;
use app\modules\portfolio\Module;
use app\modules\portfolio\models\backend\Portfolio;
use app\modules\portfolio\models\backend\PortfolioSearch;
use dosamigos\editable\EditableAction;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * DefaultController implements the CRUD actions for Portfolio model.
 */
class DefaultController extends \app\components\BackendController
{

	public function actions()
	{
		return [
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridAction::className(),
				'modelName' => Portfolio::className(),
			],
			'editable' => [
				'class' => EditableAction::className(),
				'modelClass' => Portfolio::className(),
				'forceCreate' => false
			],
			'editableiamge' => [
                'class' => \dosamigos\editable\EditableAction::className(),
                'modelClass' => \rico\yii2images\models\Image::className(),
                'forceCreate' => false
            ],
		];
	}

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Portfolio models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PortfolioSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Portfolio model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Portfolio model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Portfolio();

		if ($model->load(Yii::$app->request->post())) {
			$model->imageUpload = UploadedFile::getInstances($model, 'imageUpload');
			if($model->save()){
				$model->saveImages($model->imageUpload);

				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully added'));

				if(!empty($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}

				return $this->redirect(['index']);

			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	//Загрузка изображении аяксом
	public function actionFileUpload()
	{
		$modelId = Yii::$app->request->post('modelId');
		$model = $this->findModel($modelId);
		if($model){
			$model->imageUpload = UploadedFile::getInstances($model, 'imageUpload');
			$model->saveImages($model->imageUpload);
		}

		echo '{}';
		exit();
	}

	/**
	 * Updates an existing Portfolio model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
			$model->imageUpload = UploadedFile::getInstances($model, 'imageUpload');
			if ($model->save()) {
				$model->saveImages($model->imageUpload);

				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully changed'));

				if(!empty($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Portfolio model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionDeleteImage($id)
    {
        $img = \rico\yii2images\models\Image::findOne($id);
        $model = $this->findModel($img->itemId);
        $model->removeImage($img);

        /*return $this->render('update', [
			'model' => $model,
		]);*/
		die();
        // return $this->redirect(['update', 'id' => $model->id]);
    }

	/**
	 * Finds the Portfolio model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Portfolio the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Portfolio::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
