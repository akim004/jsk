<?php

namespace app\modules\portfolio\controllers\frontend;

use app\modules\portfolio\models\frontend\Portfolio;
use yii\web\Controller;

class DefaultController extends \app\components\FrontendController
{
    public function actionIndex()
    {
    	$portfolios = Portfolio::find()->active()->all();

        return $this->render('index', [
        		'portfolios' => $portfolios,
        	]);
    }
}
