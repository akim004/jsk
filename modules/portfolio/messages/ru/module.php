<?php

return [
	'Portfolio'               => 'Портфолио',
	'Portfolios'               => 'Портфолио',
	'Create Portfolio'         => 'Создать портфолио',
	'Create'                => 'Создать',
	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать портфолио: ',

	'apply' => 'Применить',
	'cancel' => 'Отмена',

	'successfully added' => 'Портфолио успешно добавлен',
	'successfully changed' => 'Портфолио успешно изменен',

	'id'          => 'ID',
	'created_at'  => 'Создан',
	'updated_at'  => 'Обновлен',
	'id'          => 'ID',
	'name'        => 'Заголовок',
	'short'       => 'Короткое описание',
	'description' => 'Полное описание',
	'slug'        => 'Алиас',
	'published'   => 'Статус',
	'position'    => 'Позиция в списке',
	'imageUpload' => 'Фотографии',
];