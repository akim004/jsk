<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/portfolio/migrations --interactive=0
 *
 * \himiklab\sortablegrid\SortableGridBehavior
 */

namespace app\modules\portfolio;

use Yii;

class Module extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\portfolio\controllers';

    public function init()
	{
		parent::init();
		// custom initialization code goes here
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/portfolio/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [];
	}
}
