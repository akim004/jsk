<?php

namespace app\modules\portfolio;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    use \app\modules\portfolio\ModuleTrait;

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/portfolio/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@app/modules/portfolio/messages',
            'fileMap' => [
                'modules/portfolio/module' => 'module.php',
            ],
        ];

        $app->urlManager->registerModuleRules($this->getModule());
    }
}