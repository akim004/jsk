<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/page/migrations --interactive=0
 *
 * "kartik-v/yii2-widget-datepicker": "@dev",
 * \himiklab\sortablegrid\SortableGridBehavior
 */

namespace app\modules\page;

use Yii;

class Module extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\page\controllers';

    public function init()
	{
		parent::init();
		// custom initialization code goes here
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/page/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		$rules = [];
		$pathInfo = \Yii::$app->request->pathinfo;
		if($page = \app\modules\page\models\common\Page::find()->where(['slug' => '/'.$pathInfo])->active()->one()){
			$rule = [
				'pattern'  => $pathInfo,
				'route'    => 'page/default/index',
				'defaults' => [
					'slug' => $pathInfo,
				],
			];
			$rules[] = $rule;
		}

		$rules[] = [
			'pattern'  => 'page/<slug>',
			'route' => 'page/default/index'
		];

		return $rules;
	}
}
