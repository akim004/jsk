<?php

namespace app\modules\page\models\backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\page\models\backend\Page;

/**
 * pageSearch represents the model behind the search form about `app\modules\page\models\Page`.
 */
class PageSearch extends Page
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'created_at', 'updated_at', 'position', 'published'], 'integer'],
			[['name', 'description', 'slug'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Page::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> [
				'defaultOrder' => ['position' => SORT_ASC],
			],
			'pagination' => [
				'pageSize' => 10,
			],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id'         => $this->id,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'position'   => $this->position,
			'published'  => $this->published,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'description', $this->description])
			->andFilterWhere(['like', 'slug', $this->slug]);

		return $dataProvider;
	}
}
