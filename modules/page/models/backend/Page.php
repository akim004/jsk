<?php

namespace app\modules\page\models\backend;

use Yii;
use app\modules\page\Module;
use yii\helpers\ArrayHelper;

class Page extends \app\modules\page\models\common\Page
{

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			[
				'class' => 'Zelenin\yii\behaviors\Slug',
				'attribute' => 'name',
				'slugAttribute' => 'slug',
				'ensureUnique' => true, // уникальность в базе
				'replacement' => '-',
				'lowercase' => true,
				'immutable' => true,
				// 'translit' => true,
				// If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
				'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
			],
			['class' => \yii\behaviors\TimestampBehavior::className()],
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridBehavior::className(),
				'sortableAttribute' => 'position'
			],
		]);
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['slug'], 'filter', 'filter' => function ($value) {
				if($value[0] != "/" )
						$value = "/".$value;
				return $value;
			}],
		]);
	}

	public static function getStatusesArrayForSource()
	{
		return [
			[
				'value' => self::ACTIVE,
				'text' => Module::t('module', 'Active'),
			],
			[
				'value' => self::NOT_ACTIVE,
				'text' => Module::t('module', 'Not active'),
			],
		];
	}
}
