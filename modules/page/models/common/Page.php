<?php

namespace app\modules\page\models\common;

use Yii;
use app\modules\page\Module;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property integer $
 * @property integer $updated_at
 * @property string $name
 * @property string $short
 * @property string $description
 * @property integer $date
 * @property integer $position
 * @property string $slug
 * @property integer $published
 */
class Page extends \yii\db\ActiveRecord
{

	protected $navigation;

	public $navigationId;

	const ACTIVE = 0;
	const NOT_ACTIVE = 1;

	/*public static function instantiate($row)
    {
        return \Yii::$container->get(static::class);
    }*/

	/*public function __construct($config = [])
    {
        $this->navigation = \Yii::createObject(\app\modules\navigation\models\common\Navigation::className());
        parent::__construct($config);
    }*/

    public static function find()
	{
		$pageQuery = new PageQuery(get_called_class());

		return $pageQuery->orderBy(['position' => SORT_ASC]);
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%page}}';
	}

	public function behaviors()
	{
		return [
			['class' => \app\behaviors\SeoBehavior::className()],
			['class' => \app\behaviors\RouteBehavior::className()],
		];


	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['slug', 'trim'],
			[['name'], 'required'],
			[['description'], 'string'],
			[['name', 'slug'], 'string', 'max' => 255],
			[['position', 'published'], 'safe'],
		];
	}


	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'Id'),
			'created_at'  => Module::t('module', 'Created_at'),
			'updated_at'  => Module::t('module', 'Updated_at'),
			'id'          => Module::t('module', 'Id'),
			'name'        => Module::t('module', 'Name'),
			'description' => Module::t('module', 'Description'),
			'slug'        => Module::t('module', 'Slug'),
			'published'   => Module::t('module', 'Published'),
			'position'    => Module::t('module', 'Position'),
		];
	}

	public static function getStatusesArray()
	{
		return [
			self::ACTIVE => Module::t('module', 'Active'),
			self::NOT_ACTIVE  => Module::t('module', 'Not active'),
		];
	}

	public function afterDelete() {
		parent::afterDelete();
		//some
	}

	public function getCreateUrl()
	{
		return \yii\helpers\Url::to(['/page/default/index', 'slug' => trim($this->slug, '/')]);
	}
}
