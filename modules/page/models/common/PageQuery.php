<?php

namespace app\modules\page\models\common;

use \app\modules\page\models\common\Page;

class PageQuery extends \yii\db\ActiveQuery
{
    public function active()
	{
		return $this->andWhere(['published' => Page::ACTIVE]);
	}
}