<?php

return [
	'Page'                  => 'Страница',
	'Create Page'           => 'Создать страницу',
	'Create'                => 'Создать',
	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать страницу: ',

	'Apply'  => 'Сохранить',
	'Cancel' => 'Отменить',
	'Update and close' => 'Сохранить и выйти',

	'successfully added' => 'Страница успешно добавлена',
	'successfully changed' => 'Страница успешно изменена',

	'Id'          => 'ID',
	'Created_at'  => 'Создан',
	'Updated_at'  => 'Обновлен',
	'Name'        => 'Заголовок',
	'Description' => 'Полное описание',
	'Slug'        => 'Алиас',
	'Published'   => 'Статус',
	'Position'    => 'Позиция в списке',

	'Active'    => 'Активный',
	'Not active'    => 'Скрытый',
];