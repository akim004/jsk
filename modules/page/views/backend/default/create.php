<?php

use app\modules\page\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\page\models\Page */

$this->title = Module::t('module', 'Create Page');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Page'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
