<?php

use app\modules\page\Module;
use app\modules\page\models\backend\page;
use mihaildev\ckeditor\CKEditor;
use kartik\file\FileInput;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="page-form">

	<?php $form = ActiveForm::begin([
	]); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
		<li><a href="#settings">SEO</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'description')->widget(\app\widgets\CKEditor::className(), [
				'options' => ['rows' => 6],
				'preset' => 'advanced',
				'clientOptions' => [
		            'contentsCss' => '/css/static.css',
		        ]
			]) ?>

			<?/*php echo froala\froalaeditor\FroalaEditorWidget::widget([
			    'model' => $model,
			    'attribute' => 'description',
			    'options'=>[// html attributes
			        // 'id'=>'content'
			    ],
			]);*/ ?>

			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'published')->dropDownList(Page::getStatusesArray()) ?>

			<?//= \app\widgets\BindNavigationForm::widget(['model' => $model]) ?>
		</div>
		<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\RouteForm::widget(['model' => $model]) ?>
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton(Module::t('module', 'Apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update and close'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
		<?= Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
