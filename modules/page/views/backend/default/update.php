<?php

use app\modules\page\Module;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Page\models\Page */

$this->title = Module::t('module', 'Update {modelClass}: ', [
    'modelClass' => 'Page',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'Page'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('module', 'Update');
?>
<div class="Page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
