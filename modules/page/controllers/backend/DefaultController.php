<?php

namespace app\modules\page\controllers\backend;

use Yii;
use app\modules\page\Module;
use app\modules\page\models\backend\Page;
use app\modules\page\models\backend\PageSearch;
use dosamigos\editable\EditableAction;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * DefaultController implements the CRUD actions for Page model.
 */
class DefaultController extends \app\components\BackendController
{

	public function actions()
	{
		return [
			'sort' => [
				'class' => \himiklab\sortablegrid\SortableGridAction::className(),
				'modelName' => Page::className(),
			],
			'editable' => [
				'class' => EditableAction::className(),
				'modelClass' => Page::className(),
				'forceCreate' => false
			],
		];
	}

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Page models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = Yii::createObject(PageSearch::className());
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Page model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Page model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Page();

		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){

				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully added'));

				if(!empty($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}

				return $this->redirect(['index']);

			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Page model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
			if ($model->save()) {
				Yii::$app->getSession()->setFlash('success', Module::t('module', 'successfully changed'));

				if(!empty($_GET['wait'])){
					return $this->redirect(['update', 'id' => $model->id, 'wait' => 0]);
				}
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Page model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		if (!Yii::$app->request->isAjax) {
			return $this->redirect(['index']);
		}
	}

	/**
	 * Finds the Page model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Page the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Page::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
