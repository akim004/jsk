<?php

namespace app\modules\page\controllers\frontend;

use app\modules\page\models\frontend\Page;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends \app\components\FrontendController
{
    public function actionIndex($slug)
    {
        $model = $this->findModel($slug);

        return $this->render('index', ['model' => $model]);
    }

    public function actionTest()
    {
    }

    protected function findModel($slug)
	{
		if (($model = Page::find()->where(['slug' => '/'.$slug])->active()->one()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
