<?php

use yii\db\Schema;
use yii\db\Migration;

class m160721_053816_table_page extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%page}}', [
			'id' => Schema::TYPE_PK,
			'created_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'name'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'description' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
			'position'    => Schema::TYPE_INTEGER . '(2) NULL DEFAULT NULL DEFAULT 0',
			'slug'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'published'   => Schema::TYPE_SMALLINT . '(2) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createIndex('idx_page_name', '{{%page}}', 'name');
		$this->createIndex('idx_page_slug', '{{%page}}', 'slug');
	}

	public function down()
	{
		$this->dropTable('{{%page}}');
	}
}
