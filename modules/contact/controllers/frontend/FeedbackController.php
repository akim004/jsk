<?php

namespace app\modules\contact\controllers\frontend;

use Yii;
use app\modules\contact\models\common\FeedbackForm;
use yii\web\Controller;

class FeedbackController extends \app\components\FrontendController
{
	/*public function actions()
	{
		return [
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}*/

	 public function actionIndex()
	{
		$model = new FeedbackForm();

		if ($model->load(Yii::$app->request->post()) && $model->feedback(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');
			return $this->refresh();
		} else {
			return $this->render('index', [
				'model' => $model,
			]);
		}
	}

}
