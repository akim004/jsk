<?php

namespace app\modules\contact\controllers\frontend;

use yii\web\Controller;

class DefaultController extends \app\components\FrontendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
