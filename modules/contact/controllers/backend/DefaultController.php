<?php

namespace app\modules\contact\controllers\backend;

use Yii;
use app\components\BackendController;
use app\modules\contact\Module;
use app\modules\contact\models\common\Contact;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class DefaultController extends BackendController
{

	public function actionIndex(){

		$items = Contact::find()->indexBy('id')->all();

		if(isset($_POST['Contact'])){
			$valid=true;
			foreach($items as $i => $item){

				if(isset($_POST['Contact'][$i])){
					$item->value = $_POST['Contact'][$i]['value'];
				}

				$valid=$item->save() && $valid;

				Yii::$app->getSession()->setFlash('success', Module::t('module', 'Success'));
			}
		}

		foreach ($items as $key => $value) {
			$helper[$value->group][$value->id] = $value;
		}

		return $this->render('_form',[
					'items' => $helper
				]);
	}

	protected function getMap()
	{

	}
}
