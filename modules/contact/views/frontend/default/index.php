<div class="news-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <?=\Yii::$app->getModule('contact')->getPhone()?>
    <?=\Yii::$app->getModule('contact')->getAddress()?>
    <?=\Yii::$app->getModule('contact')->getEmail()?>

    <div class="header-social">
        <?$social = \Yii::$app->getModule('contact')->getSocial();?>
        <ul>
            <?foreach ($social as $soc => $value) {?>
                <?if($value && $soc == 'vk'){?>
                    <li><a href="<?=$value?>" target="_blank"><i class="fa fa-vk"></i>vk</a></li>
                <?}?>
                <?if($value && $soc == 'fb'){?>
                    <li><a href="<?=$value?>" target="_blank"><i class="fa fa-facebook">fb</i></a></li>
                <?}?>
                <?if($value && $soc == 'insta'){?>
                    <li><a href="<?=$value?>" target="_blank"><i class="fa fa-pinterest">insta</i></a></li>
                <?}?>
            <?}?>
        </ul>
    </div>

    <?= \app\modules\contact\widgets\Feedback::widget() ?>
</div>
