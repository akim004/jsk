<?php

use app\modules\config\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li ><a href="#home">Основные</a></li>
		<li class="active"><a href="#area">Карта</a></li>
		<li ><a href="#soc">Соц. сети</a></li>
	<?/*	<li><a href="#settings">SEO</a></li>*/?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane  x_content" id="home">
			<?php foreach($items['main'] as $i => $item):?>
				<?php if (!$item->type || $item->type == 'string'): ?>
					<tr>
						<td colspan="2"><h4><?=$item->label?></h4></td>
					</tr>
					<tr>
						<td><?= $form->field($item, "[$i]value")->textInput(['style' => 'width:95%;'])->label(false);?></td>
					</tr>
				<?php elseif ($item->type == 'text'): ?>
					<tr>
						<td colspan="2"><h4><?=$item->label?></h4></td>
					</tr>
					<tr>
						<td><?= $form->field($item, "[$i]value")->textArea(['style' => 'width:95%;height:160px;'])->label(false);?></td>
					</tr>
				<?php elseif ($item->type == 'checkbox'): ?>
					<tr>
						<td>
							<?= $form->field($item, "[$i]value")->checkBox() ?>
							<?= $form->field($item, "[$i]value")->label(['class' => 'label-sib-checkbox'])?>
						</td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<?/*<div class="tab-pane x_content" id="settings">
			<?= \app\widgets\SeoForm::widget(['model' => $model]) ?>
		</div>*/?>
		<div class="tab-pane x_content active" id="area">

			<?php foreach($items['map'] as $i => $item): ?>
				<?if($item->param == 'map'){ $coords = $item->value;}?>
				<?php if (!$item->type || $item->type == 'string'): ?>
					<tr>
						<td colspan="2"><h4><?=$item->label?></h4></td>
					</tr>
					<tr>
						<td><?= $form->field($item, "[$i]value")->textInput(['style' => 'width:95%;', 'id' => 'mapInput'])->label(false);?></td>
					</tr>
				<?php elseif ($item->type == 'text'): ?>
					<tr>
						<td colspan="2"><h4><?=$item->label?></h4></td>
					</tr>
					<tr>
						<td><?= $form->field($item, "[$i]value")->textArea(['style' => 'width:95%;height:160px;'])->label(false);?></td>
					</tr>
				<?php elseif ($item->type == 'checkbox'): ?>
					<tr>
						<td>
							<?= $form->field($item, "[$i]value")->checkBox() ?>
							<?= $form->field($item, "[$i]value")->label(['class' => 'label-sib-checkbox'])?>
						</td>
					</tr>
				<?php endif; ?>
			<?php endforeach;?>

			<?	$contact = explode(';', $coords ? $coords : '42.96144767344319;47.54161834716797');
				$markers = array(
								array(
									'id' => 'contact',
									'coords' => array($contact[0], $contact[1]),
								),
							);?>
			<?= \app\modules\contact\widgets\Map::widget([
					'isCreateMarkers' => true,
					'markers'         => $markers,
					'whereSave'       => '#mapInput',
			]) ?>
			<br>
		</div>
		<div class="tab-pane  x_content" id="soc">
			<?php foreach($items['soc'] as $i => $item):?>
				<?php if (!$item->type || $item->type == 'string'): ?>
					<tr>
						<td colspan="2"><h4><?=$item->label?></h4></td>
					</tr>
					<tr>
						<td><?= $form->field($item, "[$i]value")->textInput(['style' => 'width:95%;'])->label(false);?></td>
					</tr>
				<?php elseif ($item->type == 'text'): ?>
					<tr>
						<td colspan="2"><h4><?=$item->label?></h4></td>
					</tr>
					<tr>
						<td><?= $form->field($item, "[$i]value")->textArea(['style' => 'width:95%;height:160px;'])->label(false);?></td>
					</tr>
				<?php elseif ($item->type == 'checkbox'): ?>
					<tr>
						<td>
							<?= $form->field($item, "[$i]value")->checkBox() ?>
							<?= $form->field($item, "[$i]value")->label(['class' => 'label-sib-checkbox'])?>
						</td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>


	<div class="form-group">
	<hr>
		<?= Html::submitButton(Module::t('module', 'Save'), ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
