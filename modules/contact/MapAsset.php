<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\contact;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MapAsset extends AssetBundle
{
	public $sourcePath = '@app/modules/contact/assets/';
	public $css = [
		// 'css/module.css',
	];
	public $js = [
		// 'http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js',
		'js/map/markerclusterer.js',
		'js/map/map.js',
		'https://maps.googleapis.com/maps/api/js?key=AIzaSyCk1poK2npaR0cB-YhhxLTwlkJLipvyQJ0&callback=initMapp',
		// 'https://maps.googleapis.com/maps/api/js?callback=initMapp',
		// 'js/module.js',
		// AIzaSyCk1poK2npaR0cB-YhhxLTwlkJLipvyQJ0
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
