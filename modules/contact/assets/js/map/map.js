var initMap = {


	config : {
		whereSave : '',
		iconImage : '/img/markerImg.png',
		isCreateMarker : false,
		initMarkers: [],
		allMarkers: [],
		view: 'main',
		googleMap : {
			zoom : 13,
			lat: 42.97853015564473,
			lng: 47.500762939453125,
			mapID: 'map',
		}
	},

	init : function(config){
		$.extend(initMap.config, config);
		initMap.initMap(initMap.config.googleMap);
		initMap.setInitMarkers(initMap.config.initMarkers);
	},

	// инициализация карты
	initMap : function(mapConfig) {
		map = new google.maps.Map(document.getElementById(mapConfig.mapID), {
				center: {lat: mapConfig.lat, lng: mapConfig.lng},
				scrollwheel: true,
				zoom: mapConfig.zoom,
				disableDoubleClickZoom: true,
			    // mapTypeId: 'style'
			});


		if(initMap.config.isCreateMarker){
			map.addListener('click', function(event) {

				if(marker = initMap.config.allMarkers[1]){
					markerCluster.removeMarker(marker);
				}

				var coords = [
					event.latLng.lat(),
					event.latLng.lng(),
				];

				initMap.setCoordsInField(event.latLng.lat()+';'+event.latLng.lng(), initMap.config.whereSave);
				initMap.setMarkers([{id: 1,coords: coords}] ,map);
			});
		}
	},

	setInitMarkers: function(markers){
		initMap.setMarkers(markers, map);
	},

	setCoordsInField : function(coords, inputClass){
		$(inputClass).val(coords);
	},
	// создать маркер
	createMarker : function (coords, draggable, iconImage, markerID){

		var marker = new google.maps.Marker({
			position: {lat: parseFloat(coords[0]), lng: parseFloat(coords[1])},
			// icon: iconImage,
			draggable: draggable,
			// animation: google.maps.Animation.DROP,
			// title: content,
		});

		return marker;
	},

	setMarkers : function (list,map) {

		var markers = [];
		var marker;
		var markerID;

		for (var i = 0; i < list.length; i++) {
			markerID = list[i].id;
			marker = initMap.createMarker(list[i].coords, false, initMap.config.iconImage, markerID);
			markers.push(marker);
			initMap.config.allMarkers[markerID] = marker;
			// bounds.extend(marker.position);
		}

		markerCluster = new MarkerClusterer(map, markers);

		/*setTimeout(function() {
			map.fitBounds(bounds);
			if (map.getZoom() > 16) map.setZoom(14);
	    }, 50);*/

	},
}

