<?php

namespace app\modules\contact\widgets;

use Yii;
use app\modules\contact\models\common\FeedbackForm;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class Feedback extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new FeedbackForm();

        echo $this->render('feedback', [
            'model' => $model,
        ]);
    }

}