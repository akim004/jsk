<?php

namespace app\modules\contact\widgets;

use Yii;
use app\modules\contact\MapAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class Map extends Widget
{
    /**
     * Markers of map
     * [
     *     'id' => 'contact'
     *     'coords' => [
     *         '0' => 42.98003720570486,
     *         '1' => 47.51415252685547,
     *     ]
     * ]
     * @var array
     */
    public $markers;

    /**
     * input class or id where to save the coordinates
     * @var string
     */
    public $whereSave;

    /**
     * can create a new marker
     * @var boolean
     */
	public $isCreateMarkers = true;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        MapAsset::register($this->getView());

        echo $this->render('map', [
            'markers'         => $this->markers,
            'whereSave'       => $this->whereSave,
            'isCreateMarkers' => $this->isCreateMarkers,
        ]);
    }

}