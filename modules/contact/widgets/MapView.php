<?php

namespace app\modules\contact\widgets;

use Yii;
use app\modules\contact\models\common\Contact;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class MapView extends Widget
{
    public $markers;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $map = Contact::getMap();

        echo $this->render('mapView', [
            'map' => $map,
        ]);
    }

}