<div class="contacts-map">

<?
$coords = explode(';', $map->value);
$lat = $coords[0];
$lan = $coords[1];
$script = <<< JS
	$(function(){
		window.mapCustomInit('map', $lat, $lan);
	});
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyCk1poK2npaR0cB-YhhxLTwlkJLipvyQJ0');
$this->registerJs($script, yii\web\View::POS_END);
?>

	<script>
	</script>
	<div id="map"></div>
	<div class="wrapper-holder">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-6">
					<div class="contacts-map__image row"><img src="user_images/contacts.jpg" alt="" /></div>
				</div>
			</div>
		</div>
	</div>
</div>