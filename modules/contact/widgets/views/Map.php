<?
$isCreateMarker = $isCreateMarkers ? 'true' : 'false';
$markerCoords = '';
if($markers){
		foreach ($markers as $data) {
			$markerCoords = $markerCoords . "{
				'id' : '".$data['id']."',
				'coords' : [".$data['coords'][0].",".$data['coords'][1].",]
			},";
		}
	$markers = "initMarkers: [".$markerCoords."]";
}

$script = <<< JS
	function initMapp(){
		initMap.init({
			whereSave: '$whereSave',
			isCreateMarker: $isCreateMarkers,
			$markers,
		});
	}
JS;
$this->registerJs($script, yii\web\View::POS_BEGIN);?>

<div id="map" style="width:100%;height:400px;">

</div>