<?php

namespace app\modules\contact\models\common;

use Yii;
use app\modules\contact\Module;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property integer $id
 * @property integer $param
 * @property string $value
 * @property string $default
 * @property string $label
 * @property string $type
 */
class Contact extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%contact}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['param', 'value', 'default', 'label'], 'safe'],
			[['group'], 'string', 'max' => 128],
			[['value', 'default'], 'string'],
			[['param'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'      => 'ID',
			'group'   => Module::t('module', 'group'),
			'param'   => Module::t('module', 'param'),
			'value'   => Module::t('module', 'value'),
			'default' => Module::t('module', 'default'),
			'label'   => Module::t('module', 'label'),
			'type'    => Module::t('module', 'type'),
		];
	}

	public static function getMap()
	{
		return self::findOne(['group' => 'map', 'param' => 'map']);
	}

	public static function getAddress()
	{
		return self::findOne(['group' => 'main', 'param' => 'address']);
	}

	public static function getEmail()
	{
		return self::findOne(['group' => 'main', 'param' => 'email']);
	}

	public static function getPhone()
	{
		return self::findOne(['group' => 'main', 'param' => 'phone']);
	}

	public static function getSocial()
	{
		return self::find()->where(['group' => 'soc'])->select(['value','param'])->indexBy('param')->column();
	}
}
