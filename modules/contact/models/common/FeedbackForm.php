<?php

namespace app\modules\contact\models\common;

use Yii;
use app\modules\contact\Module;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class FeedbackForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'body'], 'required'],
            ['email', 'email'],
            // ['verifyCode', 'captcha', 'captchaAction' => '/contact/feedback/captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => Module::t('module', 'verifyCode'),
            'email'      => Module::t('module', 'email'),
            'name'       => Module::t('module', 'name'),
            'subject'    => Module::t('module', 'subject'),
            'body'       => Module::t('module', 'body'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function feedback($email)
    {
        if ($this->validate()) {
           /* Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
+               ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();*/

            return true;
        } else {
            return false;
        }
    }
}
