<?php

use yii\db\Schema;
use yii\db\Migration;

class m160804_060017_table_contact extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%contact}}', [
			'id'      => Schema::TYPE_PK,
			'group'  => Schema::TYPE_STRING . '(128) NOT NULL',
			'param'   => Schema::TYPE_STRING . '(128) NOT NULL',
			'value'   => Schema::TYPE_TEXT . ' NOT NULL',
			'default' => Schema::TYPE_TEXT . ' NOT NULL',
			'label'   => Schema::TYPE_STRING . '(255) NOT NULL',
			'type'    => Schema::TYPE_STRING . '(128) NULL DEFAULT NULL',
		], $tableOptions);

		$this->createIndex('idx_contact_param', '{{%contact}}', 'param');

		$this->batchInsert('{{%contact}}',
			['id', 'group','param','value','default','label','type'], [
			['1', 'main', 'phone','89883459911','89883459912','Телефон', 'string'],
			['2', 'main', 'email','guinean@phocoid.net','preaffirmative@marantaceous.co.uk','Email', 'string'],
			['3', 'main', 'address','ул. Гагарина 59б','ул. Гагарина 59б','Адрес', 'text'],
			['4', 'map', 'map','42.966995,47.507538','42.966995,47.507538','Map', 'string'],
		]);
	}

	public function down()
	{
		$this->dropTable('{{%contact}}');
	}
}
