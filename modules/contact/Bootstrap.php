<?php

namespace app\modules\contact;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    use \app\modules\contact\ModuleTrait;

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/contact/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@app/modules/contact/messages',
            'fileMap' => [
                'modules/contact/module' => 'module.php',
            ],
        ];

        $app->urlManager->registerModuleRules($this->getModule());
    }
}