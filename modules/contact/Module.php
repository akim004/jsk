<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/contact/migrations --interactive=0
 */

namespace app\modules\contact;

use Yii;
use app\modules\contact\models\common\Contact;

class Module extends \app\components\Module
{
	public $controllerNamespace = 'app\modules\contact\controllers';

	public function init()
	{
		parent::init();
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/contact/' . $category, $message, $params, $language);
	}

	public static function getUrlRules()
	{
		return [
			// 'admin/contact/<module:[\w\-]+>' => 'admin/contact/default/index',
		];
	}

	public function getAddress()
	{
		return Contact::getAddress()->value;
	}

	public function getPhone()
	{
		return Contact::getPhone()->value;
	}

	public function getEmail()
	{
		return Contact::getEmail()->value;
	}

	public function getSocial()
	{
		return Contact::getSocial();
	}
}
