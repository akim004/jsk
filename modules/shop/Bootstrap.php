<?php

namespace app\modules\shop;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	use \app\modules\shop\traits\ModuleTrait;

	public function bootstrap($app)
	{
		$app->i18n->translations['modules/shop/*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'forceTranslation' => true,
			'basePath' => '@app/modules/shop/messages',
			'fileMap' => [
				'modules/shop/module' => 'module.php',
			],
		];

		$app->urlManager->registerModuleRules($this->getModule());

	}
}