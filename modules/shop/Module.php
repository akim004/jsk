<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/shop/migrations --interactive=0
 */

namespace app\modules\shop;

use Yii;

class Module extends \app\components\Module
{
	public $controllerNamespace = 'app\modules\shop\controllers';

	public function init()
	{
		parent::init();
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/shop/' . $category, $message, $params, $language);
	}

	public static function getUrlRules()
	{

		return [
		];
	}
}
