<?php

namespace app\modules\shop\controllers\frontend;

use Yii;
use app\modules\shop\Module;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CartController extends \app\components\FrontendController
{

	public function actions()
	{
		return [

		];
	}

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Route models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$item = \app\modules\catalog\models\common\Item::find()->one();
		if ($item) {
			\Yii::$app->cart->put($item->getCartPosition(), 65);
			// $a = \Yii::$app->cart->getPositionById($item->getCartPosition());
			$a = \Yii::$app->cart->getPositions();
		}

				echo '<pre>';
				print_r($a);
				exit();
		return $this->render('index', [
		]);
	}
}
