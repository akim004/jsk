<?php

namespace app\modules\shop\controllers\backend;

use Yii;
use app\modules\shop\Module;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends \app\components\BackendController
{

	public function actions()
	{
		return [

		];
	}

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Route models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		return $this->render('index', [
		]);
	}
}
