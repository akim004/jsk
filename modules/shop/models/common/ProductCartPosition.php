<?php

namespace app\modules\shop\models\common;

use \app\modules\catalog\models\common\Item;
use yii\base\Object;

class ProductCartPosition extends Object implements \yz\shoppingcart\CartPositionInterface
{
    use \yz\shoppingcart\CartPositionTrait;
    /**
     * @var Product
     */
    protected $_product;

    public $id;

    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->getProduct()->price;
    }

    /**
     * @return Product
    */
    public function getProduct()
    {
        if ($this->_product === null) {
            $this->_product = Item::findOne($this->id);
        }
        return $this->_product;
    }
}