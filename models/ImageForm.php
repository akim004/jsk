<?php

namespace app\models;

use Exception;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class ImageForm extends Model
{

    const UPLOAD_DIR = 'content-tools-uploads';

    /**
     * @var UploadedFile Uploaded image
     */
    public $image;

    /**
     * @var string Web accessible path to the uploaded image
     */
    public $url;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['image', 'image', 'extensions' => ['png', 'jpg', 'gif'], 'maxWidth' => 1500, 'maxHeight' => 1500, 'maxSize' => 3 * 1024 * 1024]
        ];
    }

    /**
     * Validates and saves the image.
     * Creates the folder to store images if necessary.
     * @return boolean
     */
    public function upload()
    {
        try {
            if ($this->validate()) {
                $save_path = FileHelper::normalizePath(Yii::getAlias('@app/web/' . self::UPLOAD_DIR));
                FileHelper::createDirectory($save_path);
                $this->url = Yii::getAlias('@web/' . self::UPLOAD_DIR . '/' . $this->image->baseName . '.' . $this->image->extension);
                return $this->image->saveAs(FileHelper::normalizePath($save_path . '/' . $this->image->baseName . '.' . $this->image->extension));
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }
        return false;
    }
}
