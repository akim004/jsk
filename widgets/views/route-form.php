<?php
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;

BootstrapPluginAsset::register($this);

$labelOptions = ['class' => 'control-label'];
$inputOptions = ['class' => 'form-control'];
?>

<div class="" id="route-form">
    <div class="form-group">
        <?= Html::activeLabel($model, 'alias', $labelOptions) ?>
        <?= Html::activeTextInput($model, 'alias', $inputOptions) ?>
    </div>
</div>