<?php
use app\modules\slider\models\common\Slider;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;

BootstrapPluginAsset::register($this);

$labelOptions = ['class' => 'control-label'];
$inputOptions = ['class' => 'form-control'];
?>

<div >
    <label> <?= $model->attributeLabels()['imageUpload']?></label>
    <?
        echo \kartik\file\FileInput::widget([
            'id' => 'slider-images-upload',
            'name' => 'Slider[imageUpload][]',
            'language' => 'ru',
            'options' => ['multiple' => true],

            'pluginOptions' => [
                'uploadExtraData' => [
                    'item_id' => $owner->id,
                    'module'  => $owner->getClassName(),
                ],
                'previewFileType' => 'any',
                'maxImageWidth' => $model->maxImageWidth,
                'maxImageHeight' => $model->maxImageHeight,
                'initialCaption'   => "Загрузите фотографии",
                'overwriteInitial' => true,
                'showUpload'       => true,
                'allowedFileExtensions' => ['jpg','gif','png', 'jpeg'],
                'uploadUrl' => empty($owner->id) && $owner->id !== 0 ? '' : \yii\helpers\Url::to(['slider-file-upload']),
            ],
        ]);
        ?>
</div>

<?php \yii\widgets\Pjax::begin([
        'id' => 'sliders-container',
        'timeout' => 10000,
]); ?>

<?= GridView::widget([
    'sortableAction' => ['/admin/slider/default/sort'],
    'dataProvider' => $dataProvider,
    'columns' => [
        // 'name',
        [
            'format' => 'html',
            'label' => 'Фотография',
            'value' => function ($data) {
                return Html::img($data->getImage()->getUrl('100x'));
            },
        ],
        [
            'class'     => \app\components\EditableColumn::className(),
            'type'      => 'select',
            'url'       => ['/admin/slider/default/editable'],
            'attribute' => 'published',
            'format' => 'raw',
            'filter'    => Slider::getStatusesArray(),
            'value' => function($data) {
                return Slider::getStatusesArray()[$data->published];
            },
            'editableOptions' => [
                'mode' => 'pop', // 'inline'
                'source' => json_encode(Slider::getStatusesArrayForSource()),
                'placement' => 'bottom',
            ],
        ],

        [
            'format' => 'raw',
            'label' => '',
            'value' => function ($data) {
                return Html::a('Удалить', \yii\helpers\Url::to(['/admin/slider/default/delete-module', 'id' => $data->id]),
                    [
                        'class' => 'pjax-link',
                        'data-pjax' => 0,
                        'data-pjax-id' => 'sliders-container',
                        'data-confirm-text' => 'Вы уверены, что хотите удалить баннер?',
                    ]);
            },
        ],
    ],
]); ?>

<?php \yii\widgets\Pjax::end(); ?>

<?
$script = <<< JS
    $(function(){
        $('#slider-images-upload').on('fileuploaded', function(event, data, previewId, index) {
            $.pjax.reload({container:'#sliders-container',timeout: 10000,});
        });
    });
JS;

 $this->registerJs($script, yii\web\View::POS_READY);?>