<?php

$jsMessage = \yii\helpers\Html::encode($message);

?>

<?if($message){?>
	<?php $this->registerJs("
		$(function () {
			app.alertMessage('$jsMessage');
		});
	") ;?>

<?}?>
<div class="success-mess message" id="notify_success">
	<span class="icon"></span>
	<span class="success-message">
			<?= $message ?>
	</span>
</div>