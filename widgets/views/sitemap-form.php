<?php
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;

BootstrapPluginAsset::register($this);

$labelOptions = ['class' => 'control-label'];
$inputOptions = ['class' => 'form-control'];
?>


<div class="" id="sitemap-form">
<br>
    <div class="form-group">
        <?= Html::activeCheckbox($model, 'enable') ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'priority', $labelOptions) ?>
        <?= Html::activeTextInput($model, 'priority', $inputOptions) ?>
    </div>
</div>