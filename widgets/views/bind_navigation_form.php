<?php
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;

BootstrapPluginAsset::register($this);

$labelOptions = ['class' => 'control-label'];
$inputOptions = ['class' => 'form-control'];
?>

<div class="form-group">
    <?= Html::activeLabel($model, 'id', $labelOptions) ?>
    <?= Html::activeDropDownList($model, 'id', ['0'=>'Нет']+$model->getListData(), $inputOptions) ?>
    <?//= $form->field($model, 'navigationId')->dropDownList($model->getNavigationListData()) ?>
</div>
