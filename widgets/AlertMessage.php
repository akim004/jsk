<?php
namespace app\widgets;

use Yii;
use app\assets\MessageAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class AlertMessage extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        MessageAsset::register($this->getView());

        return $this->render('alert-message', [
            'message' => $this->message
        ]);
    }

}