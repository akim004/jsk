<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

class SitemapForm extends Widget
{
	public $model;

	public function init()
	{
		parent::init();

		if (empty($this->model)) {
			throw new InvalidConfigException('Required `model` param isn\'t set.');
		}
	}

	public function run()
	{
		return $this->render('sitemap-form', [
			'model' => $this->model->sitemapText
		]);
	}

}