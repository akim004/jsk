<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

class ContentTools extends \bizley\contenttools\ContentTools
{
	public function initEditor()
    {
    	if(Yii::$app->user->can('moder')){
			parent::initEditor();
		}
    }
}