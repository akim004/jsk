<?php
namespace app\widgets;

use Yii;
use app\modules\slider\models\common\Slider;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class SliderForm extends Widget
{
    public $model;

    public function init()
    {
        parent::init();

        if (empty($this->model)) {
            throw new InvalidConfigException('Required `model` param isn\'t set.');
        }
    }

    public function run()
    {
        $query = Slider::find()->where(['in', 'id' , $this->model->slidersIds])->orderBy('position ASC');
        $dataProvider = new \yii\data\ActiveDataProvider([
                                'query' => $query,
                                'pagination' => false,
                            ]);

        echo $this->render('slider-form', [
            'owner' => $this->model,
            'model' => $this->model->slider,
            'dataProvider' => $dataProvider,
        ]);
    }

}