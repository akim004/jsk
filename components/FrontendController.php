<?php

namespace app\components;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * BackendController is base class for backend controllers in DotPlant2.
 * In such controllers we should not see backend floating panel.
 * @package app\backend\components
 */
class FrontendController extends Controller
{
	public $layout = "@app/modules/main/views/layouts/main";

	public $title;
	public $description;
	public $keywords;

	public function init()
	{
		parent::init();


		\Yii::$container->set('yii\widgets\Breadcrumbs', [
			'tag' => 'div',
			'options' => ['class' => 'breadcrumbs'],
			'itemTemplate' => "{link} / ", // template for all links
			'homeLink' => [
				'label' => 'Главная',
				'url' => \yii\helpers\Url::to('/'),
			],
			'encodeLabels' => false,
			'activeItemTemplate' => '<span>{link}</span>',
		]);
	}

	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
		$this->title = Yii::$app->getModule('seo')->getConfig('seoTitle');
		$this->description = Yii::$app->getModule('seo')->getConfig('seoDescription');
		$this->keywords = Yii::$app->getModule('seo')->getConfig('seoKeywords');

		return true;
	}
}