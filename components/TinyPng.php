<?php
/**
 * @link http://www.bigbrush-agency.com/
 * @copyright Copyright (c) 2015 Big Brush Agency ApS
 * @license http://www.bigbrush-agency.com/license/
 */

namespace app\components;

use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\web\UnauthorizedHttpException;
use Tinify;

class TinyPng extends Component
{
	const VERSION = '0.0.1';

	/**
	 * @var string $apiKey the api key to use with [TinyPng](https://tinypng.com).
	 */
	public $apiKey = '';
	public $apiKeyArray = [
		'vRD5FdQAmHDhpPWY_Yd_yqSzFct2WgNl',
		'xwVLm2tboi0ohdtQMEF473UCSqVkS8yW',
	];
	public $mayCompress = false;


	/**
	 * Initializes the component by validating [[apiKey]].
	 *
	 * @throws InvalidConfigException if [[apiKey]] is not set.
	 * @throws UnauthorizedHttpException if [[apiKey]] could not be validated.
	 */
	public function init()
	{
		/*if (!$this->apiKey) {
			throw new InvalidConfigException("The property 'apiKey' must be in set in " . get_class($this) . ".");
		}*/
		try {
			foreach ($this->apiKeyArray as $key => $apiKey) {
				$this->apiKey = $apiKey;
				Tinify\setKey($this->apiKey);
				Tinify\validate();
				if($this->compressionsThisMonth() < 500){
					$this->mayCompress = true;
					break;
				}
			}
		} catch(\Tinify\Exception $e) {
			throw new UnauthorizedHttpException("The specified apiKey '$this->apiKey' could not be validated.");
		}
	}

	/**
	 * Compresses the specified image.
	 *
	 * @param string $src the image source path.
	 * @param string $dst the image destination path after being optimized. If not set $src
	 * will be overwritten.
	 */
	public function compress($src, $dst = null)
	{
		if($this->mayCompress){
			$dst = $dst ?: $src;
			$source = Tinify\fromFile($src);
			$source->toFile($dst);
		}
	}

	public function compressionsThisMonth()
	{
		return  Tinify\compressionCount();
	}


	/**
	 * Resizes and compresses the specified image.
	 *
	 * @param string $src the image source path.
	 * @param string $dst the image destination path after being resized. If not set $src
	 * will be overwritten.
	 * @param array $options options used when resizing. Options array must be formatted like so:
	 * ~~~php
	 * [
	 *     'method' => 'fit',
	 *     'width' => 150,
	 *     'height' => 100,
	 * ]
	 * ~~~
	 *
	 * Available methods are:
	 *     - scale
	 *     - fit
	 *     - shrink
	 *
	 * See [TinyPng docs](https://tinypng.com/developers/reference/php) for information about each method.
	 */
	public function resize($src, $dst, $options)
	{
		$dst = $dst ?: $src;
		$source = Tinify\fromFile($src);
		$resized = $source->resize($options);
		$resized->toFile($dst);
	}
}
