<?php

namespace app\components;

interface ModuleInterface
{

    public static function getUrlRules();

    public static function t($category, $message, $params = [], $language = null);

    public function registerConfig($config = []);
}