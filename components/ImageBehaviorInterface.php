<?php

namespace app\components;

interface ImageBehaviorInterface
{
	// public function getModule();

	public function attachImage($absolutePath, $isMain = false, $name = '');

	/**
	 * Sets main image of model
	 * @param $img
	 * @throws \Exception
	 */
	public function setMainImage($img);

	 /**
	 * Clear all images cache (and resized copies)
	 * @return bool
	 */
	public function clearImagesCache();

	 /**
	 * Returns model images
	 * First image alwats must be main image
	 * @return array|yii\db\ActiveRecord[]
	 */
	public function getImages();

	 /**
	 * returns main model image
	 * @return array|null|ActiveRecord
	 */
	public function getImage();

	/**
	 * returns model image by name
	 * @return array|null|ActiveRecord
	 */
	public function getImageByName($name);

	 /**
	 * Remove all model images
	 */
	public function removeImages();

	/**
	 *
	 * removes concrete model's image
	 * @param Image $img
	 * @throws \Exception
	 */
	// public function removeImage(Image $img);
}