<?php

namespace app\components;

use Yii;
use yii\web\ForbiddenHttpException;

abstract class Module extends \yii\base\Module implements \app\components\ModuleInterface
{

	use \app\components\ModuleTrait;

	public function init()
    {
        parent::init();

        $this->registerConfig($this->configFromSource);

        $this->configurateKCfinder();
    }

    public function configurateKCfinder($value='')
    {
    	$kcfOptions = array_merge(\iutbay\yii2kcfinder\KCFinder::$kcfDefaultOptions, [
			    'uploadURL' => Yii::getAlias('@web').'/web/uploads/static',
			    'encoding' => 'UTF-8',
			    'access' => [
			        'files' => [
			            'upload' => true,
			            'delete' => true,
			            'copy' => true,
			            'move' => true,
			            'rename' => true,
			        ],
			        'dirs' => [
			            'create' => true,
			            'delete' => true,
			            'rename' => true,
			        ],
			    ],
			]);
			\Yii::$app->session->set('KCFINDER', $kcfOptions);
    }

}
