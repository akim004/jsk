<?php

namespace app\components;

trait ModuleTrait
{
	protected $config = []; // другие настройки не указанные в коде, но присутствуюещие в источнике
	public $configFromSource = []; // настройки из другого источника

	/**
	 * Установить настройку модуля
	 */
	public function registerConfig($config = [])
	{
		if(!$config){
			return '';
		}

		foreach ($config as $key => $value) {
			if(property_exists($this, $key)){
				$this->$key = $value;
			}else{
				$this->config[$key] = $value;
			}
		}
	}

	/**
	 * получить настройки
	 * @param  string $param
	 * @return mixed
	 */
	public function getConfig($param)
	{
		if(property_exists($this, $param)){
			return $this->$param;
		}else{
			return $this->config[$param];
		}
	}
}