<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/admin.css'
    ];
    public $js = [
        'js/adminApplication.js',
        'js/shape/jquery.liActualSize.js',
        'js/shape/raphael-min.js',
        'js/shape/shapeOverlay.js',
        'js/shape/polyDraw.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
