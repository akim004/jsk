<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MessageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'lib/message/message.css',
    ];
    public $js = [
        'lib/message/Flash.js',
    ];
    public $depends = [];
}
