<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Иконка загрузки при submit
 */
class LoaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'lib/submit-loader/ladda-themeless.css', 
    ];
    public $js = [
        'lib/submit-loader/spin.js',
        'lib/submit-loader/ladda.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
