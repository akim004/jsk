<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/reset.css',
		'css/style.css',
		'css/fancybox.css',
	];
	public $js = [
		'js/jquery.min.js',
		'js/jquery-ui.min.js',
		'js/jquery.placeholder.min.js',
		'js/jquery.carouFredSel.js',
		'js/jquery.toShowHide.js',
		'js/jquery.formstyler.min.js',
		'js/jquery.fancybox.pack.js',
		'js/main.js',
		'js/application.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
	];
}
