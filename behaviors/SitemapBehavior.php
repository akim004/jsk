<?php
namespace app\behaviors;

use Yii;
// use app\models\SeoText;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class SitemapBehavior extends Behavior
{
	private $_model;

	public function events()
	{
		return [
			ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
			ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
			ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
		];
	}

	public function afterInsert()
	{
		if($this->sitemapText->load(Yii::$app->request->post())){
			$this->sitemapText->save();
		}
	}

	public function afterUpdate()
	{
		if($this->sitemapText->load(Yii::$app->request->post())){
		   $this->sitemapText->save();
		}
	}

	public function afterDelete()
	{
		if(!$this->sitemapText->isNewRecord){
			$this->sitemapText->delete();
		}
	}

	public function getSitemap()
	{
		$class = $this->getSitemapIdentityClass();
		return $this->owner->hasOne($class::className(), ['item_id' => $this->owner->primaryKey()[0]])->where(['class' => $this->owner->className]);
	}

	public function getSitemapText()
	{
		if(!$this->_model)
		{
			$this->_model = $this->owner->sitemap;
			if(!$this->_model){
				$class = $this->getSitemapIdentityClass();
				$this->_model = new $class([
					'class' => $this->owner->className,
					'item_id' => $this->owner->primaryKey,
					'priority' => 0.5,
					'enable' => 1,
				]);
			}
		}

		return $this->_model;
	}

	protected function getSitemapIdentityClass()
	{
		return '\app\modules\seo\models\Sitemap';
	}

	public function getClassName()
	{
		if (!is_object($this->owner) && !is_string($this->owner)) {
			return false;
		}

		$class = explode('\\', (is_string($this->owner) ? $this->owner : get_class($this->owner)));
		return $class[count($class) - 1];
	}
}