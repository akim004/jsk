<?php
namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class SliderBehavior extends Behavior
{
	private $_model;
	private $_module;

	public function events()
	{
		return [
			ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
			ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
			ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
		];
	}

	public function afterInsert()
	{
		$this->slider->imageUpload = UploadedFile::getInstances($this->slider, 'imageUpload');
		if($this->slider->imageUpload){
			$this->slider->module = $this->owner->getClassName();
			$this->slider->item_id = $this->owner->primaryKey;
			$this->saveSliders($this->slider->imageUpload);
		}
	}

	public function afterUpdate()
	{
		$this->slider->imageUpload = UploadedFile::getInstances($this->slider, 'imageUpload');
		if($this->slider->imageUpload){
			$this->slider->module = $this->owner->getClassName();
			$this->slider->item_id = $this->owner->primaryKey;
			$this->saveSliders($this->slider->imageUpload);
		}
	}

	public function afterDelete()
	{
		if($this->owner->sliders){
			foreach ($this->owner->sliders as $slider) {
				$slider->delete();
			}
		}
	}

	public function getSliders()
	{
		$class = $this->getSliderModule()->identityClass;
		return $this->owner->hasMany($class::className(), ['item_id' => $this->owner->primaryKey()[0]])->where(['module' => $this->owner->getClassName()])->orderBy('position ASC');
	}

	public function getSlidersIds()
	{
	   return $this->getSliders()->select('id')->indexBy('id')->column();
	}

	public function getSlider()
	{
		if(!$this->_model)
		{
			if(!$this->_model){
				$class = $this->getSliderModule()->identityClass;
				$this->_model = new $class([
					'module' => $this->owner->getClassName(),
					'item_id' => $this->owner->primaryKey
				]);
			}
		}

		return $this->_model;
	}

	protected function getSliderModule()
	{
		if ($this->_module == null) {
			$this->_module = \Yii::$app->getModule('slider');
		}

		if(!$this->_module){
			throw new Exception("\n\n\n\n\nYii2 slider module not found, may be you didn't add it to your config?\n\n\n\n");
		}

		return $this->_module;
	}

	public function getClassName()
	{
		if (!is_object($this->owner) && !is_string($this->owner)) {
			return false;
		}

		$class = explode('\\', (is_string($this->owner) ? $this->owner : get_class($this->owner)));
		return $class[count($class) - 1];
	}

	public function saveSliders($files)
	{
		if($files){
			foreach ($files as $file) {
				$class = $this->getSliderModule()->identityClass;
				$slider = new $class([
					'module' => $this->slider->module,
					'item_id' => $this->owner->primaryKey
				]);
				if($slider->save()){
					$file->saveAs('uploads/runtime/'.$file->name);
					$slider->attachImage('uploads/runtime/'.$file->name);
					@unlink('uploads/runtime/'.$file->name);
				}
			}
		}
	}


}