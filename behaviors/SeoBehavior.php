<?php
namespace app\behaviors;

use Yii;
// use app\models\SeoText;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class SeoBehavior extends Behavior
{
    private $_model;
    private $_module;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterInsert()
    {
        if($this->seoText->load(Yii::$app->request->post())){
            if(!$this->seoText->isEmpty()){
                $this->seoText->save();
            }
        }
    }

    public function afterUpdate()
    {
        if($this->seoText->load(Yii::$app->request->post())){
            if(!$this->seoText->isEmpty()){
                $this->seoText->save();
            } else {
                if($this->seoText->primaryKey){
                    $this->seoText->delete();
                }
            }
        }
    }

    public function afterDelete()
    {
        if(!$this->seoText->isNewRecord){
            $this->seoText->delete();
        }
    }

    public function getSeo()
    {
        $class = $this->getSeoModule()->identityClass;
        return $this->owner->hasOne($class::className(), ['item_id' => $this->owner->primaryKey()[0]])->where(['class' => $this->owner->className]);
    }

    public function getSeoText()
    {
        if(!$this->_model)
        {
            $this->_model = $this->owner->seo;
            if(!$this->_model){
                $class = $this->getSeoModule()->identityClass;
                $this->_model = new $class([
                    'class' => $this->owner->className,
                    'item_id' => $this->owner->primaryKey
                ]);
            }
        }

        return $this->_model;
    }

    protected function getSeoModule()
    {
        if ($this->_module == null) {
            $this->_module = \Yii::$app->getModule('seo');
        }

        if(!$this->_module){
            throw new Exception("\n\n\n\n\nYii2 seo module not found, may be you didn't add it to your config?\n\n\n\n");
        }

        return $this->_module;
    }

    public function getClassName()
    {
        if (!is_object($this->owner) && !is_string($this->owner)) {
            return false;
        }

        $class = explode('\\', (is_string($this->owner) ? $this->owner : get_class($this->owner)));
        return $class[count($class) - 1];
    }
}