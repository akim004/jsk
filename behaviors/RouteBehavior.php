<?php
namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class RouteBehavior extends Behavior
{
	private $_model;
	private $_module;

	public function events()
	{
		return [
			ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
			ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
			ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
		];
	}

	public function afterInsert()
	{
		if($this->routeText->load(Yii::$app->request->post())){
			if(!$this->routeText->isEmpty()){
				$this->routeText->save();
			}
		}
	}

	public function afterUpdate()
	{
		if($this->routeText->load(Yii::$app->request->post())){
			if(!$this->routeText->isEmpty()){
				$this->routeText->origin = $this->owner->createUrl;
				$this->routeText->save();
			} else {
				if($this->routeText->primaryKey){
					$this->routeText->delete();
				}
			}
		}
	}

	public function afterDelete()
	{
		if(!$this->routeText->isNewRecord){
			$this->routeText->delete();
		}
	}

	public function getRoute()
	{
		$class = $this->getRouteModule()->identityClass;
		return $this->owner->hasOne($class::className(), ['item_id' => $this->owner->primaryKey()[0]])->where(['class' => $this->owner->className]);
	}

	public function getRouteText()
	{
		if(!$this->_model)
		{
			$this->_model = $this->owner->route;
			if(!$this->_model){
				$class = $this->getRouteModule()->identityClass;
				$this->_model = new $class([
					'class' => $this->owner->className,
					'item_id' => $this->owner->primaryKey,
					'origin' => $this->owner->createUrl,
				]);
			}
		}

		return $this->_model;
	}

	protected function getRouteModule()
	{
		if ($this->_module == null) {
			$this->_module = \Yii::$app->getModule('route');
		}

		if(!$this->_module){
			throw new Exception("\n\n\n\n\nYii2 route module not found, may be you didn't add it to your config?\n\n\n\n");
		}

		return $this->_module;
	}

	public function getClassName()
	{
		if (!is_object($this->owner) && !is_string($this->owner)) {
			return false;
		}

		$class = explode('\\', (is_string($this->owner) ? $this->owner : get_class($this->owner)));
		return $class[count($class) - 1];
	}
}