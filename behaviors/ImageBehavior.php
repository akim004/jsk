<?php
/**
 * Created by PhpStorm.
 * User: kostanevazno
 * Date: 22.06.14
 * Time: 16:58
 */

namespace app\behaviors;

use yii\db\ActiveRecord;


class ImageBehavior extends \rico\yii2images\behaviors\ImageBehave implements \app\components\ImageBehaviorInterface
{
	use \app\modules\image\traits\ModuleTrait;

	public $maxImageWidth;
	public $maxImageHeight;
	public $maxFiles = 10;
	public $maxSize; // 5МБ
	public $skipOnEmpty = true;
	public $enableBehaviorValidate = true; // влюкчить валидацию поведения

	public $uploadAttribute = 'imageUpload'; // 5МБ


	public function init()
    {
        parent::init();

        $this->setAttributesInSource();
    }

	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate'
		];
	}

	public function beforeValidate($event)
	{
		if($this->enableBehaviorValidate){
			/** @var ActiveRecord $model */
	        $model = $this->owner;

	        if($model->{$this->uploadAttribute} || !$this->skipOnEmpty){
		        $validator = \yii\validators\Validator::createValidator('image', $model, $this->uploadAttribute,  [
		            'extensions'  => 'png,jpg,gif',
					'maxHeight'   => $this->maxImageHeight,
					'maxWidth'    => $this->maxImageWidth,
					'skipOnEmpty' => true,
					'maxSize'     => $this->maxSize,
					'maxFiles'    => $this->maxFiles,
					'tooBig' => 'Файл «{file}» слишком большой. Размер не должен превышать {limit} МБ.',
		        ]);

		        $validator->validateAttribute($model, $this->uploadAttribute);
	        }
		}
	}

	// Устновить параметры валидации по умолчанию
	private function setAttributesInSource()
	{
		$this->setMaxImageWidth();
		$this->setMaxImageHeight();
		$this->setMaxSize();
	}

	protected function setMaxImageWidth()
	{
		$this->maxImageWidth = $this->maxImageWidth ? $this->maxImageWidth : $this->getModule()->maxImageWidth;
	}

	protected function setMaxImageHeight()
	{
		$this->maxImageHeight = $this->maxImageHeight ? $this->maxImageHeight : $this->getModule()->maxImageHeight;
	}

	protected function setMaxSize()
	{
		$this->maxSize = ($this->maxSize ? $this->maxSize : $this->getModule()->maxSize)*1024*1024;
	}
}