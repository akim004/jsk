<?php
namespace app\behaviors;

use Yii;
use \app\modules\navigation\models\common\Navigation;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class BindNavigationBehavior extends Behavior
{
	public $slug;
	public $navigationId;
	public $navigationClass;

	private $_model;

	public function getNavigation()
    {
    	return Navigation::find()->where(['slug' => $this->owner->slug])->one();
    }

    public function getNavigationById($id)
    {
    	return Navigation::findOne($id);
    }

	public function getNavigationModel()
	{
		if(!$this->_model)
        {
            $this->_model = $this->owner->navigation;
            if(!$this->_model){
                $this->_model = new Navigation();
            }
        }

        return $this->_model;
	}

	public function events()
	{
		return [
			ActiveRecord::EVENT_INIT => 'init',
			ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
			ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
		];
	}

	public function afterInsert()
	{
		if($navigation = $this->getNavigationById(Yii::$app->request->post('Navigation')['id'])){
			$navigation->slug = $this->owner->slug;
			$navigation->save(false);
		}
	}

	public function afterUpdate()
	{
		if($navigation = $this->getNavigationById(Yii::$app->request->post('Navigation')['id'])){
			$navigation->slug = $this->owner->slug;
			$navigation->save(false);
		}
	}
}