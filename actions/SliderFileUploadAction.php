<?php
namespace app\actions;

use Yii;
use yii\base\Action;
use yii\web\UploadedFile;

class SliderFileUploadAction extends Action
{
	public $modelClass;

	public function init()
    {
        if ($this->modelClass === null) {
            throw new InvalidConfigException("'modelClass' cannot be empty.");
        }
    }

    public function run()
    {
        $itemId = Yii::$app->request->post('item_id');
		$module = Yii::$app->request->post('module');

		$model = $this->findModel($itemId);

		if($model){
			$model->slider->module = $module;
			$model->slider->item_id = $itemId;
			$model->slider->imageUpload = UploadedFile::getInstances($model->slider, 'imageUpload');
			$model->saveSliders($model->slider->imageUpload);
		}

		echo '{}';
		exit();
    }

    protected function findModel($id)
	{
		$class= $this->modelClass;
		if (($model = $class::findOne($id)) !== null) {
			return $model;
		} else {
			$model = new $class;
			$model->id = $id;
			return $model;
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}