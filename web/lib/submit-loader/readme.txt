'lib/submit-loader/ladda-themeless.css',

'lib/submit-loader/spin.js',
'lib/submit-loader/ladda.js',

<?= Html::submitButton('Сохранить', ['class' => 'ladda-button btn btn-primary', 'data-style' => 'slide-right']) ?>

1.	app.createSubmitLoader($('#js-change-password-form .ladda-button')[0]);

2.	beforeSend: function( xhr ) {
		app.startSubmitLoader();
	}

3	.done(function(data) {
		app.stopSubmitLoader();
	})


createSubmitLoader: function(button){
	loader = Ladda.create(button);
},
startSubmitLoader: function(){
	loader.start();
},
stopSubmitLoader: function(){
	loader.stop();
},