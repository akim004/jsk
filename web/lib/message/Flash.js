$.fn.notify = function(settings_overwrite){
    settings = {
        placement:"top",
        default_class: ".message",
        content_class: ".success-message",
        delay:0,
        hideDelay: 3000,
        content: '',
    };
    $.extend(settings, settings_overwrite);

    $(settings.default_class).each(function(){$(this).hide();});
    $(settings.content_class).html(settings.content);

    $(this).show().css(settings.placement, -$(this).outerHeight());
    obj = $(this);

    if(settings.placement == "bottom"){
        setTimeout(function(){obj.animate({bottom:"0"}, 500)},settings.delay);
    }
    else{
        setTimeout(function(){obj.animate({top:"0"}, 500)},settings.delay);
    }

    setTimeout(function(){
            obj.fadeTo('slow', 0, function() {
                $(this).slideUp("slow", function() {
                    $(this).remove();
                });
            })},settings.hideDelay);

    obj.on('click', (function () {
        $(this).fadeTo('slow', 0, function() {
            $(this).slideUp("slow", function() {
                $(this).remove();
            });
        });
    }));

}