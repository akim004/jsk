'use strict';

var loader;

var app = {
	config : {

	},

	init : function(config){



		$.extend(app.config, config);

		// вход
		$(document).on('beforeSubmit', '#js-login-form', function (e) {

			app.createSubmitLoader($(this).find('.ladda-button')[0]);

			var form = $(this);

			if (form.find('.has-error').length) {
				  return false;
			}

			$.ajax({
				url: $(this).attr('action'),
				type: 'POST',
				dataType: 'json',
				data: form.serialize(),
				beforeSend: function( xhr ) {
					app.startSubmitLoader();
				}
			})
			.done(function(data) {
				_thank('Вы успешно вошли.', data.redirect);
			})
			.fail(function() {_thank('При входе произошла ошибка, пожалуйста повторите вход');})
			.always(function(){app.stopSubmitLoader();});

			return false;
		});

		// регистрация
		$(document).on('beforeSubmit', '#js-signup-form', function (e) {

			app.createSubmitLoader($(this).find('.ladda-button')[0]);

			var form = $(this);

			if (form.find('.has-error').length) {
				  return false;
			}

			$.ajax({
				url: $(this).attr('action'),
				type: 'POST',
				dataType: 'json',
				data: form.serialize(),
				beforeSend: function( xhr ) {
					app.startSubmitLoader();
				}
			})
			.done(function(data) {
				_thank('Регистрация прошла успешно!', data.redirect);
			})
			.fail(function() {_thank('При регистрации произошла ошибка, пожалуйста повторите регистрацию');})
			.always(function(){app.stopSubmitLoader();});

			return false;
		});

	},

	// submit lloader
	createSubmitLoader: function(button){
		loader = Ladda.create(button);
	},
	startSubmitLoader: function(){
		loader.start();
	},
	stopSubmitLoader: function(){
		loader.stop();
	},
};


$(window).load(function() {
	app.init();
});
