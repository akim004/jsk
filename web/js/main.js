﻿$.ui.dialog.prototype._focusTabbable = function(){};

$(document).ready(function(){

	$('input[placeholder], textarea[placeholder]').placeholder();

	$('input, textarea').focus(function(){
		$(this).data('placeholder', $(this).attr('placeholder'))
		$(this).attr('placeholder', '');
	});
	$('input, textarea').blur(function(){
		$(this).attr('placeholder', $(this).data('placeholder'));
	});

	$('input[type="checkbox"]').styler();

	$('.fancybox').fancybox();

	$('.num-box input').force_numeric_only().keyup(function(){if($(this).val().length<=0)$(this).val('0')});

	$('.num-box a').on('click', function(){
		var $this = $(this);
		var $this_box = $this.parent();
		var $this_in = $this_box.find('input');
		var number = Number($this_in.val());
		number = ($this.is('.plus')) ? number+1 : number-1;
		number = (number > 0) ? number : 0;
		$this_in.val(number);
		return false;
	});

	$('.site-header .catalog li').toShowHide({
		button: '> a',
		box: '> ul',
		effect: 'fade',
		anim_speed: 200,
		delay: 50,
		close_only_button: false,
		onBefore: function(el){
			el.addClass('active');
		},
		onAfter: function(el){
			el.removeClass('active');
		}
	});

	$dialog_thank = $('.dialog-thank');

	$dialog_thank.dialog({
		autoOpen: false,
		modal: true,
		width: 265,
		draggable: false,
		resizable: false,
		dialogClass: 'ui-dialog-thank',
		closeText: '',
		show: {
			effect: 'drop', duration: 400
		},
		hide: {
			effect: 'drop', duration: 400
		},
		open: function(){
			$('.ui-widget-overlay').on('click', function(){
				$dialog_thank.dialog('close');
			});
			$dialog_thank.find('.bt a').bind('click', function(){
				$dialog_thank.dialog('close');
				return false;
			});
		},
		close: function(){
			var button = $dialog_thank.find('.bt a');
			if(button.attr('href')){
				console.log('button.attr(\'href\')', button.attr('href'));
				window.location = button.attr('href');
			}else{
				button.unbind('click');
			}
		}
	});

	_thank = function(text, url = ''){
		$dialog_thank.find('.text').html(text);
		if(url){
			console.log('url', url);
			$dialog_thank.find('.button').attr('href', url);
		}
		$dialog_thank.dialog('open');
	};

	var $dialog_question = $('.dialog-question');

	$('.open-question').on('click', function(){

		$dialog_question.dialog({
			autoOpen: true,
			modal: true,
			width: 714,
			draggable: false,
			resizable: false,
			dialogClass: 'ui-dialog-form',
			closeText: '',
			show: {
				effect: 'drop', duration: 400
			},
			hide: {
				effect: 'drop', duration: 400
			},
			open: function(){
				$('.ui-widget-overlay').on('click', function(){
					$dialog_question.dialog('close');
				});
			},
			close: function(){
				$dialog_question.find('input[type=text], textarea').val('');
			}
		});

		return false;
	});
	$dialog_question.find('.bt button').on('click', function(){
		$dialog_question.dialog('close');
		_thank('Ваш вопрос успешно отправлен.');
		return false;
	});

	var $dialog_login = $('.dialog-login');

	$('.open-login').on('click', function(){

		$dialog_login.dialog({
			autoOpen: true,
			modal: true,
			width: 786,
			draggable: false,
			resizable: false,
			dialogClass: 'ui-dialog-form',
			closeText: '',
			show: {
				effect: 'drop', duration: 400
			},
			hide: {
				effect: 'drop', duration: 400
			},
			open: function(){
				$('.ui-widget-overlay').on('click', function(){
					$dialog_login.dialog('close');
				});
			},
			close: function(){
				$dialog_login.find('input[type=text], input[type=password]').val('');
			}
		});

		return false;
	});

	var $dialog_register = $('.dialog-register');

	$('.open-register').on('click', function(){

		if($dialog_login.css('display') == 'block'){
			$dialog_login.dialog('close');
		}

		$dialog_register.dialog({
			autoOpen: true,
			modal: true,
			width: 416,
			draggable: false,
			resizable: false,
			dialogClass: 'ui-dialog-form',
			closeText: '',
			show: {
				effect: 'drop', duration: 400
			},
			hide: {
				effect: 'drop', duration: 400
			},
			open: function(){
				$('.ui-widget-overlay').on('click', function(){
					$dialog_register.dialog('close');
				});
			},
			close: function(){
				$dialog_register.find('input[type=text], input[type=password]').val('');
			}
		});

		return false;
	});
	/*$dialog_register.find('.bt button').on('click', function(){
		$dialog_register.dialog('close');
		_thank('Регистрация прошла успешно!');
		return false;
	});*/

	var $dialog_checkout = $('.dialog-checkout');

	$('.open-checkout').on('click', function(){

		$dialog_checkout.dialog({
			autoOpen: true,
			modal: true,
			width: 835,
			draggable: false,
			resizable: false,
			dialogClass: 'ui-dialog-form',
			closeText: '',
			show: {
				effect: 'drop', duration: 400
			},
			hide: {
				effect: 'drop', duration: 400
			},
			open: function(){
				$('.ui-widget-overlay').on('click', function(){
					$dialog_checkout.dialog('close');
				});
			},
			close: function(){
				$dialog_checkout.find('input[type=text], textarea').val('');
			}
		});

		return false;
	});
	$dialog_checkout.find('.bt button').on('click', function(){
		$dialog_checkout.dialog('close');
		_thank('Ваш заказ успешно подтвержден.');
		return false;
	});

	$('.tab-switch a').on('click', function(){
		var $this = $(this);
		var $tab = $this.parent();
		var $tab_parent = $tab.parent();
		$tab.find('a').removeClass('active');
		$this.addClass('active');
		$tab_parent.find('.tab-box').hide();
		$tab_parent.find('.tab-box[data-id='+$this.data('id')+']').show();
		return false;
	});

	$('.product-filter .tab a').on('click', function(){
		var $this = $(this);
		$('.product-filter .tab a').removeClass('active');
		$this.addClass('active');
		$('ul.product-list').hide();
		$('ul.product-list[data-id='+$this.data('id')+']').show();
		return false;
	});

	$('.product-detail .other a').on('click', function(){
		$('body, html').animate({scrollTop: $('.product-size').offset().top}, 400);
		return false;
	});

	$('.product-size ul li div a').hover(function(){
		var $this_div = $(this).parent();
		var $this_li = $this_div.parent();
		var _div_index = $this_div.index();
		var _li_index = $this_li.index();
		$this_div.find('i.l').animate({'width': ((_div_index*110)+55)}, 150);
		$this_div.find('i.t').animate({'height': (_li_index*40)}, 150);
	}, function(){
		var $this = $(this);
		$this.find('i.l').animate({'width': 0}, 150);
		$this.find('i.t').animate({'height': 0}, 150);
	});

	var reCart = function(){
		if($('.cabinet-cart-del li').length>0){
			$('.cabinet-cart-del').show();
		} else {
			$('.cabinet-cart-del').hide();
		}
	};

	$('.cabinet-cart li .del').on('click', function(){
		var $this_item = $(this).parent();
		var $new_item = $this_item.clone();
		$new_item.find('.del').remove();
		$new_item.prepend('<a href="" class="re">восстановить</a>');
		$('.cabinet-cart-del ul').prepend($new_item);
		$this_item.hide();
		$new_item.find('.re').on('click', function(){
			$this_item.show();
			$new_item.remove();
			reCart();
			return false;
		});
		reCart();
		return false;
	});

});

$(window).load(function(){

	$('.slider-promo ul').carouFredSel({
		circular: true,
		infinite: true,
		auto: 4000,
		responsive: true,
		prev: {
			button: '.slider-promo .arr-l'
		},
		next: {
			button: '.slider-promo .arr-r'
		},
		scroll: {
			pauseOnHover: true,
			duration: 700
		},
		pagination: '.slider-promo .page'
	});

	$('.catalog-coll-slider ul').carouFredSel({
		circular: true,
		infinite: true,
		auto: 4000,
		responsive: false,
		prev: {
			button: '.catalog-coll-slider .arr-l'
		},
		next: {
			button: '.catalog-coll-slider .arr-r'
		},
		scroll: {
			pauseOnHover: true,
			duration: 700
		},
		pagination: '.catalog-coll-slider .page'
	});

	$('.product-detail .img .big li, .product-detail .img .preview li').each(function(){
		$(this).attr('data-id', $(this).index());
	});

	$('.product-detail .img .big ul').carouFredSel({
		circular: true,
		infinite: true,
		auto: false,
		responsive: false,
		prev: {
			button: '.product-detail .img .big .arr-l'
		},
		next: {
			button: '.product-detail .img .big .arr-r'
		},
		scroll: {
			duration: 300,
			onBefore: function(data){
				$('.product-detail .img .preview ul').trigger('slideTo', $(this).triggerHandler('currentPosition'));
				$('.product-detail .img .preview li').removeClass('active');
				$('.product-detail .img .preview li[data-id='+$(this).triggerHandler('currentPosition')+']').addClass('active');
			}
		}
	});

	$('.product-detail .img .preview ul').carouFredSel({
		circular: true,
		infinite: true,
		auto: false,
		responsive: false,
		direction: 'up',
		prev: {
			button: '.product-detail .img .preview .arr-l'
		},
		next: {
			button: '.product-detail .img .preview .arr-r'
		},
		items: {
			visible: 3
		},
		scroll: {
			items: 1,
			duration: 300
		}
	});

	$('.product-detail .img .preview li').on('click', function(){
		$('.product-detail .img .big ul').trigger('slideTo', $(this).data('id'));
	});

});

jQuery.fn.force_numeric_only =
function()
{
	return this.each(function()
	{
		$(this).keydown(function(e)
		{
			var key = e.charCode || e.keyCode || 0;
			// Разрешаем backspace, tab, delete, стрелки, обычные цифры и цифры на дополнительной клавиатуре
			return (
				key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 37 && key <= 40) ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
		});
	});
};