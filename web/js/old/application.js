'use strict';

var loader;

var app = {
	config : {

	},

	init : function(config){

		$.extend(app.config, config);


		app.elementCover('.cover');

		$(document).on('mouseover', '.svg path', function() {
			$(this).attr('fill', 'rgba(255,255,255,.3)');
		}).on('mouseout', '.svg path', function() {
			$(this).attr('fill', 'rgba(255,255,255,0.2)');
		});

		// Получить подъезд
		$('body').on('click', '#get-pd path', function(event) {
			event.preventDefault();
			var pd = $(this).attr('data-id');
			// app.loader('in');
			app.getPd(pd);
		});


		// Смена пароля
		$(document).on('beforeSubmit', '#js-change-password-form', function (e) {

			app.createSubmitLoader($(this).find('.ladda-button')[0]);

			var form = $(this);

			if (form.find('.has-error').length) {
				  return false;
			}

			$.ajax({
				url: $(this).attr('action'),
				type: 'POST',
				dataType: 'json',
				data: form.serialize(),
				beforeSend: function( xhr ) {
					app.startSubmitLoader();
				}
			})
			.done(function(data) {
				$('#js-change-password-form').trigger('reset');
				alert('Успешная отправка');
			})
			.fail(function() {alert('Произошла ошибка')})
			.always(function() {app.stopSubmitLoader();});

			return false;
		});

		// регистрация
		$(document).on('beforeSubmit', '#js-signup-form', function (e) {

			app.createSubmitLoader($(this).find('.ladda-button')[0]);

			var form = $(this);

			if (form.find('.has-error').length) {
				  return false;
			}

			$.ajax({
				url: $(this).attr('action'),
				type: 'POST',
				dataType: 'json',
				data: form.serialize(),
				beforeSend: function( xhr ) {
					app.startSubmitLoader();
				}
			})
			.done(function(data) {
				alert('Успешная отправка');
			})
			.fail(function() {alert('Произошла ошибка')})
			.always(function(){app.stopSubmitLoader();});

			return false;
		});

		// вход
		$(document).on('beforeSubmit', '#js-login-form', function (e) {

			app.createSubmitLoader($(this).find('.ladda-button')[0]);

			var form = $(this);

			if (form.find('.has-error').length) {
				  return false;
			}

			$.ajax({
				url: $(this).attr('action'),
				type: 'POST',
				dataType: 'json',
				data: form.serialize(),
				beforeSend: function( xhr ) {
					app.startSubmitLoader();
				}
			})
			.done(function(data) {
				alert('Успешная отправка');
			})
			.fail(function() {alert('Произошла ошибка')})
			.always(function(){app.stopSubmitLoader();});

			return false;
		});
	},

	// Всплывающее сообщение
	alertMessage : function(content){
		$('#notify_success').notify({
			delay: 500,
			hideDelay: 8000,
			content: content,
		});
	},

	// submit lloader
	createSubmitLoader: function(button){
		loader = Ladda.create(button);
	},
	startSubmitLoader: function(){
		loader.start();
	},
	stopSubmitLoader: function(){
		loader.stop();
	},
	// ************************************

	getPd: function(pd){
		$.ajax({
			url: '/project/'+pd,
			type: 'POST',
			dataType: 'json',
			// data: {param1: 'value1'},
		})
		.done(function(data) {
			app.setContent(data.content);
			window.history.pushState('url', 'Title', '/project/'+pd);
		})
		.fail(function() {})
		.always(function() {});
	},

	// замена контента при ajax
	setContent: function(content){
		$('#main-content').html(content);
		setTimeout(function() {app.elementCover('.cover')}, 10);

		/*var updateBlock = $('#update-block');
		updateBlock.before('<div id="update-block" class="update">'+content+'</div>');

		imagesLoaded( '.update .plan-house-img', { background: true }, function() {

			app.elementCover('.cover');
			app.resizeCoordElements();

			app.updateSVG('#update-block.update .cover img');
			updateBlock.addClass('opacity0');
			$('#update-block.update').addClass('opacity1');
			// $('#update-block.update').addClass('opacityPath');
			// $('#update-block.update .part-number').addClass('opacity');
			// updateBlock.removeClass('update1');
			// updateBlock.removeClass('update');

			setTimeout("$('#update-block.opacity0').remove();", 1000);
			setTimeout("$('#update-block.opacity1').removeClass('update');nextUpdate = true;", 500);

			// setTimeout("app.resizeImage('.update1 #chooseImage');", 0);
			// app.loader('out');
		});*/



	},

	elementCover: function(elementClass){
		$(elementClass).liCover({
			parent: $(window),
			position:'absolute',
			veticalAlign:'top',
			align:'center'
		});
	},
};


$(window).load(function() {
	app.init();
});
