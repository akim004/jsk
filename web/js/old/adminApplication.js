'use strict';

var adminApp = {
	config : {

	},

	init : function(config){

		$.extend(adminApp.config, config);

		$(document).on('mouseover', '.svg path', function() {

			if($(this).attr('data-edit') == 'admin'){
				$('.admin-edit').css({
					display: 'block',
					top: $(this)[0].getBoundingClientRect().top+20+'px',
					left: $(this)[0].getBoundingClientRect().left+20+'px'
				});

				$('.admin-edit').attr('data-to-save', $(this).attr('data-to-save'));
				$('.admin-edit').attr('data-points', $(this).attr('data-points'));
				$('.admin-edit').attr('data-edit-url', $(this).attr('data-edit-url'));
			}
		}).on('mouseout', '.svg path', function() {
		});

		$(document).on('click', '.admin-edit', function(event) {
			event.preventDefault();

			adminApp.clearUpdateObject();
			var WhereToSaveId = $(this).attr('data-to-save');
			var points = JSON.parse($(this).attr('data-points'));
			var transform = $('.svg g').attr('data-koof');
			var url = $(this).attr('data-edit-url');

			$('#modal')
				.show()
				.find('#modalContent')
				.load(url);

			if(points != 'undefined'){
				$('#js-big-image').polyDraw({
					pointCount: 3,
					koof: transform,
					WhereToSaveId: WhereToSaveId,
					points: points,
				});
			}
		});

		$(document).on('beforeSubmit', '#ajax-form', function(e) {
		    var form = $(this);
		    var formData = form.serialize();
		    $.ajax({
		        url: form.attr("action"),
		        type: form.attr("method"),
		        data: formData,
		        success: function (data) {
				    if(data.project){
				    	var projectPath = $('#path'+data.project.id);
				    	projectPath.attr('data-points', data.project.points);
				    	projectPath.attr('data-name', data.project.name);
				    	projectPath.attr('d', data.project.coords);
				    }
		             adminApp.clearUpdateObject(data);
		        },
		        error: function () {
		            alert("Something went wrong");
		        }
		    });
		}).on('submit', '#ajax-form', function(e){
		    e.preventDefault();
		});


	},

	//Закрыть форму редактирования и очистить все
	clearUpdateObject: function(){
		$('#holder').remove();
	    $('#modal').hide();
	    $('#modalContent').html('');
	    $('#holder-delete-pixels').remove();

	},
};


$(window).load(function() {
	adminApp.init();
});
