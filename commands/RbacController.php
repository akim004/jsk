<?php

namespace app\commands;

use Yii;
use app\modules\user\models\common\User;
use yii\console\Controller;
//use common\components\rbac\UserRoleRule;

class RbacController extends Controller
{
	public function actionInit()
	{
		$auth = Yii::$app->authManager;
		$auth->removeAll(); //удаляем старые данные

		/*$rule = new app\components\rbac\AuthorRule();
		$auth->add($rule);

		$updateOwnObject = $auth->createPermission('updateOwnObject');
		$updateOwnObject->description = 'Редактирование своего объекта';
		$updateOwnObject->ruleName = $rule->name;
		$auth->add($updateOwnObject);*/

		$userRoleRule = new \app\rbac\UserRoleRule();
        $auth->add($userRoleRule);

		$adminka = $auth->createPermission('adminka');
		$adminka->description = 'Админка';
		$auth->add($adminka);

		$user = $auth->createRole('user');
		$user->description = 'Пользователь';
		$user->ruleName = $userRoleRule->name;
		$auth->add($user);

		$moder = $auth->createRole('moder');
		$moder->description = 'Модератор';
		$moder->ruleName = $userRoleRule->name;
		$auth->add($moder);
		$auth->addChild($moder, $user);
		$auth->addChild($moder, $adminka);

		$admin = $auth->createRole('admin');
		$admin->description = 'Администратор';
		$admin->ruleName = $userRoleRule->name;
		$auth->add($admin);
		$auth->addChild($admin, $moder);


		$this->stdout('Done!' . PHP_EOL);
	}
}